USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 09.08.2013
-- Description:	Load System From MS SMS (system)
-- =============================================
ALTER PROCEDURE [dbo].ParseSystem @Result bit OUTPUT	
AS
BEGIN   

	SET NOCOUNT ON;
	
	SET @Result = 0;
	
	BEGIN TRANSACTION;

		UPDATE RawSystems 
			SET User_Name0 = User_Domain0 + '\' + User_Name0
			WHERE UPPER(SUBSTRING(User_Name0, 1, LEN(User_Domain0))) != UPPER(User_Domain0);

		UPDATE RawSystems SET 
			EmployeeId = (SELECT EmployeeId FROM Employees 
				WHERE UPPER(Authid) = UPPER(User_Name0)),
			CompanyId = (SELECT CompanyId FROM Employees 
				WHERE UPPER(Authid) = UPPER(User_Name0));
				
		DELETE FROM RawSystems WHERE EmployeeId is null
				
	COMMIT TRANSACTION;
	
	SET @Result = 1;
END
GO		