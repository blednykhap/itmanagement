USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 19.09.2013
-- Description:	Parse Maintenance By User
-- =============================================
ALTER PROCEDURE [dbo].[ParseMaintenanceByUser] @Result bit OUTPUT
--ALTER PROCEDURE [dbo].[ParseMaintenanceByUser]	@result = 0
AS
BEGIN   	
	DECLARE @MaintenanceId int, @SoftwareId int;

	SET NOCOUNT ON;
	
	SET @Result = 0;
	
	BEGIN TRANSACTION;

		UPDATE RawMaintenanceByUsers 
		SET RawLogin = 'CORP\' + RawLogin
		WHERE UPPER(SUBSTRING(RawLogin, 1, 4)) != 'CORP';

		UPDATE RawMaintenanceByUsers 
		SET 
			CompanyId = (SELECT CompanyId FROM Employees WHERE UPPER(Authid) = UPPER(RawLogin)),
			ComponentId = (SELECT ComponentId FROM Components WHERE UPPER(Code) = UPPER('ACC')),
			CompanyStructureId = (SELECT c.CompanyStructureId FROM CompanyStructures c, Employees e 
				WHERE c.EmployeeId = e.EmployeeId AND UPPER(e.Authid) = UPPER(RawLogin)),								
			CodeMonthFrom = dbo.CodeMonthByDate(GETDATE()),			
			CodeMonthTo = (SELECT dbo.CodeMonthByDate(DischargeDate) FROM Employees 
				WHERE UPPER(Authid) = UPPER(RawLogin)),
			SupportLevelId = (SELECT SupportLevelId FROM SupportLevels 
				WHERE UPPER(Name) = UPPER(RawSupportLevel)),
			MaintenanceId = (SELECT MaintenanceId FROM Maintenances 
				WHERE UPPER(Name) = UPPER(REPLACE(RawMaintenance, '  ', ' ')))
		WHERE GETDATE() < (SELECT DischargeDate FROM Employees 
			WHERE UPPER(Authid) = UPPER(RawLogin) AND CreatedBy = 'EKC' AND IsActive = 1);	
			
		UPDATE RawMaintenanceByUsers
		SET 
			SoftwareId = (SELECT SoftwareId FROM Softwares WHERE Name = '������� ���������� �����'),
			SoftwareModuleId = (SELECT SoftwareModuleId FROM SoftwareModules WHERE Code = 'BUH')
		WHERE RawSoftware = '���.ORNF.';
		
		UPDATE RawMaintenanceByUsers
		SET 
			SoftwareId = (SELECT SoftwareId FROM Softwares WHERE Name = '������� ���������� �����'),
			SoftwareModuleId = (SELECT SoftwareModuleId FROM SoftwareModules WHERE Code = 'ZSR')
		WHERE RawSoftware = '���.ORNF.';
		
		DELETE FROM RawMaintenanceByUsers 
		WHERE 1 = (SELECT COUNT(MaintenanceOrderId) FROM MaintenanceOrders o, CompanyStructures s, Employees e
				   WHERE o.CompanyStructureId = s.CompanyStructureId AND s.EmployeeId = e.EmployeeId
						AND o.MaintenanceId = RawMaintenanceByUsers.MaintenanceId 
						AND UPPER(e.Authid) = UPPER(RawMaintenanceByUsers.RawLogin) 
						AND dbo.CodeMonthByDate(GETDATE()) BETWEEN o.CodeMonthFrom AND o.CodeMonthTo);
		
		/*��� ����� �����*/
		DELETE FROM RawMaintenanceByUsers 
		WHERE 1 = (SELECT COUNT(MaintenanceOrderId) FROM MaintenanceOrders o, CompanyStructures s, Employees e
				   WHERE o.CompanyStructureId = s.CompanyStructureId AND s.EmployeeId = e.EmployeeId
						AND o.MaintenanceId = RawMaintenanceByUsers.MaintenanceId 
						AND o.SoftwareModuleId = RawMaintenanceByUsers.SoftwareModuleId 
						AND UPPER(e.Authid) = UPPER(RawMaintenanceByUsers.RawLogin) 
						AND dbo.CodeMonthByDate(GETDATE()) BETWEEN o.CodeMonthFrom AND o.CodeMonthTo);

	COMMIT TRANSACTION;

	SET @Result = 1;
	
END
GO		

--SELECT DISTINCT RawSoftware
--FROM RawMaintenanceByUsers
--WHERE RawMaintenance = '��������� �� "�����"'

/*
SELECT * 
FROM MaintenanceOrders 
WHERE MakeDate > CONVERT(DATETIME, '2013.10.02 17:15:00')
delete 
FROM MaintenanceOrders 
WHERE MakeDate > CONVERT(DATETIME, '2013.10.02 17:15:00')
*/

--select * from RawMaintenanceByUsers where CompanyStructureId is null
--select * from RawMaintenanceByUsers where MaintenanceId is null
--select * from Employees where Authid = 'CORP\nvaverina2'
--SELECT * FROM Employees WHERE Authid = 'CORP\nvaverina2'
