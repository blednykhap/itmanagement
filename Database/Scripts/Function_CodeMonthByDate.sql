USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 24.09.2013
-- Description:	GET CODE MONTH BY DATE
-- =============================================
ALTER FUNCTION [dbo].[CodeMonthByDate](@Date datetime) RETURNS int
--ALTER PROCEDURE [dbo].[ParseSAPMaintenance]	@result = 0
AS
BEGIN   		
	DECLARE @Result int;

	IF (@Date > convert(datetime,'2099.12.31 23:59:59',102)) BEGIN
		SET @Result = 2099*12 + 12;
	END
	ELSE BEGIN
		SET @Result = (DATEPART(MONTH, @Date)+DATEPART(YEAR, @Date)*12);
	END;
	
	RETURN @Result;	
END
GO	
--SELECT dbo.CodeMonthByDate(GETDATE())

--SELECT GETDATE(), CAST('31.12.2099 01:01:01' AS DATE)
--convert(datetime,replace(substring([Validend],1,10),''9999'',''2099''),103)
--SELECT convert(datetime,'2099.12.31 23:59:59',102)