USE [ITManagement]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- Description:	Report about how long monkey can work with ball on thid dec ship in 7 poin shtorm
-- =============================================
/*CREATE*/ ALTER PROCEDURE [dbo].[ReportUsedLicense]	
AS
BEGIN 	
	SELECT isnull(c.Name,'�� ������������') Contractor,
		s.softwareId ,isnull(s.Name,'�� ������������') Software,
		sum(Amount) Amount,
		l.ContractNumber + ' �� ' + convert(char(10), l.ContractDate, 104) Contract,
        case max(DateTo) when null then '���������' else '�� ' + convert(char(10), max(DateTo), 104) end DateTo,
        sum(isnull(Ins.UsedAmount,0)) UsedAmount,
        sum(Amount - isnull(Ins.UsedAmount,0)) Balance
	FROM Licenses l 
		left outer join Contractors C on l.ContractorId = c.ContractorId
		left outer join Softwares S on l.SoftwareId = S.SoftwareId
		left outer join 
		(	select inst.SoftwareId,COUNT(*) UsedAmount
			from InstalledSoftwares inst, ComputerSystems cs
			where cs.IsActive = 1 and inst.ResourceId = cs.ResourceId
			group by inst.SoftwareId ) Ins
		on ins.SoftwareId = l.SoftwareId
	where l.DateTo >=GETDATE()
	group by c.Name,s.softwareId, s.Name,l.ContractNumber,l.ContractDate
	order by c.Name,s.Name;
END

GO


