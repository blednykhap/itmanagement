
use ITManagement
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                                                    
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 21.09.2013
-- Description:	Get data from IAM and update if necessary portal user
-- =============================================
alter PROCEDURE [dbo].[LoadEmployee] @Result bit OUTPUT	
AS
BEGIN
	DECLARE @ExtAuthid nvarchar(100), @ExtSurname nvarchar(100), @ExtFirstname nvarchar(100), @ExtMiddlename nvarchar(100);
	DECLARE /*@ExtCostCenterCode nvarchar(100), @ExtPositionName nvarchar(500),*/ @ExtDepartmentCode nvarchar(100);
	DECLARE @ExtDischargeDate datetime, /*@ExtCompanyId int,*/ @ExtEmail nvarchar(100), @ExtPhone nvarchar(100);	

	DECLARE @IntAuthid nvarchar(100), @IntSurname nvarchar(100), @IntFirstname nvarchar(100), @IntMiddlename nvarchar(100);
	DECLARE @IntCostCenterId int, @IntPositionId int, @IntDepartmentId int;
	DECLARE @IntDischargeDate datetime, @IntCompanyId int, @IntEmail nvarchar(100), @IntPhone nvarchar(100);		

	DECLARE @SysEmployeeId int, @IntEmployeeId int, @LogAppEventId int; 
	DECLARE @IntParentCompanyStructureId int, @IntCompanyStructureId int;
	DECLARE @CompanyId int, @EKC int;

	SET NOCOUNT ON;

	SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'CORP\orn_service_ulu';

	SET @Result = 0;
	
	BEGIN TRANSACTION;

		DECLARE c CURSOR FOR 			 									
			SELECT EmployeeId,RawAuthid,RawSurname,RawFirstname,RawMiddlename,CostCenterId,RawEmail,
				PositionId,RawDepartmentCode,RawDischargeDate,CompanyId
			FROM RawEmployees				
		OPEN c;
	
		IF @@CURSOR_ROWS > 0 
		BEGIN			
			--INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
			--	VALUES(GETDATE(), @SysEmployeeId, '�������� ���������� (����������) �� ���');
			--SELECT @LogAppEventId = @@IDENTITY;

			UPDATE Employees SET IsActive = 0 WHERE CreatedBy = 'EKC'; 
			UPDATE CompanyStructures SET IsActive = 0 WHERE CreatedBy = 'EKC' AND EmployeeId is not null;
		END;
	
		FETCH NEXT FROM c 
			INTO @IntEmployeeId,@ExtAuthid,@ExtSurname,@ExtFirstname,@ExtMiddlename,@IntCostCenterId,@ExtEmail, 
				@IntPositionId, @ExtDepartmentCode, @ExtDischargeDate, @IntCompanyId;			 
	
		WHILE @@FETCH_STATUS=0 BEGIN
	
			-- STEP 1. FIND DEPARTMENT
			SELECT @IntDepartmentId = DepartmentId FROM Departments WHERE Code = @ExtDepartmentCode;
			
			-- STEP 2. UPDATE EMPLOYEE INFORMATION
			IF @IntEmployeeId is null BEGIN
				INSERT INTO Employees(Authid,Surname,Firstname,Middlename,Fullname,CompanyId,CostCenterId,PositionId,
					DepartmentId,EMail,DischargeDate,Comment,IsActive,CreatedBy)					
				VALUES(@ExtAuthid,@ExtSurname,@ExtFirstname,@ExtMiddlename,
					ISNULL(@ExtSurname,'')+' '+ISNULL(@ExtFirstname,'')+' '+ISNULL(@ExtMiddlename,''),
					@IntCompanyId, ISNULL(@IntCostCenterId,1), @IntPositionId, @IntDepartmentId, @ExtEmail, 
					@ExtDischargeDate,'��������� �� ���',1,'EKC');
				SELECT @IntEmployeeId = @@IDENTITY;
				
				--INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
				--	VALUES(@LogAppEventId, 'LOAD_EKC', 'Employees', @IntEmployeeId, 'INSERT', '��������� ��������: Authid=' + @ExtAuthid + 
				--		'; Fullname=' + ISNULL(@ExtSurname,'')+' '+ISNULL(@ExtFirstname,'')+' '+ISNULL(@ExtMiddlename,''));
			END
			ELSE BEGIN
				UPDATE Employees SET Surname = @ExtSurname, Firstname = @ExtFirstname, Middlename = @ExtMiddlename, 
					Fullname = ISNULL(@ExtSurname,'')+' '+ISNULL(@ExtFirstname,'')+' '+ISNULL(@ExtMiddlename,''),
					CompanyId = @IntCompanyId, CostCenterId = @IntCostCenterId, PositionId = @IntPositionId,
					DepartmentId = @IntDepartmentId, EMail = @ExtEmail, DischargeDate = @ExtDischargeDate,
					Comment = '��������� �� ���', IsActive = 1, CreatedBy = 'EKC'
				WHERE EmployeeId = @IntEmployeeId;				
			END;
			
			-- STEP 3. UPDATE RAWEMPLOYEE INFORMATION (DEPARTMENT)
			UPDATE RawEmployees SET DepartmentId = @IntDepartmentId WHERE EmployeeId = @IntEmployeeId;

			-- STEP 4. CHECK WHETHER EXIST MAINTENANCEOBJECTUNIT
			IF (@IntDepartmentId is not null) BEGIN
				SELECT @IntParentCompanyStructureId = CompanyStructureId FROM CompanyStructures
					WHERE DepartmentId = @IntDepartmentId;
			
				SELECT @IntCompanyStructureId = CompanyStructureId FROM CompanyStructures 
					WHERE ParentCompanyStructureId = @IntParentCompanyStructureId AND EmployeeId = @IntEmployeeId;
			
				IF (@IntCompanyStructureId is null) BEGIN				
					INSERT INTO CompanyStructures(ParentCompanyStructureId, EmployeeId, CompanyId, CreatedBy, IsActive)
						VALUES(@IntParentCompanyStructureId, @IntEmployeeId, @IntCompanyId, 'EKC', 1);
					SET @IntCompanyStructureId = NULL;	
					SELECT @IntCompanyStructureId = @@IDENTITY;

					--insert into Test(FildContent, IntField) Values('str=' + cast(isnull(@IntCompanyStructureId,0) as nvarchar) + 
					--	'; emp=' + cast(isnull(@IntEmployeeId,0) as nvarchar) + ';', @IntEmployeeId);
					
					--INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					--	VALUES(@LogAppEventId, 'LOAD_EKC', 'MaintenanceObjectUnits', @IntMaintenanceObjectUnitId, 'INSERT', '��������� ��������: Authid=' + 'CORP\' + @ExtAuthid);										
				END
				ELSE BEGIN
					UPDATE CompanyStructures SET IsActive = 1 WHERE CompanyStructureId = @IntCompanyStructureId;
				END;				
			END
			ELSE BEGIN				
				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_EKC', 'Employees', 0, 'ERROR', '�� ������� ������������� ���: Authid=' + 'CORP\' + @ExtAuthid);											
			END;
			
			SET @IntPositionId = NULL;	SET @IntCostCenterId = NULL;SET @IntDepartmentId = NULL;
			SET @IntCompanyId = NULL;	SET @IntEmployeeId = NULL;	SET @IntCompanyStructureId = NULL;						
			SET @IntParentCompanyStructureId = NULL;						

			SET @ExtAuthid = NULL;			SET @ExtSurname = NULL;		SET @ExtFirstname = NULL;	SET @ExtMiddlename = NULL;	
			/*SET @ExtCostCenterCode = NULL;*/	SET @ExtEmail = NULL;		/*SET @ExtPositionName = NULL;*/SET @ExtDepartmentCode = NULL;
			SET @ExtDischargeDate = NULL;		SET @ExtPhone = NULL;
			
			FETCH NEXT FROM c 
				INTO @IntEmployeeId,@ExtAuthid,@ExtSurname,@ExtFirstname,@ExtMiddlename,@IntCostCenterId,@ExtEmail, 
					@IntPositionId, @ExtDepartmentCode, @ExtDischargeDate, @IntCompanyId;	
		END;
    
		CLOSE c;
		DEALLOCATE c;

	COMMIT TRANSACTION;
	
	DELETE FROM RawEmployees;
	
	SET @Result = 1;
	
END;