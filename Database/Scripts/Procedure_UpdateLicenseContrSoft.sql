
use ITManagement
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                                                    
-- =============================================

-- Description:	Update data from SUL 
-- =============================================
/*Create*/ Alter PROCEDURE [dbo].[UpdateLicenseContSoft] 	
AS
BEGIN
	DECLARE @License_Id int, @SContractor nvarchar(240), @SSoftware nvarchar(240);
	DECLARE @IContractor int, @ISoftware int;	

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

--1Phase : Updata GeneralContractor & GeneralSoftware
	BEGIN TRANSACTION;
		DECLARE c CURSOR FOR 			 									
		select l.LicenseId,	--l.Code,CSS.LICENSES,l.ContractorId,
		       CSS.PUBLISHER, --l.SoftwareId,
		       CSS.PRODUCTS
		 from Licenses l
		left outer join --Raw_LICENSING
		(   select pu.name PUBLISHER ,pr.name PRODUCTS,li.name LICENSES
				from
				[CSS]..[CSS].[SUL_TR_LICENSING] ls
				left outer join  [CSS]..[CSS].[SUL_TR_PUBLISHER] pu
				on ls.publisher = pu.rn
				left outer join [CSS]..[CSS].[SUL_TR_PRODUCTS] pr
				on ls.product = pr.rn
				left outer join [CSS]..[CSS].[SUL_TR_LICENSES] li
				on ls.license = li.rn
		)
		CSS
		on convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(rtrim(ltrim(l.Code))) collate SQL_Latin1_General_CP1_CI_AS)))
		 = convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(rtrim(ltrim(css.Licenses))) collate SQL_Latin1_General_CP1_CI_AS)))
		where l.Code is not null 
				and (l.ContractorId is null or SoftwareId is null)	;	        			
		OPEN c;
	
		IF @@CURSOR_ROWS > 0 
				
		FETCH NEXT FROM c 		
			INTO @License_Id, @SContractor, @SSoftware;			     
			     
		WHILE @@FETCH_STATUS=0 BEGIN	
				     
		IF (@SContractor is not null) BEGIN	
		
			select @IContractor = ContractorId  from Contractors
			where UPPER(name) = upper(@SContractor);
			
			IF (@IContractor is null) BEGIN
			
			INSERT INTO Contractors(Name,Comment)
				VALUES(@SContractor, '������������� ��� ������������� ������ �� SUL');
			SELECT @IContractor = @@IDENTITY;			
			END;
		END;
		
		IF (@SSoftware is not null) BEGIN	
		
			select @ISoftware = SoftwareId  from Softwares
			where UPPER(name) = upper(@SSoftware);
			
			IF (@ISoftware is null) BEGIN
			
			INSERT INTO Softwares(Name,Comment)
				VALUES(@SSoftware, '������������� ��� ������������� ������ �� SUL');
			SELECT @ISoftware = @@IDENTITY;			
			END;
		END;		

			
		UPDATE Licenses
		--UPDATE Test_Licenses
			SET ContractorId = isnull(@IContractor,null)
			   ,SoftwareId = isnull(@ISoftware,null)
		WHERE LicenseId = @License_Id ; 

			
			SET @License_Id = null; SET @SContractor = null;SET @SSoftware = null;	
			SET @IContractor = null; SET @ISoftware = null; 
	
			FETCH NEXT FROM c 
				INTO @License_Id, @SContractor, @SSoftware;
		END;
    
		CLOSE c;
		DEALLOCATE c;
	COMMIT TRANSACTION;
END;

