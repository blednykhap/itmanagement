USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 10.08.2013
-- Description:	Parse SMS Info
-- =============================================
create PROCEDURE [dbo].[ParseSMSInfo]	
AS
BEGIN   
	DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;
	
	SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'CORP\orn_service_ulu';
	
	INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
		VALUES(GETDATE(), @SysEmployeeId, '������� ���������� ���������� �� SMS');
	SELECT @LogAppEventId = @@IDENTITY;

	COMMIT TRANSACTION;
END
GO		