USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 10.08.2013
-- Description:	Report In
-- =============================================
ALTER PROCEDURE [dbo].[ReportInstalledSoftware]	(@softwareId int)
AS
BEGIN   	
	IF (@softwareId = 0) BEGIN
		SELECT inst.InstalledSoftwareId, empl.Fullname EmployeeFullname, comp.NetbiosName, 
			soft.SoftwareId SoftwareId, soft.Name Software		
		FROM InstalledSoftwares inst, ComputerSystems comp, Employees empl, Softwares soft
		WHERE inst.ResourceId = comp.ResourceId AND comp.EmployeeId = empl.EmployeeId 
			AND inst.SoftwareId = soft.SoftwareId
		ORDER BY empl.Fullname;	
	END
	ELSE
	BEGIN
		SELECT inst.InstalledSoftwareId, empl.Fullname EmployeeFullname, comp.NetbiosName, 
			soft.SoftwareId SoftwareId, soft.Name Software		
		FROM InstalledSoftwares inst, ComputerSystems comp, Employees empl, Softwares soft
		WHERE inst.ResourceId = comp.ResourceId AND comp.EmployeeId = empl.EmployeeId 
			AND inst.SoftwareId = soft.SoftwareId AND soft.SoftwareId = @softwareId
		ORDER BY empl.Fullname;	
	END
END
GO		