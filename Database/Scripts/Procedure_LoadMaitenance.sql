USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 10.07.2013
-- Description:	Load Maintenance From BMC Remedy
-- =============================================
alter PROCEDURE [dbo].[LoadMaintenance]	
AS
BEGIN	
	DECLARE @IntCompanyId int, @IntCompanyName nvarchar(500), @IntMaintenanceId int;
	DECLARE @IntServiceLineId int, @IntCompanyMaintenanceId int;
	DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	
	
	DECLARE @ExtMaintenanceCode nvarchar(100), @ExtMaintenanceName nvarchar(500), @ExtServiceLineCode nvarchar(100);	

	SET NOCOUNT ON;

	SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'user_ro';

	BEGIN TRANSACTION;

		DECLARE c CURSOR FOR 
		SELECT MaintenanceName, MaintenanceCode, ServiceLineCode, c.CompanyId, c.Name FROM 				
			OPENQUERY([NV-ITSM-SQL01\SQL01],
				'SELECT maintanence.Name MaintenanceName, maintanence.Service_Code MaintenanceCode, maintanence.Service_Line ServiceLineCode, link.Full_Name CompanyName 
				 FROM [ARSystem].[dbo].[AST_AssetPeople] link, [ARSystem].[dbo].[AST_BusinessService] maintanence 
				 WHERE link.AssetInstanceId = maintanence.Reconciliation_Identity and maintanence.Service_Line is not null') BMC
			INNER JOIN
			Companies c ON BMC.CompanyName = c.BMC
		OPEN c;

		IF @@CURSOR_ROWS > 0 
		BEGIN			
			INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
				VALUES(GETDATE(), @SysEmployeeId, '�������� ���������� �� BMC Remedy');
			SELECT @LogAppEventId = @@IDENTITY;
		END;

		FETCH NEXT FROM c INTO @ExtMaintenanceName, @ExtMaintenanceCode, @ExtServiceLineCode, @IntCompanyId, @IntCompanyName;

		WHILE @@FETCH_STATUS = 0 BEGIN

			/*CHECK AND ADD SERVICE LINE*/
			SELECT @IntServiceLineId = ServiceLineId FROM ServiceLines WHERE Code = @ExtServiceLineCode;
			IF @IntServiceLineId IS NULL BEGIN
				INSERT INTO ServiceLines(Code, Comment) VALUES(@ExtServiceLineCode, '��������� �� BMC Remedy');
				SELECT @IntServiceLineId = @@IDENTITY;

				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_REMEDY', 'ServiceLines', @IntServiceLineId, 'INSERT', '��������� ��������: ServiceLineCode=' + @ExtServiceLineCode);
			END;

			/*CHECK AND ADD MAINTENANCE*/
			SELECT @IntMaintenanceId = MaintenanceId FROM Maintenances WHERE Code = @ExtMaintenanceCode AND ServiceLineId = @IntServiceLineId;
			IF @IntMaintenanceId IS NULL BEGIN
				INSERT INTO Maintenances(Code, Name, ServiceLineId, Comment) 
					VALUES(@ExtMaintenanceCode, @ExtMaintenanceName, @IntServiceLineId, '��������� �� BMC Remedy');
				SELECT @IntMaintenanceId = @@IDENTITY;

				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_REMEDY', 'Maintenances', @IntMaintenanceId, 'INSERT', '��������� ��������: MaintenanceCode=' 
						+ @ExtMaintenanceCode + '; MaintenanceName=' + @ExtMaintenanceName);
			END;

			/*CHECK AND ADD MAINTENANCE FOR THE COMPANY*/
			SELECT @IntCompanyMaintenanceId = CompanyMaintenanceId FROM CompanyMaintenances WHERE CompanyId = @IntCompanyId AND MaintenanceId = @IntMaintenanceId;			
			IF @IntCompanyMaintenanceId IS NULL BEGIN
				INSERT INTO CompanyMaintenances(CompanyId, MaintenanceId, Comment) 
					VALUES(@IntCompanyId, @IntMaintenanceId, '��������� �� BMC Remedy');
				SELECT @IntCompanyMaintenanceId = @@IDENTITY;
					
				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_REMEDY', 'CompanyMaintenances', @IntCompanyMaintenanceId, 'INSERT', '��������� ��������: CompanyName=' 
						+ @IntCompanyName + '; @MaintenanceName=' + @ExtMaintenanceName);									
			END;

			SET @IntServiceLineId = NULL;	SET @ExtServiceLineCode = NULL;		SET @IntMaintenanceId = NULL;	
			SET @ExtMaintenanceCode = NULL;	SET @ExtMaintenanceName = NULL;		SET @IntCompanyMaintenanceId = NULL;

			FETCH NEXT FROM c INTO @ExtMaintenanceName, @ExtMaintenanceCode, @ExtServiceLineCode, @IntCompanyId, @IntCompanyName;
		END;
		
		CLOSE c;
		DEALLOCATE c;

	COMMIT TRANSACTION;

END
GO

--USE ITManagement
--go
--exec LoadMaintenance

--select * FROM [ITManagement].[dbo].[Maintenances]
--select * FROM [ITManagement].[dbo].[ServiceLines]
--select * FROM [ITManagement].[dbo].[CompanyMaintenances]
--select * FROM [ITManagement].[dbo].[LogAppEvents]
--select * FROM [ITManagement].[dbo].[LogDataChanges]

--delete FROM [ITManagement].[dbo].[Maintenances]
--delete FROM [ITManagement].[dbo].[ServiceLines]
--delete FROM [ITManagement].[dbo].[CompanyMaintenances]