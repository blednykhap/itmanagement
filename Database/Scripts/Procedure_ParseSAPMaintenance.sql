USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 24.08.2013
-- Description:	Parse SAP Maintenance
-- =============================================
ALTER PROCEDURE [dbo].[ParseSAPMaintenance] @Result bit OUTPUT
AS
BEGIN   	

	SET NOCOUNT ON;
	SET @Result = 0;
	
	BEGIN TRANSACTION;

		UPDATE RawSAPMaintenances 
		SET CompanyId = (SELECT CompanyId FROM Companies WHERE RawCompany like '%'+SAP+'%')

		DELETE FROM RawSAPMaintenances WHERE CompanyId IS NULL;

		UPDATE RawSAPMaintenances 
		SET RawLogin = 'CORP\' + RawLogin
		WHERE UPPER(SUBSTRING(RawLogin, 1, 4)) != 'CORP';

		UPDATE RawSAPMaintenances 
		SET MaintenanceId = (SELECT MaintenanceId FROM Maintenances WHERE Code = 'SAP'),			
			ComponentId = (SELECT ComponentId FROM Components WHERE UPPER(Code) = UPPER('ACC')),
			CompanyStructureId = (SELECT c.CompanyStructureId FROM CompanyStructures c, Employees e 
				WHERE c.EmployeeId = e.EmployeeId AND UPPER(e.Authid) = UPPER(RawLogin)),
			SoftwareId =
				(SELECT SoftwareId FROM Softwares 
				 WHERE Name = (CASE WHEN Module = 'HR' THEN 'SAP HR ������'
									WHEN Module = 'US' THEN 'SAP US ������'
									WHEN Module = 'MDM' THEN 'SAP MDM ������' END) );

	COMMIT TRANSACTION;

	SET @Result = 1;
	
END
GO