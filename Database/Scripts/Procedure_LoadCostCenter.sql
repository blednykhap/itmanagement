USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 26.08.2013
-- Description:	Parse CostCenter
-- =============================================
ALTER PROCEDURE [dbo].[LoadCostCenter]
--ALTER PROCEDURE [dbo].[ParseSAPMaintenance]	@result = 0
AS
BEGIN   

	SET NOCOUNT ON;

	--select * FROM RawCostCenters
	--select * FROM CostCenters
	--update CostCenters set Name = null 

	UPDATE CostCenters SET Name = (SELECT ExcelName FROM RawCostCenters WHERE ExcelCode = Code AND CostCenterId is not null);

	INSERT INTO CostCenters(CompanyId, Code, Name, Comment)
	SELECT CompanyId, ExcelCode, ExcelName, '�������� ���������� �� ����� Excel'
	FROM RawCostCenters
	WHERE CostCenterId is null;
	
	
	return 1;
END
GO		