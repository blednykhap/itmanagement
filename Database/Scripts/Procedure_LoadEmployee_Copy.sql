
use ITManagement
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                                                    
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 21.09.2012
-- Description:	Get data from IAM and update if necessary portal user
-- =============================================
alter PROCEDURE [dbo].[LoadEmployee] 	
AS
BEGIN
	DECLARE @ExtAuthid nvarchar(100), @ExtSurname nvarchar(100), @ExtFirstname nvarchar(100), @ExtMiddlename nvarchar(100);
	DECLARE @ExtCostCenterCode nvarchar(100), @ExtPositionName nvarchar(500), @ExtDepartmentCode nvarchar(100);
	DECLARE @ExtValidend nvarchar(100), /*@ExtCompanyId int,*/ @ExtEmail nvarchar(100), @ExtPhone nvarchar(100);	

	DECLARE @IntAuthid nvarchar(100), @IntSurname nvarchar(100), @IntFirstname nvarchar(100), @IntMiddlename nvarchar(100);
	DECLARE @IntCostCenterId int, @IntPositionId int, @IntDepartmentId int;
	DECLARE @IntDischargeDate datetime, @IntCompanyId int, @IntEmail nvarchar(100), @IntPhone nvarchar(100);		

	DECLARE @SysEmployeeId int, @IntEmployeeId int, @LogAppEventId int; 
	DECLARE @IntParentMaintenanceObjectUnitId int, @IntMaintenanceObjectUnitId int;
	DECLARE @CompanyId int, @EKC int;

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'CORP\orn_service_ulu';

	BEGIN TRANSACTION;

		DECLARE c CURSOR FOR 			 									
			SELECT Authid, Surname, Firstname, Middlename, CostCenterCode, Email, PositionName, DepartmentCode, Phone, Validend, c.CompanyId
			FROM 							
			OPENQUERY([MSK-EMPCAT-01], 
				'SELECT adlogin Authid, lastname_ru Surname, firstname_ru Firstname, middlename_ru Middlename, Costcenter CostCenterCode, Email,
					Jobposition PositionName, RIGHT(Orgunitpathids,CHARINDEX('':'',REVERSE(Orgunitpathids))-1) DepartmentCode,  
					Extphone Phone, Validend, companyid CompanyId
				 FROM [the-one-catalog].[the-one-catalog].[EMPLOYEE]
				 WHERE convert(datetime,replace(substring([Validend],1,10),''9999'',''2099''),103) > GETDATE() and adlogin is not NULL') EKC
			INNER JOIN
			Companies c ON EKC.CompanyId = c.EKC; -- ORN OIL - 231		
		OPEN c;
	
		IF @@CURSOR_ROWS > 0 
		BEGIN			
			INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
				VALUES(GETDATE(), @SysEmployeeId, '�������� ���������� (����������) �� ���');
			SELECT @LogAppEventId = @@IDENTITY;

			UPDATE Employees SET IsActive = 0 WHERE CreatedBy = 'EKC'; 
		END;
	
		FETCH NEXT FROM c 
			INTO @ExtAuthid, @ExtSurname, @ExtFirstname, @ExtMiddlename, @ExtCostCenterCode, @ExtEmail, 
				@ExtPositionName, @ExtDepartmentCode, @ExtPhone, @ExtValidend, @IntCompanyId;			 
	
		WHILE @@FETCH_STATUS=0 BEGIN
	
			--STEP 1. CHECK WHETHER EXIST POSITION		
			SELECT @IntPositionId = PositionId FROM Positions WHERE Name = @ExtPositionName;
				
			IF (@IntPositionId is null) BEGIN			
				INSERT INTO Positions(Name, Comment) VALUES(@ExtPositionName, '�������� ���������� �� ���')
				SELECT @IntPositionId = @@IDENTITY;
			
				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_EKC', 'Positions', @IntPositionId, 'INSERT', '��������� ��������: Name=' + @ExtPositionName);							
			END;					

			--STEP 2. CHECK WHETHER EXIST COSTCENTER		
			--update CostCenters set Code = CONVERT(NVARCHAR,CONVERT(int, Code))
			--where Code != '000000000' and ISNUMERIC(Code) = 1 			
			SELECT @IntCostCenterId = CostCenterId FROM CostCenters WHERE Code = @ExtCostCenterCode;
				
			IF (@IntCostCenterId is null AND @ExtCostCenterCode is not null) BEGIN			
				INSERT INTO CostCenters(CompanyId, Code, Comment) 
					VALUES(@IntCompanyId, @ExtCostCenterCode, '�������� ���������� �� ���')
				SELECT @IntCostCenterId = @@IDENTITY;
			
				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_EKC', 'CostCenters', @IntCostCenterId, 'INSERT', '��������� ��������: Code=' + @ExtCostCenterCode);							
			END;	

			-- STEP 3. CHECK WHETHER EXIST DEPARTMENT
			SELECT @IntDepartmentId = DepartmentId FROM Departments WHERE Code = @ExtDepartmentCode;

			-- STEP 4. CHECK WHETHER EXIST EMPLOYEE
			SELECT @IntEmployeeId = EmployeeId, @IntAuthid = Authid, @IntSurname = Surname, @IntFirstname = Firstname, 
				@IntMiddlename = Middlename, /*@IntCompanyId = CompanyId,*/ @IntCostCenterId = CostCenterId, @IntPositionId = PositionId,
				@IntDepartmentId = DepartmentId, @IntEmail = Email, @IntDischargeDate = DischargeDate	
			FROM Employees WHERE Authid = 'CORP\' + @ExtAuthid;				
					
			-- STEP 4.1. IF EMPLOYEE DOESN'T EXIST - CREATE
			IF @IntEmployeeId is null BEGIN
				INSERT INTO Employees(Authid,Surname,Firstname,Middlename,Fullname,CompanyId,CostCenterId,PositionId,
					DepartmentId,EMail,DischargeDate,Comment,IsActive,CreatedBy)					
				VALUES('CORP\' + @ExtAuthid,@ExtSurname,@ExtFirstname,@ExtMiddlename,
					ISNULL(@ExtSurname,'')+' '+ISNULL(@ExtFirstname,'')+' '+ISNULL(@ExtMiddlename,''),
					@IntCompanyId, ISNULL(@IntCostCenterId,1), @IntPositionId, @IntDepartmentId, @ExtEmail, 
					CONVERT(datetime,@ExtValidend,103),'��������� �� ���',1,'LOAD_EKC');
				SELECT @IntEmployeeId = @@IDENTITY;
				
				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_EKC', 'Employees', @IntEmployeeId, 'INSERT', '��������� ��������: Authid=' + @ExtAuthid + 
						'; Fullname=' + ISNULL(@ExtSurname,'')+' '+ISNULL(@ExtFirstname,'')+' '+ISNULL(@ExtMiddlename,''));
			END
			ELSE BEGIN
				UPDATE Employees SET IsActive = 1 WHERE EmployeeId = @IntEmployeeId;
			END;
			
			-- STEP 5. CHECK WHETHER EXIST MAINTENANCEOBJECTUNIT
			IF (@IntDepartmentId is not null) BEGIN
				SELECT @IntParentMaintenanceObjectUnitId = MaintenanceObjectUnitId FROM MaintenanceObjectUnits
					WHERE DepartmentId = @IntDepartmentId;
			
				SELECT @IntMaintenanceObjectUnitId = MaintenanceObjectUnitId FROM MaintenanceObjectUnits 
					WHERE ParentMaintenanceObjectUnitId = @IntParentMaintenanceObjectUnitId AND EmployeeId = @IntEmployeeId;
			
				IF (@IntMaintenanceObjectUnitId is null) BEGIN				
					INSERT INTO MaintenanceObjectUnits(ParentMaintenanceObjectUnitId, EmployeeId)
						VALUES(@IntMaintenanceObjectUnitId, @IntEmployeeId);
					SET @IntMaintenanceObjectUnitId = NULL;	
					SELECT @IntMaintenanceObjectUnitId = @@IDENTITY;
					
					INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
						VALUES(@LogAppEventId, 'LOAD_EKC', 'MaintenanceObjectUnits', @IntMaintenanceObjectUnitId, 'INSERT', '��������� ��������: Authid=' + 'CORP\' + @ExtAuthid);										
				END
				ELSE BEGIN
					UPDATE MaintenanceObjectUnits SET IsActive = 1 WHERE MaintenanceObjectUnitId = @IntMaintenanceObjectUnitId;
				END;				
			END
			ELSE BEGIN				
				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_EKC', 'Employees', 0, 'ERROR', '�� ������� ������������� ���: Authid=' + 'CORP\' + @ExtAuthid);											
			END;
			
			SET @IntPositionId = NULL;	SET @IntCostCenterId = NULL;SET @IntDepartmentId = NULL;
			SET @IntCompanyId = NULL;	SET @IntEmployeeId = NULL;	SET @IntMaintenanceObjectUnitId = NULL;						
			SET @IntParentMaintenanceObjectUnitId = NULL;						

			SET @ExtAuthid = NULL;			SET @ExtSurname = NULL;		SET @ExtFirstname = NULL;	SET @ExtMiddlename = NULL;	
			SET @ExtCostCenterCode = NULL;	SET @ExtEmail = NULL;		SET @ExtPositionName = NULL;SET @ExtDepartmentCode = NULL;
			SET @ExtValidend = NULL;		SET @ExtPhone = NULL;
					
			FETCH NEXT FROM c 
				INTO @ExtAuthid, @ExtSurname, @ExtFirstname, @ExtMiddlename, @ExtCostCenterCode, @ExtEmail,
					@ExtPositionName, @ExtDepartmentCode, @ExtPhone, @ExtValidend, @IntCompanyId;
		END;
    
		CLOSE c;
		DEALLOCATE c;

	COMMIT TRANSACTION;
END;