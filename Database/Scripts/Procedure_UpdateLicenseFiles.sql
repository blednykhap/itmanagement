use ITManagement
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                                                    
-- =============================================
-- Description:	Get data from SUL 
-- =============================================
create PROCEDURE [dbo].[UpdateLicenseFiles] 	
AS
BEGIN
	
	use ITManagement
	go	
	MERGE INTO LicenseFiles AS target
	USING (
		SELECT S.LicenseId, O.SAVED_FILENAME
		FROM (
			SELECT 
				L_O.NAME NAME, 
			--convert(varchar(255), convert(varbinary(255), convert(varchar(255), upper(L_O.NAME) collate SQL_Latin1_General_CP1_CI_AS))) NAME,			
				F.SAVED_FILENAME
			FROM [CSS]..[CSS].[SUL_TR_LICENSES] L_O, [CSS]..[CSS].[SUL_TR_FILES] F
			WHERE L_O.RN = F.LIC_RN) O, Licenses S
		WHERE 
			--O.NAME = S.Code	
			convert(varchar(255), convert(varbinary(255), convert(varchar(255), upper(O.NAME) collate SQL_Latin1_General_CP1_CI_AS))) = 
			convert(varchar(255), convert(varbinary(255), convert(varchar(255), upper(S.Code) collate SQL_Latin1_General_CP1_CI_AS)))				
			
	) AS source(LicenseId, SAVED_FILENAME)
	ON (target.LicenseId = source.LicenseId AND 	
		convert(varchar(255), convert(varbinary(255), convert(varchar(255), upper(target.FileName) collate SQL_Latin1_General_CP1_CI_AS))) = 
		convert(varchar(255), convert(varbinary(255), convert(varchar(255), upper(source.SAVED_FILENAME) collate SQL_Latin1_General_CP1_CI_AS)))					
		--target.FileName = 
		--source.SAVED_FILENAME
		)		
	WHEN NOT MATCHED BY TARGET THEN INSERT(LicenseId, FileName) VALUES(LicenseId, SAVED_FILENAME);						


END;

