
use ITManagement
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                                                    
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 20.09.2013
-- Description:	Get data from IAM and update if necessary portal user
-- =============================================
ALTER PROCEDURE [dbo].[ImportEmployee]/* @Result bit OUTPUT	*/
AS
BEGIN

	/*SET @Result = 0;*/
	
	BEGIN TRANSACTION;
	
		DELETE FROM RawEmployees;

		INSERT INTO RawEmployees(RawAuthid,RawSurname,RawFirstname,RawMiddlename,RawCostCenter,RawEMail,RawPosition,
			RawDepartmentCode,RawDepartmentCodes,RawDepartmentNames,RawDischargeDate,CompanyId)
		SELECT 'CORP\'+RawAuthid,RawSurname,RawFirstname,RawMiddlename,RawCostCenterCode,RawEmail,RawPosition,
			RawDepartmentCode,RawDepartmentCodes,RawDepartmentNames,Validend,c.CompanyId
		FROM OPENQUERY([MSK-EMPCAT-01], 
			'SELECT adlogin RawAuthid, lastname_ru RawSurname, firstname_ru RawFirstname, middlename_ru RawMiddlename, 
				Costcenter RawCostCenterCode, Email RawEmail, Jobposition RawPosition, 
				RIGHT(Orgunitpathids,CHARINDEX('':'',REVERSE(Orgunitpathids))-1) RawDepartmentCode,
				Orgunitpathids RawDepartmentCodes, Orgunitpath RawDepartmentNames,
				convert(datetime,replace(substring([Validend],1,10),''9999'',''2099''),103) Validend, companyid CompanyId
			 FROM [the-one-catalog].[the-one-catalog].[EMPLOYEE]
			 WHERE convert(datetime,replace(substring([Validend],1,10),''9999'',''2099''),103) > GETDATE() and adlogin is not NULL' ) EKC
		INNER JOIN Companies c ON EKC.CompanyId = c.EKC;
		
	COMMIT TRANSACTION;	
	
	/*SET @Result = 1;*/
		
END;
