USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 09.08.2013
-- Description:	Load System From MS SMS (system)
-- =============================================
ALTER PROCEDURE [dbo].[ImportSystem] @Result bit OUTPUT
AS
BEGIN   
	SET NOCOUNT ON;
	
	SET @Result = 0;
	
	BEGIN TRANSACTION;
		DELETE FROM RawSystems;	

		INSERT INTO RawSystems(ResourceId,AgentTime,User_Domain0,User_Name0,Netbios_Name0,Operating_System_Name_and0)	
		SELECT ResourceId,AgentTime,User_Domain0,User_Name0,Netbios_Name0,Operating_System_Name_and0		
		FROM 							
			OPENQUERY([BUZULUK-MAN01], 
				'SELECT DISTINCT s.ResourceID, Atime.AgentTime, s.User_Domain0, s.User_Name0, s.Netbios_Name0, s.Operating_System_Name_and0
				 FROM [SMS_005].[dbo].[v_R_System] s
					LEFT JOIN 
				(SELECT ResourceID,MAX(AgentTime) AS AgentTime 
				 FROM [SMS_005].[dbo].[v_AgentDiscoveries]
				 WHERE LOWER(agentname) = ''heartbeat discovery'' GROUP BY ResourceID) AS Atime
					ON s.ResourceID = ATime.ResourceID
				 WHERE s.User_Name0 IS NOT NULL')
	COMMIT TRANSACTION;
	
	SET @Result = 1;
END
GO		