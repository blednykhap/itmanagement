USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 16.08.2013
-- Description:	Load License From Old System
-- =============================================
alter PROCEDURE [dbo].[LoadLicense]	
AS
BEGIN   
	DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;	

		SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'ro_sms05';
		
		INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
			VALUES(GETDATE(), @SysEmployeeId, '�������� ���������� �� ���������');
		SELECT @LogAppEventId = @@IDENTITY;
			
		INSERT INTO Licenses(Code,Name,Amount/*,DateTo*/,ContractNumber,ContractDate/*,ContragentId*/)	
		SELECT Name Code, Fullname Name, Quant Amount,/*Date_To,*/CNumb ContractNumber,
			ISNULL(CDate,CONVERT(datetime,'01.01.2000')) ContractDate/*, Contragent ContragentId*/
		FROM [CSS]..[CSS].[SUL_TR_LICENSES];
					
	COMMIT TRANSACTION;
END
GO		

--SELECT se.Name Code, se.Fullname Name, se.Quant Amount,/*se.Date_To,*/se.CNumb ContractNumber,
--	ISNULL(se.CDate,CONVERT(datetime,'01.01.2000')) ContractDate,/*, Contragent ContragentId*/
--	ing.publisher, ing.product
--FROM [CSS]..[CSS].[SUL_TR_LICENSES] se left join [CSS]..[CSS].[SUL_TR_LICENSING] ing
--	ON se.rn = ing.License
--WHERE (ing.Publisher is not null AND ing.Product is not null AND ing.License is not null)

--SELECT *
--FROM [CSS]..[CSS].[SUL_TR_LICENSING]

--SELECT License, COUNT(License)
--FROM [CSS]..[CSS].[SUL_TR_LICENSING] 
--GROUP BY License
--HAVING COUNT(License) > 1

--  SELECT a.rn, a.publisher, a.product, a.license
--    FROM sul_tr_licensing a;