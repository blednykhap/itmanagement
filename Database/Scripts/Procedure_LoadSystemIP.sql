USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 09.08.2013
-- Description:	Load System From MS SMS (IP)
-- =============================================
create PROCEDURE [dbo].[LoadSystemIP]	
AS
BEGIN   
	DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;
	
	SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'ro_sms05';
	
	INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
		VALUES(GETDATE(), @SysEmployeeId, '�������� �������������� �� �� SMS (IP)');
	SELECT @LogAppEventId = @@IDENTITY;
			
	DELETE FROM RawSystemIPs;	

	INSERT INTO RawSystemIPs(MakeDate,ResourceId,IP_Addresses0)	
	SELECT GETDATE() MakeDate,ResourceId,IP_Addresses0		
	FROM 							
		OPENQUERY([BUZULUK-MAN01], 
			'SELECT ResourceID,IP_Addresses0 FROM [SMS_005].[dbo].[v_RA_System_IPAddresses]')

	COMMIT TRANSACTION;
END
GO		