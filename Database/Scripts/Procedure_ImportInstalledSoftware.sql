USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 09.08.2013
-- Description:	Load Installed Software From MS SMS
-- =============================================
ALTER PROCEDURE [dbo].[ImportInstalledSoftware] --@Result bit OUTPUT
AS
BEGIN   
	--DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	
	--DECLARE @IntResourceId int;

	SET NOCOUNT ON;
	--SET @Result = 0;
	
	BEGIN TRANSACTION;	
		--SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'ro_sms05';
		
		--INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
		--	VALUES(GETDATE(), @SysEmployeeId, '�������� �������������� �� �� SMS');
		--SELECT @LogAppEventId = @@IDENTITY;
				
		DELETE FROM RawInstalledSoftwares;	

		-- http://www.sql.ru/forum/1040719-1/problema-s-kodirovkoy
		INSERT INTO RawInstalledSoftwares(/*MakeDate,*/MachineId,InstanceKey/*,InstallDirectoryValidatio0,RevisionId,
			AgentId,TimeKey,SoftwareCode00,ProductCode00,CM_DSLID00*/,ProductName00/*,ARPDisplayName00*/,
			ProductVersion00,Publisher00/*,VersionMajor00,VersionMinor00*/,ServicePack00/*,Language00,
			ProductID00,InstalledLocation00,InstallSource00,UninstallString00,LocalPackage00,
			UpgradeCode00*/,InstallDate00/*,RegisteredUser00,SoftwarePropertiesHash00,SoftwarePropertiesHashEx00*/)		
		SELECT /*GETDATE() MakeDate,*/ MachineId, InstanceKey
			/*InstallDirectoryValidatio0,RevisionId,AgentId,TimeKey,SoftwareCode00,ProductCode00,CM_DSLID00*/
			,convert(varchar(255), convert(varbinary(255), convert(varchar(255), ProductName00 collate SQL_Latin1_General_CP1_CI_AS)))		
			/*,ARPDisplayName00*/
			,convert(varchar(48), convert(varbinary(48), convert(varchar(48), ProductVersion00 collate SQL_Latin1_General_CP1_CI_AS)))
			,convert(varchar(255), convert(varbinary(255), convert(varchar(255), Publisher00 collate SQL_Latin1_General_CP1_CI_AS)))		
			/*,VersionMajor00,VersionMinor00*/
			,convert(varchar(16), convert(varbinary(16), convert(varchar(16), ServicePack00 collate SQL_Latin1_General_CP1_CI_AS)))		
			/*,Language00,ProductID00, InstalledLocation00,InstallSource00,UninstallString00,LocalPackage00,UpgradeCode00*/,
			InstallDate00/*,RegisteredUser00,SoftwarePropertiesHash00,SoftwarePropertiesHashEx00*/
		FROM [BUZULUK-MAN01].[SMS_005].[dbo].[INSTALLED_SOFTWARE_DATA]
		WHERE MachineId in (SELECT ResourceId FROM RawSystems)	
		
		
		--FROM 							
		--	OPENQUERY([BUZULUK-MAN01], 
		--		'SELECT MachineId, InstanceKey
		--		 FROM [SMS_005].[dbo].[INSTALLED_SOFTWARE_DATA]')
		
		
		
	COMMIT TRANSACTION;
	
	--SET @Result = 1;
	
	--BEGIN TRANSACTION;
	--	INSERT INTO Softwares(Name,Comment)
	--	SELECT DISTINCT ProductName00 Name, '��������� �� MS SMS (�� ��������)' Comment
	--	FROM RawInstalledSoftwares 
	--	WHERE ProductName00 is not null AND 
	--		LTRIM(RTRIM(ProductName00)) NOT IN (SELECT LTRIM(RTRIM(Name)) FROM Softwares)

	--	INSERT INTO Contractors(Name,Comment)
	--	SELECT DISTINCT Publisher00 Name, '��������� �� MS SMS (�� ��������)' Comment
	--	FROM RawInstalledSoftwares 
	--	WHERE Publisher00 is not null AND 
	--		LTRIM(RTRIM(Publisher00)) NOT IN (SELECT LTRIM(RTRIM(Name)) FROM Contractors);
	--COMMIT TRANSACTION;
	
	--BEGIN TRANSACTION;
	--	UPDATE InstalledSoftwares SET IsActive = 0;
		
	--	MERGE INTO InstalledSoftwares AS target
	--	USING (
	--		SELECT inst.MakeDate,inst.MachineId,inst.InstanceKey,soft.SoftwareId,inst.ProductVersion00,cont.ContractorId,
	--			inst.ServicePack00,inst.InstallDate00
	--		FROM RawInstalledSoftwares inst 
	--			left join Softwares soft ON LOWER(LTRIM(RTRIM(inst.ProductName00))) = LOWER(LTRIM(RTRIM(soft.Name)))	
	--			left join Contractors cont ON LOWER(LTRIM(RTRIM(inst.Publisher00))) = LOWER(LTRIM(RTRIM(cont.Name)))
	--	) AS source(MakeDate,MachineId,InstanceKey,SoftwareId,ProductVersion,ContractorId,ServicePack,InstallDate)
	--	ON (target.ResourceId = source.MachineId AND target.InstanceKey = source.InstanceKey /*AND
	--		target.SoftwareId = source.SoftwareId AND target.ContractorId = source.ContractorId*/)
	--	WHEN MATCHED THEN UPDATE SET IsActive = 1
	--	WHEN NOT MATCHED BY TARGET THEN INSERT(MakeDate,ResourceId,InstanceKey,SoftwareId,ProductVersion,ContractorId,ServicePack,InstallDate,IsActive)
	--		VALUES(MakeDate,MachineId,InstanceKey,SoftwareId,ProductVersion,ContractorId,ServicePack,InstallDate,1);						
	
	--	INSERT ContractorSoftwares(OriginalContractorId, OriginalSoftwareId, SoftwareTypeId)
	--	SELECT query.ContractorId, query.SoftwareId, 1 SoftwareTypeId
	--	FROM (SELECT inst.ContractorId, inst.SoftwareId
	--		  FROM (SELECT DISTINCT ContractorId, SoftwareId FROM InstalledSoftwares WHERE IsActive = 1) inst
	--				EXCEPT
	--		  SELECT OriginalContractorId ContractorId, OriginalSoftwareId SoftwareId
	--		  FROM ContractorSoftwares) query;			  
	--COMMIT TRANSACTION;
	
END
GO		