
use ITManagement
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                                                    
-- =============================================

-- Description:	Update data from SUL 
-- =============================================
alter PROCEDURE [dbo].[UpdateCheckedContSoft] 	
AS
BEGIN
	DECLARE @CS_Id int, @SGenerContr nvarchar(240), @SGenerSoft nvarchar(240);
	DECLARE @IGenerContr int, @IGenerSoft int;
	
	DECLARE @SGenerType nvarchar(30), @IGenerType int;		

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

--1Phase : Updata GeneralContractor & GeneralSoftware
	BEGIN TRANSACTION;
		DECLARE c CURSOR FOR 			 									
			SELECT CS.ContractorSoftwareId
					,CSS.PUBLISHER_CHECKED GenerContractor
					,CSS.PRODUCT_CHECKED GenerSoftware
			FROM ContractorSoftwares CS
			left outer join Contractors OrigContr
			on cs.OriginalContractorId=OrigContr.ContractorId
			left outer join Softwares OrigSoft
			on cs.OriginalSoftwareId=OrigSoft.SoftwareId
			left outer join [CSS]..[CSS].[SUL_VR_SOFT] CSS --RawSul_VR_Soft CSS 
			---select rn,p1,p3,p2,p4,p5 from PODGOT
			on (upper(OrigContr.Name)=
			convert(varchar(255), convert(varbinary(255), convert(varchar(255), upper(CSS.PUBLISHER) collate SQL_Latin1_General_CP1_CI_AS))))
				and (upper(OrigSoft.Name)=
				convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(CSS.PRODUCT) collate SQL_Latin1_General_CP1_CI_AS))))
			where ( CSS.[PUBLISHER_CHECKED] is not null or CSS.[PRODUCT_CHECKED] is not null)
					and (cs.GeneralContractorId is null or cs.GeneralSoftwareId is null) ; 		        			
		OPEN c;
	
		IF @@CURSOR_ROWS > 0 
				
		FETCH NEXT FROM c 		
			INTO @CS_Id, @SGenerContr, @SGenerSoft;			     
			     
		WHILE @@FETCH_STATUS=0 BEGIN	
				     
		IF (@SGenerContr is not null) BEGIN	
		
			select @IGenerContr = ContractorId  from Contractors
			where UPPER(name) = upper(@SGenerContr);
			
			IF (@IGenerContr is null) BEGIN
			
			INSERT INTO Contractors(Name,Comment)
				VALUES(@SGenerContr, '������������� ��� ������������� ������ �� SUL');
			SELECT @IGenerContr = @@IDENTITY;			
			END;
		END;
		
		IF (@SGenerSoft is not null) BEGIN	
		
			select @IGenerSoft = SoftwareId  from Softwares
			where UPPER(name) = upper(@SGenerSoft);
			
			IF (@IGenerSoft is null) BEGIN
			
			INSERT INTO Softwares(Name,Comment)
				VALUES(@SGenerSoft, '������������� ��� ������������� ������ �� SUL');
			SELECT @IGenerSoft = @@IDENTITY;			
			END;
		END;		

			
		UPDATE ContractorSoftwares
		--UPDATE Test_CS
			SET GeneralContractorId = isnull(@IGenerContr,null)
			   ,GeneralSoftwareId = isnull(@IGenerSoft,null)
		WHERE ContractorSoftwareId = @CS_Id ; 

			
			SET @CS_Id = null; SET @SGenerContr = null;SET @SGenerSoft = null;	
			SET @IGenerSoft = null; SET @IGenerContr = null;	
	
			FETCH NEXT FROM c 
				INTO @CS_Id, @SGenerContr, @SGenerSoft;
		END;
    
		CLOSE c;
		DEALLOCATE c;
	COMMIT TRANSACTION;
		
	--2Phase : Update SoftwareType	
	BEGIN TRANSACTION;		
		DECLARE s CURSOR FOR 			 									
			SELECT CS.ContractorSoftwareId					
					,CSS.SOFT_TYPE GenerType
			FROM ContractorSoftwares CS
			left outer join Contractors OrigContr
			on cs.OriginalContractorId=OrigContr.ContractorId
			left outer join Softwares OrigSoft
			on cs.OriginalSoftwareId=OrigSoft.SoftwareId
			left outer join [ITManagement].[dbo].[SoftwareTypes] SoftType
			on cs.SoftwareTypeId=SoftType.SoftwareTypeId
			left outer join [CSS]..[CSS].[SUL_VR_SOFT] CSS
			on (upper(OrigContr.Name)=
			    convert(varchar(255), convert(varbinary(255), convert(varchar(255), upper(CSS.PUBLISHER) collate SQL_Latin1_General_CP1_CI_AS))))
			and (upper(OrigSoft.Name)=
				convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(CSS.PRODUCT) collate SQL_Latin1_General_CP1_CI_AS))))
			where CSS.SOFT_TYPE is not null
			and upper(rtrim(ltrim(SoftType.Name)))
			      --convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(rtrim(ltrim(SoftType.Name))) collate SQL_Latin1_General_CP1_CI_AS)))
			      != 
			      convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(rtrim(ltrim(CSS.SOFT_TYPE))) collate SQL_Latin1_General_CP1_CI_AS)));
			        			
		OPEN s;
		
		IF @@CURSOR_ROWS > 0 
				
		FETCH NEXT FROM s 
			INTO @CS_Id, @SGenerType;
			     
		WHILE @@FETCH_STATUS=0 BEGIN		
				
		IF (@SGenerType is not null) BEGIN	
		
			select @IGenerType = SoftwareTypeId  from SoftwareTypes
			where UPPER(name) = upper(@SGenerType);
			
			IF (@IGenerType is null) BEGIN
			
			INSERT INTO SoftwareTypes(Name,Code,IsNeedLicense)
				VALUES(@SGenerType, '������������� ��� ������������� ������ �� SUL',0);
			SELECT @IGenerType = @@IDENTITY;			
			END;
		END;
			
		UPDATE ContractorSoftwares
--		UPDATE Test_CS
			SET SoftwareTypeId = isnull(@IGenerType,null)
		WHERE ContractorSoftwareId = @CS_Id ; 

			
			SET @CS_Id = null;SET @SGenerType = null;SET @IGenerType = null; 			
	
			FETCH NEXT FROM s 
				INTO @CS_Id, @SGenerType;
			     
		END;
		
		CLOSE s;
		DEALLOCATE s;
		
	COMMIT TRANSACTION;

END;

