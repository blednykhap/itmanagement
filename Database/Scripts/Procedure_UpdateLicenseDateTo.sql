USE [ITManagement]
GO

/****** Object:  StoredProcedure [dbo].[UpdateLicenseContSoft]    Script Date: 08/23/2013 12:02:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================

-- Description:	Update data from SUL 
-- =============================================
/*Create*/ CREATE PROCEDURE [dbo].[UpdateLicenseDateTo] 	
AS
BEGIN
	DECLARE @License_Id int, @DDateTo datetime;	

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

--1Phase : Updata GeneralContractor & GeneralSoftware
	BEGIN TRANSACTION;
		DECLARE c CURSOR FOR 			 									
		select l.LicenseId /*,l.Code,li.name,*/
		        ,isnull(li.date_to,CONVERT(datetime, '31.12.9999', 104))
		 from Licenses l
		left outer join [CSS]..[CSS].[SUL_TR_LICENSES] li		
		on convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(rtrim(ltrim(l.Code))) collate SQL_Latin1_General_CP1_CI_AS)))
		 = convert(varchar(255), convert(varbinary(255), convert(varchar(255),upper(rtrim(ltrim(li.name))) collate SQL_Latin1_General_CP1_CI_AS)))
		where l.Code is not null;	        			
		OPEN c;
	
		IF @@CURSOR_ROWS > 0 
				
		FETCH NEXT FROM c 		
			INTO @License_Id, @DDateTo;			     
			     
		WHILE @@FETCH_STATUS=0 BEGIN		     

		UPDATE Licenses
		--UPDATE Test_Licenses
			SET DateTo = @DDateTo
		WHERE LicenseId = @License_Id ; 

			
			SET @License_Id = null; SET @DDateTo = null; 
	
			FETCH NEXT FROM c 
				INTO @License_Id, @DDateTo
		END;
    
		CLOSE c;
		DEALLOCATE c;
	COMMIT TRANSACTION;
END;


GO


