USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 30.10.2013
-- Description:	Report
-- =============================================
ALTER PROCEDURE [dbo].[ReportR24HoursMaintenance](@period int)
AS
BEGIN   	
	SELECT m.Name Maintenance, e.Authid, e.Fullname Employee
	FROM MaintenanceOrders o, Maintenances m, CompanyStructures s, Employees e
	WHERE o.MaintenanceId = m.MaintenanceId AND o.CompanyStructureId = s.CompanyStructureId 
		AND s.EmployeeId = e.EmployeeId AND m.Is24Hours = 1 
		AND @period between o.CodeMonthFrom ANd o.CodeMonthTo;
END
GO	

