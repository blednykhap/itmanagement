USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 10.07.2013
-- Description:	Load Maintenance From BMC Remedy
-- =============================================
alter PROCEDURE [dbo].[LoadDepartment]/* @Result bit OUTPUT*/	
AS
BEGIN   
	DECLARE @IntCompanyId int; --, @EKC nvarchar(500);	
	DECLARE @ExtDepartmentNames nvarchar(4000), @ExtDepartmentCodes nvarchar(1000);
	DECLARE @ExtDepartmentName nvarchar(500), @ExtDepartmentCode nvarchar(100);
	  
	DECLARE @IntDepartmentId int, @IntParentDepartmentId int;
	DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	

	DECLARE @IntCompanyStructureId int, @IntParentCompanyStructureId int;

	SET NOCOUNT ON;

	--SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'CORP\orn_service_ulu';

	/*SET @Result = 0;*/

	BEGIN TRANSACTION;

		DECLARE c CURSOR FOR 	
			SELECT DISTINCT RawDepartmentNames, RawDepartmentCodes, CompanyId
			FROM RawEmployees		 					
		OPEN c;

		IF @@CURSOR_ROWS > 0 
		BEGIN			
			--INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
			--	VALUES(GETDATE(), @SysEmployeeId, '�������� ���������� (�������������) �� ���');
			--SELECT @LogAppEventId = @@IDENTITY;

			UPDATE Departments SET IsActive = 0 WHERE CreatedBy = 'EKC'; 
			UPDATE CompanyStructures SET IsActive = 0 WHERE CreatedBy = 'EKC'; 
		END;

		FETCH NEXT FROM c INTO @ExtDepartmentNames, @ExtDepartmentCodes, @IntCompanyId; --, @EKC;

		WHILE @@FETCH_STATUS = 0 BEGIN
		
			SET @IntParentDepartmentId = NULL;
			SET @IntParentCompanyStructureId = NULL;

			DECLARE d CURSOR FOR			
				SELECT t_code.code, t_name.name FROM 
					(SELECT (zeroBasedOccurance+1) id, s code 
					 FROM dbo.SplitString(@ExtDepartmentCodes, ':')) t_code,
					(SELECT (zeroBasedOccurance+1) id, s name
					 FROM dbo.SplitString(@ExtDepartmentNames, ':')) t_name
				WHERE t_code.id = t_name.id;

			OPEN d;
			
			FETCH NEXT FROM d INTO @ExtDepartmentCode, @ExtDepartmentName;

			WHILE @@FETCH_STATUS = 0 BEGIN
				
				SELECT @IntDepartmentId = DepartmentId FROM Departments WHERE Code = @ExtDepartmentCode;
				
				IF @IntDepartmentId IS NULL BEGIN
					INSERT INTO Departments(ParentDepartmentId, CompanyId, Code, Name, Comment, IsActive, CreatedBy) 
						VALUES(@IntParentDepartmentId, @IntCompanyId, @ExtDepartmentCode, @ExtDepartmentName, '�������� ���������� �� ���', 1, 'EKC');
					SELECT @IntDepartmentId = @@IDENTITY;										
					--INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					--	VALUES(@LogAppEventId, 'LOAD_EKC', 'Departments', @IntDepartmentId, 'INSERT', '��������� ��������: Name=' + @ExtDepartmentName);
				
					INSERT INTO CompanyStructures(ParentCompanyStructureId, DepartmentId, CompanyId, IsActive, CreatedBy)
						VALUES(@IntParentCompanyStructureId, @IntDepartmentId, @IntCompanyId, 1, 'EKC');
					SELECT @IntCompanyStructureId = @@IDENTITY;					
					--INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					--	VALUES(@LogAppEventId, 'LOAD_EKC', 'MaintenanceObjectUnits', @IntMaintenanceObjectId, 'INSERT', '��������� ��������: Name=' + @ExtDepartmentName);									
				END
				ELSE BEGIN					
					SELECT @IntCompanyStructureId = CompanyStructureId FROM CompanyStructures
						WHERE DepartmentId = @IntDepartmentId;

					UPDATE Departments SET IsActive = 1 WHERE DepartmentId = @IntDepartmentId;
					UPDATE CompanyStructures SET IsActive = 1 WHERE CompanyStructureId = @IntCompanyStructureId;
				END;

				SET @IntParentDepartmentId = @IntDepartmentId;
				SET @IntParentCompanyStructureId = @IntCompanyStructureId;
			
				SET @ExtDepartmentName = NULL;		SET @ExtDepartmentCode = NULL;
				SET @IntDepartmentId = NULL;		SET @IntCompanyStructureId = NULL;

				FETCH NEXT FROM d INTO @ExtDepartmentCode, @ExtDepartmentName;
			END;

			CLOSE d;
			DEALLOCATE d;

			SET @ExtDepartmentNames = NULL;		SET @IntCompanyId = NULL;	SET @ExtDepartmentCodes = NULL;	

			FETCH NEXT FROM c INTO @ExtDepartmentNames, @ExtDepartmentCodes, @IntCompanyId;
		END;
		
		CLOSE c;
		DEALLOCATE c;

	COMMIT TRANSACTION;
	
	/*SET @Result = 1;*/

END
GO		

-- EXAMPLE		
--use ITManagement
--go				
--	SELECT t_code.id, t_code.code, t_name.name FROM 
--		(SELECT (zeroBasedOccurance+1) id, s code 
--		 FROM dbo.SplitString('0:50000100:50000231:50012876:50015698:50000147:50013331:50013751',':')) t_code,
--		(SELECT (zeroBasedOccurance+1) id, s name
--		 FROM dbo.SplitString('SAP HR:������� ���:����������� / Upstream:���-��������� ��������:��� �������������:��� "�������������":������� ����������:�����������',':')) t_name
--	WHERE t_code.id = t_name.id;

--USE [ITManagement]
--GO
--EXEC [dbo].[LoadDepartment]
			
			
			
			
			