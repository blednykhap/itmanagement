USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 09.08.2013
-- Description:	Load System From MS SMS (system)
-- =============================================
alter PROCEDURE [dbo].[LoadSystem] @Result bit OUTPUT
AS
BEGIN   
	--DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	

	SET NOCOUNT ON;
	SET @Result = 0;
	
	BEGIN TRANSACTION;
	
		--SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'ro_sms05';
		
		--INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
		--	VALUES(GETDATE(), @SysEmployeeId, '�������� �������������� �� �� SMS (�������)');
		--SELECT @LogAppEventId = @@IDENTITY;

		--DELETE FROM RawSystems 
		--WHERE LOWER('corp\'+User_Name0) NOT IN (SELECT LOWER(Authid) FROM Employees WHERE IsActive = 1)

		--UPDATE RawSystems SET 
		--	EmployeeId = (SELECT EmployeeId FROM Employees WHERE LOWER(Authid) = LOWER(User_Domain0 + '\' +User_Name0));
		
		UPDATE ComputerSystems SET IsActive = 0;
		--select * from ComputerSystems

		--select * from RawSystems where User_Name0 = 'CORP\avvoronin' -- 19736
		--select * from ComputerSystems where EmployeeId = 19736

		MERGE INTO ComputerSystems AS Target
		USING (SELECT ResourceId,AgentTime,User_Domain0,User_Name0,EmployeeId,Netbios_Name0,
					Operating_System_Name_and0 
			   FROM RawSystems) AS source (ResourceId,AgentTime,User_Domain0,User_Name0,EmployeeId,Netbios_Name0,
					Operating_System_Name_and0)
		ON (target.ResourceId = source.ResourceId)
		WHEN MATCHED THEN UPDATE SET target.EmployeeId = source.EmployeeId,
			target.NetbiosName = source.Netbios_Name0, target.OperatingSystem = source.Operating_System_Name_and0, 
			target.IsActive = 1
		WHEN NOT MATCHED BY TARGET THEN 		
			INSERT(MakeDate,ResourceId,AgentTime,EmployeeId,NetbiosName,OperatingSystem,IsActive)
			VALUES(GETDATE(),ResourceId,AgentTime,EmployeeId,Netbios_Name0,Operating_System_Name_and0,1);

	COMMIT TRANSACTION;
	SET @Result = 1;
END
GO		