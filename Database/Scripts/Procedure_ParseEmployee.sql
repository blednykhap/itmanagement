use ITManagement
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                                                    
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 30.08.2013
-- Description:	Parse Data Which Was Getting From EKC
-- =============================================
ALTER PROCEDURE [dbo].[ParseEmployee] /*@Result bit OUTPUT*/
AS
BEGIN

	/*SET @Result = 0;*/
	
	BEGIN TRANSACTION;
	
		-- Add new positions
		INSERT INTO Positions(Name, Comment)
			(SELECT LTRIM(RTRIM(RawPosition)) Name, '�������� ���������� �� ��� (�� ��������)' Comment FROM RawEmployees
				EXCEPT
			 SELECT LTRIM(RTRIM(Name)) Name, '�������� ���������� �� ��� (�� ��������)' Comment FROM Positions);

		-- Remove ZERO's at the begining
		UPDATE RawEmployees SET RawCostCenter = CAST(CAST(RawCostCenter AS int) AS nvarchar)
		WHERE ISNUMERIC(RawCostCenter) = 1;

		-- Add new cost centers
		INSERT INTO CostCenters(Code)
			(SELECT LTRIM(RTRIM(RawCostCenter)) Code FROM RawEmployees WHERE RawCostCenter != NULL
				EXCEPT
			 SELECT LTRIM(RTRIM(Code)) Code FROM CostCenters);

		UPDATE RawEmployees SET 
			EmployeeId = (SELECT EmployeeId FROM Employees WHERE Authid = RawAuthid),
			CostCenterId = (SELECT CostCenterId FROM CostCenters WHERE Code = RawCostCenter),
			PositionId = (SELECT PositionId FROM Positions WHERE Name = RawPosition);

	COMMIT TRANSACTION;
	
	/*SET @Result = 1;*/
	
END;
