USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 24.09.2013
-- Description:	Load Maintenances From AD
-- =============================================
ALTER PROCEDURE [dbo].[LoadADMaintenance] @Result bit OUTPUT
AS
BEGIN   	

	SET NOCOUNT ON;

	SET @Result = 0;
	
	BEGIN TRANSACTION;

		INSERT INTO MaintenanceOrders(MakeDate, MaintenanceId, CompanyId, CompanyStructureId, ComponentId,
			Total, CodeMonthFrom, CodeMonthTo, Comment)
		SELECT GETDATE() MakeDate, r.MaintenanceId, r.CompanyId, r.CompanyStructureId, r.ComponentId,
			0 Total, r.CodeMonthFrom, r.CodeMonthTo, '�������� ���������� �� AD' Comment
		FROM (SELECT DISTINCT MaintenanceId, CompanyId, CompanyStructureId, ComponentId, CodeMonthFrom, CodeMonthTo
			  FROM RawADMaintenances) r;
			  
		DELETE FROM RawADMaintenances;

	COMMIT TRANSACTION;

	SET @Result = 1;
	
END
GO