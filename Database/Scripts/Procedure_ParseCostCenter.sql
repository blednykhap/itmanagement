USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 26.08.2013
-- Description:	Parse CostCenter
-- =============================================
ALTER PROCEDURE [dbo].[ParseCostCenter]
--ALTER PROCEDURE [dbo].[ParseSAPMaintenance]	@result = 0
AS
BEGIN   
	--DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	
	--DECLARE @MaintenanceId int;

	SET NOCOUNT ON;

	UPDATE RawCostCenters 
	SET CostCenterId = 
			(SELECT CostCenterId FROM CostCenters
			 WHERE UPPER(LTRIM(RTRIM(Code))) = UPPER(LTRIM(RTRIM(ExcelCode))));

	--UPDATE RawCostCenters 
	--SET CostCenterId = 
	--		(SELECT CostCenterId FROM CostCenters
	--		 WHERE 
	--			CASE WHEN ISNUMERIC(Code) = 1 THEN CONVERT(NVARCHAR,CONVERT(int, Code)) ELSE NULL END =
	--			CASE WHEN ISNUMERIC(ExcelCode) = 1 THEN CONVERT(NVARCHAR,CONVERT(int, ExcelCode)) ELSE NULL END);

	return 1;
END
GO		