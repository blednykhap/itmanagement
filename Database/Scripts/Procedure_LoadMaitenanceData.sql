USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 10.07.2013
-- Description:	Load Maintenance From BMC Remedy
-- =============================================
ALTER PROCEDURE [dbo].[LoadMaintenanceData]	
AS
BEGIN
	DECLARE @ExtMaintenanceId int, @ExtAuthid nvarchar(100);
	DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;
	DECLARE @IntEmployeeId int, @IntCompanyId int, @IntMaintenanceObjectUnitId int, @IntMaintenanceCalcUnitId int;
	DECLARE @DateFrom datetime, @DateTo datetime, @IntEmployeeDischargeDate datetime;

	SET NOCOUNT ON;

	SELECT @SysEmployeeId = EmployeeId 
	FROM Employees
	WHERE Authid = 'CORP\orn_service_ulu';

	INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
	VALUES(GETDATE(), @SysEmployeeId, '�������� ���������� �� Active Directory');
	SELECT @LogAppEventId = @@IDENTITY;

	SELECT @DateFrom = CONVERT(datetime, STR(YEAR(GETDATE()))+'-'+STR(MONTH(GETDATE()))+'-01 00:00:00', 120);

	-- SELECT @DateTo = DATEADD(DD, -1, DATEADD(MM, 1, CONVERT(datetime, STR(YEAR(GETDATE()))+'-'+STR(MONTH(GETDATE()))+'-01 23:59:59', 120)));

    DECLARE c CURSOR FOR 
	SELECT MaintenanceId, Authid FROM LoadADMaintenances;
	OPEN c;

	FETCH NEXT FROM c INTO @ExtMaintenanceId, @ExtAuthid;

	WHILE @@FETCH_STATUS = 0 BEGIN
		
		SELECT @IntEmployeeId = EmployeeId, @IntCompanyId = CompanyId, @IntEmployeeDischargeDate = DischargeDate
		FROM Employees
		WHERE UPPER(Authid) = UPPER('CORP\' + @ExtAuthid);

		IF (@IntEmployeeId is not null) BEGIN
			
			SELECT @IntMaintenanceObjectUnitId = MaintenanceObjectUnitId 
			FROM MaintenanceObjectUnits
			WHERE EmployeeId = @IntEmployeeId;

			SELECT @IntMaintenanceCalcUnitId = MaintenanceCalcUnitId 
			FROM MaintenanceCalcUnits
			WHERE MaintenanceObjectUnitId = @IntMaintenanceObjectUnitId AND MaintenanceId = @ExtMaintenanceId 
				AND GETDATE() BETWEEN DateFrom AND DateTo;

			IF (@IntMaintenanceCalcUnitId is null) BEGIN
				INSERT INTO MaintenanceCalcUnits(DateFrom, DateTo, MaintenanceObjectUnitId, MaintenanceId, CompanyId)
				VALUES(@DateFrom, @IntEmployeeDischargeDate, @IntMaintenanceObjectUnitId, @ExtMaintenanceId, @IntCompanyId);
				--VALUES(@DateFrom, @DateTo, @IntMaintenanceObjectUnitId, @ExtMaintenanceId, @IntCompanyId);
				SET @IntMaintenanceCalcUnitId = @@IDENTITY;
				
				INSERT INTO LogDataChanges(LogAppEventId, OperationType, TargetTable, TargetId, DML, Comment)
					VALUES(@LogAppEventId, 'LOAD_ACTIVEDIRECTORY', 'MaintenanceCalcUnits', @IntMaintenanceCalcUnitId, 'INSERT', '��������� �������� ���: Authid=' + @ExtAuthid);				
			END;
		END;

		SET @ExtMaintenanceId = NULL;	SET @ExtAuthid = NULL;		SET @IntMaintenanceObjectUnitId = NULL; 
		SET @IntEmployeeId = NULL;		SET @IntCompanyId = NULL;	SET @IntMaintenanceCalcUnitId = NULL;
		SET @IntEmployeeDischargeDate = NULL;		

		FETCH NEXT FROM c INTO @ExtMaintenanceId, @ExtAuthid;
	END;

	CLOSE c;
	DEALLOCATE c;

END
GO