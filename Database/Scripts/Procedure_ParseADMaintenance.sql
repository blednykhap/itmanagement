USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 24.09.2013
-- Description:	Parse Maintenances From AD
-- =============================================
ALTER PROCEDURE [dbo].[ParseADMaintenance] @Result bit OUTPUT
--ALTER PROCEDURE [dbo].[ParseSAPMaintenance]	@result = 0
AS
BEGIN   	

	SET NOCOUNT ON;
	
	SET @Result = 0;

	BEGIN TRANSACTION;

		UPDATE RawADMaintenances 
		SET RawAuthid = 'CORP\' + RawAuthid
		WHERE UPPER(SUBSTRING(RawAuthid, 1, 4)) != 'CORP';

		UPDATE RawADMaintenances 
		SET 
			EmployeeId = (SELECT EmployeeId FROM Employees 
				WHERE UPPER(Authid) = UPPER(RawAuthid) AND CreatedBy = 'EKC' AND IsActive = 1),
			CompanyId = (SELECT CompanyId FROM Employees WHERE UPPER(Authid) = UPPER(RawAuthid)),
			ComponentId = (SELECT ComponentId FROM Components WHERE UPPER(Code) = UPPER('ACC')),
			CompanyStructureId = (SELECT c.CompanyStructureId FROM CompanyStructures c, Employees e 
				WHERE c.EmployeeId = e.EmployeeId AND UPPER(e.Authid) = UPPER(RawAuthid)),
			CodeMonthFrom = dbo.CodeMonthByDate(GETDATE()),
			CodeMonthTo = (SELECT dbo.CodeMonthByDate(DischargeDate) FROM Employees 
				WHERE UPPER(Authid) = UPPER(RawAuthid))
		WHERE GETDATE() < (SELECT DischargeDate FROM Employees 
			WHERE UPPER(Authid) = UPPER(RawAuthid) AND CreatedBy = 'EKC' AND IsActive = 1);
		
		DELETE FROM RawADMaintenances WHERE EmployeeId is null;
		
		DELETE FROM RawADMaintenances 
		WHERE 1 = (SELECT COUNT(MaintenanceOrderId) FROM MaintenanceOrders o, CompanyStructures s
				   WHERE o.CompanyStructureId = s.CompanyStructureId 
						AND o.MaintenanceId = RawADMaintenances.MaintenanceId AND s.EmployeeId = RawADMaintenances.EmployeeId 
						AND dbo.CodeMonthByDate(GETDATE()) BETWEEN o.CodeMonthFrom AND o.CodeMonthTo);

	COMMIT TRANSACTION;

	SET @Result = 1;
	
END
GO		

--DELETE FROM MaintenanceOrders
--DELETE FROM RawADMaintenances
--select * from RawADMaintenances

--select * from RawADMaintenances
--WHERE 1 in (SELECT 1 FROM MaintenanceOrders o, CompanyStructures s
--				   WHERE o.CompanyStructureId = s.CompanyStructureId 
--						AND o.MaintenanceId = MaintenanceId AND s.EmployeeId = EmployeeId 
--						AND dbo.CodeMonthByDate(GETDATE()) BETWEEN o.CodeMonthFrom AND o.CodeMonthTo)

--SELECT O.MaintenanceId, s.EmployeeId, COUNT(O.MaintenanceId), COUNT(s.EmployeeId)
--FROM MaintenanceOrders o, CompanyStructures s
--WHERE o.CompanyStructureId = s.CompanyStructureId 
--	--AND o.MaintenanceId = MaintenanceId AND s.EmployeeId = EmployeeId 
--	--AND dbo.CodeMonthByDate(GETDATE()) BETWEEN o.CodeMonthFrom AND o.CodeMonthTo
--GROUP BY O.MaintenanceId, s.EmployeeId
--HAVING COUNT(O.MaintenanceId) > 1 AND COUNT(s.EmployeeId) > 1

--select COUNT(*) from MaintenanceOrders
--delete from MaintenanceOrders

--select CompanyStructureId, MaintenanceId, count(CompanyStructureId), count(MaintenanceId)
--from MaintenanceOrders
--group by CompanyStructureId, MaintenanceId
--having count(CompanyStructureId) > 1 and count(MaintenanceId) > 1

--select COUNT(*) from RawADMaintenances 2060
--select distinct MaintenanceId, EmployeeId from RawADMaintenances  --2060

--select * 
--from RawADMaintenances


--SELECT o.*
--FROM MaintenanceOrders o, CompanyStructures s
--WHERE o.CompanyStructureId = s.CompanyStructureId and
-- o.MaintenanceId = 42 AND s.EmployeeId = 19222
 
-- select * from Employees where EmployeeId = 19222

--select * from CompanyStructures where EmployeeId = 1922

--19272
--19287
--19338
--19617
--20046
--20279
--20421
--20532

--select * from CompanyStructures where EmployeeId = 20532
--delete from CompanyStructures where CompanyStructureId = 16261

--SELECT * FROM CompanyStructures WHERE EmployeeId IN
--(SELECT e.EmployeeId FROM CompanyStructures c, Employees e 
--WHERE c.EmployeeId = e.EmployeeId GROUP BY e.EmployeeId
--HAVING COUNT(e.EmployeeId) > 1)

/* SELECT e.EmployeeId, COUNT(e.EmployeeId)
FROM CompanyStructures c, Employees e 
WHERE c.EmployeeId = e.EmployeeId 
group by e.EmployeeId
having COUNT(e.EmployeeId) > 1
-- 19272	2

select * from CompanyStructures where EmployeeId = 19272
--CompanyStructureId	ParentCompanyStructureId	CompanyId	DepartmentId	EmployeeId	CreatedBy	IsActive
--14682	295	2	NULL	19272	EKC	0
--16245	289	2	NULL	19272	EKC	1 */

--SELECT * FROM RawADMaintenances -- 2058
--DELETE FROM RawADMaintenances -- 2058
--SELECT COUNT(*) FROM MaintenanceOrders -- 0