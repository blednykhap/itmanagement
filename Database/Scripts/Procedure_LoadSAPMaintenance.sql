USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 30.08.2013
-- Description:	Load Maintenances From SAP
-- =============================================
ALTER PROCEDURE [dbo].[LoadSAPMaintenance] @Result bit OUTPUT
AS
BEGIN   

	SET NOCOUNT ON;
	SET @Result = 0;
	
	BEGIN TRANSACTION;

		INSERT INTO MaintenanceOrders(MakeDate, MaintenanceId, CompanyId, CompanyStructureId, ComponentId,
			SoftwareId, Total, CodeMonthFrom, CodeMonthTo, Comment)
		SELECT GETDATE() MakeDate, MaintenanceId, CompanyId, CompanyStructureId, ComponentId, 
			SoftwareId, 0 Total, CodeMonthFrom, CodeMonthTo, '�������� ���������� �� Excel' Comment
		FROM RawSAPMaintenances
		WHERE CompanyId is not null AND MaintenanceId is not null AND ComponentId is not null AND
			SoftwareId is not null AND CompanyStructureId is not null;

		DELETE FROM RawSAPMaintenances 
		WHERE CompanyId is not null AND MaintenanceId is not null AND ComponentId is not null AND
			SoftwareId is not null AND CompanyStructureId is not null;

	COMMIT TRANSACTION;

	SET @Result = 1;	
END
GO