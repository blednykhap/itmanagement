USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 26.09.2013
-- Description:	Load Maintenances From Excel File
-- =============================================
ALTER PROCEDURE [dbo].[LoadMaintenanceByUser] @Result bit OUTPUT
AS
BEGIN   	

	SET NOCOUNT ON;

	SET @Result = 0;
	
	BEGIN TRANSACTION;

		INSERT INTO MaintenanceOrders(MakeDate, MaintenanceId, CompanyId, CompanyStructureId, ComponentId,
			SoftwareId, SoftwareModuleId, Total, CodeMonthFrom, CodeMonthTo, Comment)
		SELECT GETDATE() MakeDate, MaintenanceId, CompanyId, CompanyStructureId, ComponentId, 
			SoftwareId, SoftwareModuleId, 0 Total, CodeMonthFrom, CodeMonthTo, '�������� ���������� �� Excel' Comment
		FROM RawMaintenanceByUsers
		WHERE CompanyId is not null AND MaintenanceId is not null AND ComponentId is not null AND
			SupportLevelId is not null AND CompanyStructureId is not null;

		DELETE FROM RawMaintenanceByUsers 
		WHERE CompanyId is not null AND MaintenanceId is not null AND ComponentId is not null AND
			SupportLevelId is not null AND CompanyStructureId is not null;

	COMMIT TRANSACTION;

	SET @Result = 1;
	
END
GO