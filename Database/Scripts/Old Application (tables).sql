SELECT a.id, a.name, a.fullname, a.code
  FROM suu_act_type a;
  
SELECT a.id, a.fullname
  FROM suu_addr_streets a;
  
SELECT a.id, a.fullname, a.distance
  FROM suu_addr_town a;  

SELECT a.id, a.street_id, a.town_id, a.house, a.building, a.room
  FROM suu_address a;

SELECT a.id, a.name, a.fullname
  FROM suu_equip_brand a;

SELECT a.id, a.user_id, a.addr_id, a.equip_id, a.abonent_num,
       a.invent_num, a.date_start, a.date_end, a.status
  FROM suu_equip_card a;

SELECT a.id, a.brand_id, a.type_id, a.name, a.fullname
  FROM suu_equip_model a;

SELECT a.id, a.name, a.fullname
  FROM suu_equip_type a;

SELECT a.id, a.model_id, a.serial, a.type_num, a.created,
       a.guaranty_end, a.exp_date, a.exp_doc, a.rem_date, a.rem_doc
  FROM suu_equipment a;

SELECT a.id, a.user_login, a.user_role, a.active
  FROM suu_identity a;

SELECT a.id, a.role_name
  FROM suu_identity_roles a;

SELECT a.id, a.mvz_code, a.date_from, a.date_to, a.bossk_id, a.active
  FROM suu_mvz a;

SELECT a.id, a.mvz_id, a.identity_id, a.to_write
  FROM suu_mvz_editors a;

SELECT a.id, a.name, a.fullname, a.main_unit_id, a.mvz_id, a.bossk_id,
       a.temp_id, a.fullname1, a.active
  FROM suu_org_unit a;

SELECT a.id, a.list_id, a.accept_user_id, a.accept_date
  FROM suu_prius_accepting a;

SELECT a.id, a.catal_id, a.component_id, a.user_id, a.customer_id,
       a.custom_date, a.active, a.serv_id, a.date_end
  FROM suu_prius_add a;

SELECT a.id, a.date_del, a.active, a.id_card, a.deleter_id
  FROM suu_prius_del a;

SELECT a.id, a.author_id, a.name, a.planed_date, a.done
  FROM suu_prius_list a;

SELECT a.id, a.prius_id, a.list_id
  FROM suu_prius_list_members a;

SELECT a.id, a.user_id, a.equip_id, a.request_date, a.request_num,
       a.date_begin, a.date_end, a.num, a.active, a.serv_id, a.smod_id
  FROM suu_serv_card a;

-- ������
SELECT a.id, a.denom_id, a.line_code, a.serv_code, a.name, a.fullname,
       a.itn_id, a.line_id
  FROM suu_serv_catal a;

-- ����������
SELECT a.id, a.name, a.fullname, a.itn_id
  FROM suu_serv_component a;
  
-- ������� ��������� (�����)  
SELECT a.id, a.name, a.fullname
  FROM suu_serv_denom a;  
  
-- ������� ���������  
SELECT a.id, a.name, a.fullname, a.itn_id
  FROM suu_serv_level a;  
  
-- ��������� ����� (�����������)  
SELECT a.id, a.name, a.code, a.kr_name, a.date_del
  FROM suu_serv_line a;  
  
SELECT a.id, a.name, a.fullname, a.scatal_id, a.itn_id
  FROM suu_serv_modules a;  
  
SELECT a.id, a.serv_id, a.mvz_id, a.num, a.date_begin, a.date_end,
       a.tarif, a.compon_id, a.catal_id, a.slevel_id, a.descript,
       a.act_type_id, a.sline_id
  FROM suu_serv_others a;
  
-- ������  
SELECT a.id, a.catal_id, a.comp_id, a.itn_id, a.tarif, a.nomen,
       a.tariftype_id, a.slevel_id, a.notes
  FROM suu_service a;  
  
SELECT a.id, a.name, a.fullname, a.comments
  FROM suu_tarif_type a;    
MODULES �� ���������� ������������ ������� 
USERS   �� ���������� ������������� ������� 

SELECT a.id, a.role_id, a.user_id, a.org_unit_id, a.num, a.date_start,
       a.date_end, a.mvz_id, a.bossk_id, a.temp_id
  FROM suu_user_contract a; 
  
SELECT a.id, a.name, a.fullname, a.bossk_id
  FROM suu_user_roles a;  
  
SELECT a.id, a.login, a.lastname, a.name, a.parname, a.bday, a.email,
       a.phone, a.phone_mobile, a.active, a.itn_id, a.bossk_id,
       a.email_test, a.org_unit_xml
  FROM suu_users a;    
  
