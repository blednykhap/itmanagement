USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 09.08.2013
-- Description:	Load Installed Software From MS SMS
-- =============================================
ALTER PROCEDURE [dbo].[ParseInstalledSoftware]	@Result bit OUTPUT
AS
BEGIN   
	--DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	
	--DECLARE @IntResourceId int;

	SET NOCOUNT ON;
	SET @Result = 0;
	
	BEGIN TRANSACTION;
	
		INSERT INTO Softwares(Name, Comment)
			(SELECT DISTINCT ProductName00 Name, '��������� �� MS SMS (�� ��������)' Comment FROM RawInstalledSoftwares
				EXCEPT
			 SELECT DISTINCT Name, '��������� �� MS SMS (�� ��������)' Comment FROM Softwares)

		INSERT INTO Contractors(Name, Comment)
			(SELECT DISTINCT Publisher00 Name, '��������� �� MS SMS (�� ��������)' Comment FROM RawInstalledSoftwares
				EXCEPT
			 SELECT DISTINCT Name, '��������� �� MS SMS (�� ��������)' FROM Contractors)

		UPDATE RawInstalledSoftwares
		SET SoftwareId = (SELECT SoftwareId FROM Softwares WHERE Name = ProductName00),
			ContractorId = (SELECT ContractorId FROM Contractors WHERE Name = Publisher00);
		
	COMMIT TRANSACTION;
	SET @Result = 1;
	
END
GO		