USE [ITManagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andrey Blednykh
-- Create date: 09.08.2013
-- Description:	Load Installed Software From MS SMS
-- =============================================
alter PROCEDURE [dbo].[LoadInstalledSoftware] @Result bit OUTPUT
AS
BEGIN   
	--DECLARE @SysEmployeeId nvarchar(100), @LogAppEventId int;	
	--DECLARE @IntResourceId int;

	SET NOCOUNT ON;
	SET @Result = 0;
	
	--SELECT @SysEmployeeId = EmployeeId FROM Employees WHERE Authid = 'ro_sms05';
		
	--INSERT INTO LogAppEvents(MakeDate, EmployeeId, Comment)
	--	VALUES(GETDATE(), @SysEmployeeId, '�������� �������������� �� �� SMS');
	--SELECT @LogAppEventId = @@IDENTITY;	
	
	BEGIN TRANSACTION;
		UPDATE InstalledSoftwares SET IsActive = 0;
		
		MERGE INTO InstalledSoftwares AS target
		USING (
			SELECT MachineId,InstanceKey,SoftwareId,ProductVersion00,ContractorId,
				ServicePack00,InstallDate00
			FROM RawInstalledSoftwares
		) AS source(MachineId,InstanceKey,SoftwareId,ProductVersion,ContractorId,ServicePack,InstallDate)
		ON (target.ResourceId = source.MachineId AND target.InstanceKey = source.InstanceKey /*AND
			target.SoftwareId = source.SoftwareId AND target.ContractorId = source.ContractorId*/)
		WHEN MATCHED THEN UPDATE SET IsActive = 1
		WHEN NOT MATCHED BY TARGET THEN INSERT(MakeDate,ResourceId,InstanceKey,SoftwareId,ProductVersion,ContractorId,ServicePack,InstallDate,IsActive)
			VALUES(GETDATE(),MachineId,InstanceKey,SoftwareId,ProductVersion,ContractorId,ServicePack,InstallDate,1);						
	
		INSERT ContractorSoftwares(OriginalContractorId, OriginalSoftwareId, SoftwareTypeId)
		SELECT query.ContractorId, query.SoftwareId, 1 SoftwareTypeId
		FROM (SELECT inst.ContractorId, inst.SoftwareId
			  FROM (SELECT DISTINCT ContractorId, SoftwareId FROM InstalledSoftwares WHERE IsActive = 1) inst
					EXCEPT
			  SELECT OriginalContractorId ContractorId, OriginalSoftwareId SoftwareId
			  FROM ContractorSoftwares) query;			  
	COMMIT TRANSACTION;
	
	SET @Result = 1;
	
END
GO		