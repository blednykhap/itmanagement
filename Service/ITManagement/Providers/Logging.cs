﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ITManagement.Providers
{
    public static class Logging
    {
        public static void WriteToFile(string message)
        {
            string date = DateTime.Now.ToShortDateString().Replace(".", "");
            date = date.Replace("/", "");

            var logFile = new StreamWriter(
               Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
               + "\\TBInform\\ITManagement\\Logs\\" + date + ".log", true);
            logFile.WriteLine(DateTime.Now.ToString() + ": " + message);
            logFile.Flush();
            logFile.Close();
        }
    }
}
