﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using ITManagement.Providers;
using System.Timers;
using ITManagement.Models;
using System.DirectoryServices.AccountManagement;

namespace ITManagement
{
    public partial class ITManagement : ServiceBase
    {
        public ITManagement()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var timer = new Timer();

            try
            {
                Logging.WriteToFile("**************************************************");
                Logging.WriteToFile("Service PortalOrenburgneft was started.");

                timer.Enabled = true;
                //timer.Interval = 60000;
                timer.Interval = 180000;
                timer.Elapsed += new ElapsedEventHandler(ElapsedEvent);
                timer.AutoReset = true;
                timer.Start();
            }
            catch (Exception ex)
            {
                Logging.WriteToFile("Exception from OnStart");
                Logging.WriteToFile("Message: " + ex.Message);
                Logging.WriteToFile("Inner Exception: " + ex.InnerException);
            }
        }

        protected override void OnStop()
        {
            Logging.WriteToFile("Service PortalOrenburgneft was stopped.");
            Logging.WriteToFile("**************************************************");
        }

        private void ElapsedEvent(object sender, ElapsedEventArgs e)
        {
            Logging.WriteToFile("From ElapsedEvent");
            Preparation();

            try {
                ITManagementEntities context = new ITManagementEntities();
                
                foreach (var scheduledTask in context.ScheduledTasks.Where(p => p.IsActive == true && DateTime.Now < p.EndDate).ToList())
                {
                    //Logging.WriteToFile("From task selected: " + scheduledTask.Name);
                    //Logging.WriteToFile("Amount Of Executed Task: " + scheduledTask.ExecutedTasks.Count().ToString());

                    ExecutedTask executedTask;

                    if (scheduledTask.ExecutedTasks.Any())
                    {

                        //Logging.WriteToFile("Footprint 1.");

                        var lastExecutedTask = scheduledTask.ExecutedTasks.OrderByDescending(p => p.ExecutedDate).First();
                        if (lastExecutedTask.ExecutedDate.AddMinutes(scheduledTask.Period) < DateTime.Now)
                        {
                            //Logging.WriteToFile("Footprint 2.");
                            context.TaskExecuter(scheduledTask.Action);

                            executedTask = new ExecutedTask();
                            executedTask.ExecutedDate = DateTime.Now;
                            executedTask.ScheduledTaskId = scheduledTask.ScheduledTaskId;
                            executedTask.IsSuccessful = true;

                            //Logging.WriteToFile("Footprint 3.");
                            context.ExecutedTasks.AddObject(executedTask);
                            //Logging.WriteToFile("Footprint 4.");
                            context.SaveChanges();

                            //Logging.WriteToFile("From execution 1: " + scheduledTask.Name);
                        }
                    }
                    else
                    {
                        //Logging.WriteToFile("Footprint 5.");
                        context.TaskExecuter(scheduledTask.Action);

                        executedTask = new ExecutedTask();
                        executedTask.ExecutedDate = DateTime.Now;
                        executedTask.ScheduledTaskId = scheduledTask.ScheduledTaskId;
                        executedTask.IsSuccessful = true;

                        //Logging.WriteToFile("Footprint 6.");
                        context.ExecutedTasks.AddObject(executedTask);
                        context.SaveChanges();
                        //Logging.WriteToFile("From execution 2: " + scheduledTask.Name);
                    }
                }
            }
            catch (Exception ex) {
                Logging.WriteToFile(ex.Message);
                Logging.WriteToFile(ex.InnerException.Message);
            }
        }

        private void Preparation()
        {
            Logging.WriteToFile("From Preparation");            

            ITManagementEntities context = new ITManagementEntities();
            string[] containers;
            
            try
            {
                foreach (LoadADMaintenance loadADMaintenance in context.LoadADMaintenances.ToList())
                {
                    context.LoadADMaintenances.DeleteObject(loadADMaintenance);
                    context.SaveChanges();
                }

                var maintenances = context.Maintenances.Where(p => p.ContainerAD != null && p.ContainerAD.Trim() != "").ToList();
                var serverController = context.SystemParameters.Where(p => p.Name == "DomainServerController").FirstOrDefault();
                var rootContainer = context.SystemParameters.Where(p => p.Name == "DomainRootContainer").FirstOrDefault(); ;

                PrincipalContext principalContext =
                    new PrincipalContext(ContextType.Domain, serverController.Value, rootContainer.Value);
                GroupPrincipal groupPrincipal;

                foreach (Maintenance maintenance in maintenances)
                {
                    //Logging.WriteToFile("maintenance = " + maintenance.Name); 
                    containers = maintenance.ContainerAD.Split(';');
                    foreach (string container in containers)
                    {
                        groupPrincipal = GroupPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, container.Trim());
                        //Logging.WriteToFile("group = " + container); 
                        if (groupPrincipal != null)
                        {
                            foreach (Principal principalUser in groupPrincipal.GetMembers(true))
                            {
                                //Logging.WriteToFile("user = " + principalUser.SamAccountName); 
                                context.LoadADMaintenances.AddObject(new LoadADMaintenance { 
                                    Authid = principalUser.SamAccountName,
                                    MaintenanceId = maintenance.MaintenanceId
                                });
                                context.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.WriteToFile(ex.Message);
                Logging.WriteToFile(ex.InnerException.Message);
            }
        }
    }
}
