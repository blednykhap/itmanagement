﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{    
    public class LogErrorView
    {
        public int LogErrorId { get; set; }

        [DisplayName("Дата события")]
        public DateTime EventDate { get; set; }

        [MaxLength(100)]
        [DisplayName("Пользователь")]
        public string Authid { get; set; }

        //[MaxLength(100)]
        //[DisplayName("Компьютер")]
        //public string ComputerIP { get; set; }

        [DisplayName("Сообщение")]
        public string Message { get; set; }

        //[MaxLength(2000)]
        //[DisplayName("Внутреннее исключение")]
        //public string InnerException { get; set; }

        //[MaxLength(1000)]
        //[DisplayName("Источник")]
        //public string Source { get; set; }

        //[DisplayName("Стек")]
        //public string StackTrace { get; set; }
    }


}
