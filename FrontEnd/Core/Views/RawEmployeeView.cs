﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class RawEmployeeView
    {
        public int RawEmployeeId { get; set; }
     
        [DisplayName("Учетная запись (ЕКС)")]        
        public string RawAuthid { get; set; }

        [DisplayName("Фамилия (ЕКС)")]
        public string RawSurname { get; set; }

        [DisplayName("Имя (ЕКС)")]
        public string RawFirstname { get; set; }

        [DisplayName("Отчество (ЕКС)")]   
        public string RawMiddlename { get; set; }

        [DisplayName("ФИО (ЕКС)")]
        public string RawFullname { get; set; }

        [UIHint("Employee")]
        [DisplayName("Сотрудник (база)")]
        public string EmployeeId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания (ЕКС)")]
        public string CompanyId { get; set; }

        [DisplayName("МВЗ (ЕКС)")]        
        public string RawCostCenter { get; set; }

        [UIHint("CostCenter")]
        [DisplayName("МВЗ (база)")]
        public string CostCenterId { get; set; }

        [DisplayName("Должность (ЕКС)")]        
        public string RawPosition { get; set; }

        [UIHint("Position")]
        [DisplayName("Должность (база)")]
        public string PositionId { get; set; }

        [DisplayName("Подразделение (ЕКС)")]
        public string RawDepartmentCode { get; set; }

        [DisplayName("Путь кодов подразделений (ЕКС)")]
        public string RawDepartmentCodes { get; set; }

        [DisplayName("Путь наименований подразделений (ЕКС)")]
        public string RawDepartmentNames { get; set; }

        [UIHint("Department")]
        [DisplayName("Подразделение (база)")]
        public string DepartmentId { get; set; }

        [DisplayName("E-Mail (ЕКС)")]
        public string RawEMail { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Дата увольнения (ЕКС)")]
        public DateTime? RawDischargeDate { get; set; }
    }
}
