﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class RawCostCenterView
    {
        public int RawCostCenterId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]
        public string CompanyId { get; set; }        

        [DisplayName("Код из файла")]        
        public string ExcelCode { get; set; }

        [DisplayName("Наименование из файла")]
        //[Required(ErrorMessage = "Необходимо заполнить Наименование")]
        public string ExcelName { get; set; }

        [UIHint("CostCenter")]
        [DisplayName("Код из базы")]
        public string CostCenterId { get; set; }  

        //[DisplayName("Код из Базы")]
        ////[Required(ErrorMessage = "Необходимо заполнить Код")]
        //public string BaseCode { get; set; }

        //[MaxLength(500)]
        //[DisplayName("Наименование")]
        ////[Required(ErrorMessage = "Необходимо заполнить Наименование")]
        //public string ExcelName { get; set; }

        //[MaxLength(500)]
        //[DisplayName("Комментарий")]
        //public string Comment { get; set; }
    }
}
