﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class RawInstalledSoftwareView
    {
        public int RawInstalledSoftwareId { get; set; }

        [DisplayName("ID станции (MS SMS)")]
        public int? MachineId { get; set; }

        [DisplayName("Instance Key (MS SMS)")]
        public int? InstanceKey { get; set; }

        /*public int? InstallDirectoryValidatio0 { get; set; }

        public int? RevisionId { get; set; }
    
        public int? AgentId { get; set; }

        public DateTime? TimeKey { get; set; }

        [MaxLength(100)]
        public string SoftwareCode00 { get; set; }

        [MaxLength(100)]
        public string ProductCode00 { get; set; }

        [MaxLength(100)]
        public string CM_DSLID00 { get; set; }*/

        [DisplayName("Программное обеспечение (MS SMS)")]
        public string ProductName00 { get; set; }

        /*[MaxLength(500)]
        public string ARPDisplayName00 { get; set; }*/

        [DisplayName("Версия продукта (MS SMS)")]
        public string ProductVersion00 { get; set; }

        [DisplayName("Разработчик (MS SMS)")]
        public string Publisher00 { get; set; }

        /*public int? VersionMajor00 { get; set; }

        public int? VersionMinor00 { get; set; }*/

        [DisplayName("Сервис пак (MS SMS)")]
        public string ServicePack00 { get; set; }

        /*public int? Language00 { get; set; }

        [MaxLength(100)]
        public string ProductID00 { get; set; }

        [MaxLength(500)]
        public string InstalledLocation00 { get; set; }

        [MaxLength(500)]
        public string InstallSource00 { get; set; }

        [MaxLength(500)]
        public string UninstallString00 { get; set; }

        [MaxLength(500)]
        public string LocalPackage00 { get; set; }

        [MaxLength(100)]
        public string UpgradeCode00 { get; set; }*/

        [DataType(DataType.Date)]
        [DisplayName("Дата инсталяции (MS SMS)")]
        public DateTime? InstallDate00 { get; set; }

        /*[MaxLength(100)]
        public string RegisteredUser00 { get; set; }

        [MaxLength(100)]
        public string SoftwarePropertiesHash00 { get; set; }

        [MaxLength(100)]
        public string SoftwarePropertiesHashEx00 { get; set; }*/

        [UIHint("Software")]
        [DisplayName("Программное обеспечение (база)")]
        public string SoftwareId { get; set; }

        [UIHint("Contractor")]
        [DisplayName("Разработчик (база)")]
        public string ContractorId { get; set; }        
    }
}
