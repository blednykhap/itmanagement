﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Views
{
    public class HelpArticleListView
    {
        public PagingInfo PagingInfo { get; set; }

        public virtual IEnumerable<HelpArticle> HelpArticles { get; set; }
    }
}
