﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class CostCenterShortView
    {
        public int CostCenterId { get; set; }

        public string Name { get; set; }
    }
}
