﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class LicenseFileView
    {
        public int LicenseFileId { get; set; }

        public int LicenseId { get; set; }        

        [DisplayName("Имя файла")]
        public string FileName { get; set; }

        [DisplayName("Тип файла")]
        public string FileType { get; set; }
    }
}
