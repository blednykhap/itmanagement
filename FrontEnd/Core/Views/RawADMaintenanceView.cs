﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Views
{
    public class RawADMaintenanceView
    {
        public int RawADMaintenanceId { get; set; }

        [UIHint("Maintenance")]
        [DisplayName("Услуга (база)")]
        public string MaintenanceId { get; set; }
        
        [DisplayName("Учетная запись (AD)")]
        public string RawAuthid { get; set; }

        [UIHint("Employee")]
        [DisplayName("Сотрудник (база)")]
        public string EmployeeId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания (база)")]
        public string CompanyId { get; set; }

        [UIHint("CompanyStructure")]
        [DisplayName("Элемент структуры компании (база)")]
        public string CompanyStructureId { get; set; }

        [UIHint("Component")]
        [DisplayName("Компонента (база)")]
        public string ComponentId { get; set; }

        [UIHint("CodeMonthFrom")]
        [DisplayName("Месяц с")]
        public int? CodeMonthFrom { get; set; }

        [UIHint("CodeMonthTo")]
        [DisplayName("Месяц по")]
        public int? CodeMonthTo { get; set; }
    }
}
