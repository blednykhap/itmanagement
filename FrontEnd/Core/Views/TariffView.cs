﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{    
    public class TariffView
    {
        public int TariffId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]
        public string CompanyId { get; set; }

        [UIHint("Maintenance")]
        [DisplayName("Услуга")]
        public string MaintenanceId { get; set; }

        [UIHint("Component")]
        [DisplayName("Ком-та")]
        public string ComponentId { get; set; }

        [UIHint("SupportLevel")]
        [DisplayName("Уровень поддержки")]
        public string SupportLevelId { get; set; }

        [UIHint("Measure")]
        [DisplayName("Ед. измерения")]
        public string MeasureId { get; set; }

        [DisplayName("Цена")]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
    }
}
