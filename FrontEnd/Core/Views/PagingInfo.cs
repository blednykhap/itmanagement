﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{    
    public class PagingInfo
    {
        public int CurrentPage { get; set; }

        public int TotalItems { get; set; }

        public int ItemsPerSheet { get; set; }
    }
}
