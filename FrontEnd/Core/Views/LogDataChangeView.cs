﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class LogDataChangeView
    {
        public int LogDataChangeId { get; set; }

        //public int LogAppEventId { get; set; }        

        [MaxLength(100)]
        [DisplayName("Таблица-назначение")]
        public string OperationType { get; set; }

        [MaxLength(100)]
        [DisplayName("Таблица-назначение")]
        public string TargetTable { get; set; }

        //[DisplayName("Id данных")]
        //public int TargetId { get; set; }

        [MaxLength(500)]
        [DisplayName("Тип операции")]
        public string DML { get; set; }

        [MaxLength(2000)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
