﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class MaintenanceOrderHierarchyView
    {
        public int MaintenanceOrderId { get; set; }

        public int CompanyStructureId { get; set; }        

        [UIHint("Maintenance")]
        [DisplayName("Услуга")]
        public string MaintenanceId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]
        public string CompanyId { get; set; }

        [UIHint("Component")]
        [DisplayName("Компонента")]
        public string ComponentId { get; set; }

        [UIHint("SupportLevel")]
        [DisplayName("Уровень поддержки")]
        public string SupportLevelId { get; set; }

        [UIHint("Tariff")]
        [DisplayName("Тариф")]
        public string TariffId { get; set; }

        [UIHint("Software")]
        [DisplayName("Программное обеспечение")]
        public string SoftwareId { get; set; }

        [UIHint("SoftwareModule")]
        [DisplayName("Модуль ПО")]
        public string SoftwareModuleId { get; set; }

        [DisplayName("Месяц с")]
        [UIHint("CodeMonthFrom")]        
        public int? CodeMonthFrom { get; set; }

        [DisplayName("Месяц по")]
        [UIHint("CodeMonthTo")]
        public int? CodeMonthTo { get; set; }

        [DisplayName("Сумма")]
        [DataType(DataType.Currency)]
        public decimal? Total { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
