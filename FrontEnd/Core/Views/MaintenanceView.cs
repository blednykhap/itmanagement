﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class MaintenanceView
    {
        public int MaintenanceId { get; set; }

        [UIHint("ServiceLine")]
        [DisplayName("Линия")]
        public string ServiceLineId { get; set; }

        [DisplayName("Код")]
        public string Code { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Группы в AD (;)")]
        public string ContainerAD { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        [DisplayName("Круглосуточная")]
        public bool Is24Hours { get; set; }
    }
}
