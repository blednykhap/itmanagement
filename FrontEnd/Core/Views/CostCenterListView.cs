﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class CostCenterView
    {
        public int CostCenterId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]
        public string CompanyId { get; set; }

        [DisplayName("Код")]
        public string Code { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
