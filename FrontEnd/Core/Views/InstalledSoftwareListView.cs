﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class InstalledSoftwareListView
    {
        public PagingInfo PagingInfo { get; set; }

        public virtual IEnumerable<InstalledSoftwareView> InstalledSoftwares { get; set; }
    }
}
