﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class RawSAPMaintenanceView
    {
        public int RawSAPMaintenanceId { get; set; }

        [DisplayName("Компания (файл)")]
        public string RawCompany { get; set; }

        [DisplayName("Компания (база)")]
        [UIHint("Company")]
        public string CompanyId { get; set; }

        public string Module { get; set; }

        [DisplayName("Логин (файл)")]        
        public string RawLogin { get; set; }

        [DisplayName("ФИО из файла")]
        public string RawFullname { get; set; }
        
        [UIHint("CompanyStructure")]
        [DisplayName("Элемент структуры компании (база)")]
        public string CompanyStructureId { get; set; }

        [UIHint("Maintenance")]
        [DisplayName("Услуга (база)")]
        public string MaintenanceId { get; set; }

        [UIHint("Component")]
        [DisplayName("Компонента (база)")]
        public string ComponentId { get; set; }

        [UIHint("Software")]
        [DisplayName("Программное обеспечение")]
        public string SoftwareId { get; set; }

        [UIHint("CodeMonthFrom")]
        [DisplayName("Месяц с")]
        public int? CodeMonthFrom { get; set; }

        [UIHint("CodeMonthTo")]
        [DisplayName("Месяц по")]
        public int? CodeMonthTo { get; set; }
    }
}
