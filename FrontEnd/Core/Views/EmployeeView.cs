﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class EmployeeView
    {
        public int EmployeeId { get; set; }

        [DisplayName("Учетная запись")]        
        public string Authid { get; set; }
       
        [DisplayName("Фамилия")]
        public string Surname { get; set; }
        
        [DisplayName("Имя")]
        public string Firstname { get; set; }

        [MaxLength(100)]
        [DisplayName("Отчество")]
        public string Middlename { get; set; }

        [DisplayName("ФИО")]        
        public string Fullname { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]   
        public string CompanyId { get; set; }        

        [UIHint("CostCenter")]
        [DisplayName("МВЗ")]   
        public string CostCenterId { get; set; }

        [UIHint("Position")]
        [DisplayName("Должность")]
        public string PositionId { get; set; }

        [UIHint("Department")]
        [DisplayName("Подразделение")]
        public string DepartmentId { get; set; }

        public virtual ComputerSystem ComputerSystem { get; set; }

        [DisplayName("E-Mail")]
        public string EMail { get; set; }

        [DisplayName("Дата увольнения")]
        [DataType(DataType.Date)]
        public DateTime? DischargeDate { get; set; }

        [DisplayName("Статус")]
        public string Status { get; set; }

        [DisplayName("Активен")]
        public bool IsActive { get; set; }
    }
}
