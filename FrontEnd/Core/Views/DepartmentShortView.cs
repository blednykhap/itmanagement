﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class DepartmentShortView
    {
        public int DepartmentId { get; set; }

        public string Name { get; set; }
    }
}
