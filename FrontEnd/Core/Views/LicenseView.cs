﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Views
{
    public class LicenseView
    {
        public int LicenseId { get; set; }
        
        [DisplayName("Код")]
        public string Code { get; set; }

        [DisplayName("Действительно до")]
        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }

        [UIHint("Contractor")]
        [DisplayName("Разработчик")]
        public string ContractorId { get; set; }

        [UIHint("Software")]
        [DisplayName("Программное обеспечение")]
        public string SoftwareId { get; set; }

        [DisplayName("Кол.")]
        public int Amount { get; set; }

        [DisplayName("№ дог.")]        
        public string ContractNumber { get; set; }

        [DisplayName("Дата дог.")]
        [DataType(DataType.Date)]
        public DateTime ContractDate { get; set; }

        public ICollection<LicenseFile> LicenseFiles { get; set; }

        //public bool IsFileAttached { get; set; }

        //[DisplayName("Тип файла")]
        //public string ContractFileType { get; set; }

        //[DisplayName("Файл договора")]
        //public byte[] ContractFileContent { get; set; }
    }
}
