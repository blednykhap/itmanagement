﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class SoftwareView
    {
        public int SoftwareId { get; set; }

        [DisplayName("Программное обеспечение")]
        public string Name { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
