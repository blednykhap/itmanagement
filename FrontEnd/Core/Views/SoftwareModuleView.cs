﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class SoftwareModuleView
    {
        public int SoftwareModuleId { get; set; }

        [UIHint("Software")]
        [DisplayName("Программное обеспечение")]
        public string SoftwareId { get; set; }        

        [DisplayName("Код")]
        public string Code { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }
    }
}
