﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class MaintenanceOrderView
    {
        public int MaintenanceOrderId { get; set; }

        public int CompanyStructureId { get; set; }        

        public string MaintenanceId { get; set; }        

        public string CompanyId { get; set; }        

        public string TariffId { get; set; }        

        public string ComponentId { get; set; }        

        public string SoftwareId { get; set; }

        public string SoftwareModuleId { get; set; }

        //public DateTime? DateFrom { get; set; }

        //public DateTime? DateTo { get; set; }

        public int? CodeMonthFrom { get; set; }

        public int? CodeMonthTo { get; set; }

        public decimal? Total { get; set; }
     
        public string Comment { get; set; }
    }
}
