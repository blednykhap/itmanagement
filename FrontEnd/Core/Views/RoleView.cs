﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class RoleView
    {
        public int RoleId { get; set; }

        [DisplayName("Роль")]
        public string Name { get; set; }

        [UIHint("SecArea")]
        [DisplayName("Область")]
        public string SecAreaId { get; set; }
        
        [DisplayName("Группа AD")]
        public string GroupAD { set; get; }

    }
}
