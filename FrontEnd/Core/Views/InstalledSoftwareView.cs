﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class InstalledSoftwareView
    {
        public int InstalledSoftwareId { get; set; }

        [DisplayName("Сотрудник")]
        public string EmployeeFullname { get; set; }

        [DisplayName("Компьютер")]
        public string NetbiosName { get; set; }

        public int SoftwareId { get; set; }

        [DisplayName("Программа")]
        public string Software { get; set; }

        //[DisplayName("Разработчик")]
        //public string Contractor { get; set; }
    }
}
