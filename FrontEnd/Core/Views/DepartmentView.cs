﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class DepartmentView
    {
        public int DepartmentId { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }
        
        [UIHint("ParentDepartment")]
        [DisplayName("Родительское подразделение")]
        public string ParentDepartmentId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]
        public string CompanyId { get; set; }

        [UIHint("CostCenter")]
        [DisplayName("МВЗ")]
        public string CostCenterId { get; set; }   
    }
}
