﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class LogUserActivityView
    {
        public int LogUserActivityId { get; set; }

        [DisplayName("Дата события")]
        public DateTime EventDate { get; set; }

        [MaxLength(100)]
        [DisplayName("Учетная запись")]
        public string Authid { get; set; }

        [MaxLength(100)]
        [DisplayName("IP компьютера")]
        public string ComputerIP { get; set; }

        [DisplayName("Область")]
        public string SecArea { get; set; }

        [DisplayName("Контроллер")]
        public string SecController { get; set; }

        [DisplayName("Действие")]
        public string SecAction { get; set; }

        [MaxLength(500)]
        [DisplayName("Строка параметров")]
        public string QueryString { get; set; }

        [DisplayName("Успех доступа")]
        public bool Result { get; set; }
    }
}
