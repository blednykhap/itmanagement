﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class CompanyMaintenanceView
    {
        public int CompanyMaintenanceId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]
        public string CompanyId { get; set; }

        [UIHint("Maintenance")]
        [DisplayName("Услуга")]
        public string MaintenanceId { get; set; }

        [DisplayName("Код услуги")]
        public string MaintenanceCode { get; set; }

        [DisplayName("Сервисная линия")]
        public string ServiceLineName { get; set; }
    }
}
