﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class RawMaintenanceByUserView
    {
        public int RawMaintenanceByUserId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания (база)")]        
        public string CompanyId { get; set; }

        [DisplayName("Логин (файл)")]
        public string RawLogin { get; set; }

        [DisplayName("ФИО (файл)")]
        public string RawFIO { get; set; }

        [DisplayName("Подразделение (файл)")]
        public string RawDepartment { get; set; }

        [DisplayName("МВЗ (файл)")]
        public string RawCostCenter { get; set; }

        [DisplayName("Услуга (файл)")]
        public string RawMaintenance { get; set; }

        [DisplayName("Уровень поддержки (файл)")]
        public string RawSupportLevel { get; set; }

        [DisplayName("ПО (файла)")]
        public string RawSoftware { get; set; }

        [UIHint("CompanyStructure")]
        [DisplayName("Элемент структуры компании (база)")]
        public string CompanyStructureId { get; set; }

        [UIHint("Maintenance")]
        [DisplayName("Услуга (база)")]
        public string MaintenanceId { get; set; }

        [UIHint("Component")]
        [DisplayName("Компонента (база)")]
        public string ComponentId { get; set; }

        [UIHint("Software")]
        [DisplayName("ПО (база)")]
        public string SoftwareId { get; set; }

        [UIHint("SoftwareModule")]
        [DisplayName("Модуль ПО (база)")]
        public string SoftwareModuleId { get; set; }

        [UIHint("SupportLevel")]
        [DisplayName("Уровень поддержки (база)")]
        public string SupportLevelId { get; set; }

        [UIHint("CodeMonthFrom")]
        [DisplayName("Месяц с")]        
        public int? CodeMonthFrom { get; set; }

        [UIHint("CodeMonthTo")]
        [DisplayName("Месяц по")]
        public int? CodeMonthTo { get; set; }
    }
}
