﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class RawSystemView
    {
        public int RawSystemId { get; set; }

        [DisplayName("ID ресурса (SMS)")]
        public int? ResourceId { get; set; }

        [DisplayName("Время агента (SMS)")]
        public DateTime? AgentTime { get; set; }

        [DisplayName("Имя домена (SMS)")]
        public string User_Domain0 { get; set; }

        [DisplayName("Логин (SMS)")]
        public string User_Name0 { get; set; }

        [DisplayName("Имя компьютера (SMS)")]
        public string Netbios_Name0 { get; set; }

        [DisplayName("Операционная система (SMS)")]
        public string Operating_System_Name_and0 { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания (база)")]
        public string CompanyId { get; set; }

        [UIHint("Employee")]
        [DisplayName("Сотрудник (база)")]
        public string EmployeeId { get; set; }
    }
}
