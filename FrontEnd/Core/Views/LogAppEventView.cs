﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Views
{
    public class LogAppEventView
    {
        public int LogAppEventId { get; set; }

        [DisplayName("Дата")]
        public DateTime MakeDate { get; set; }

        [DisplayName("Пользователь")]
        public string Employee { get; set; }

        public int LogDataChanges { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        public PagingInfo PagingInfo { get; set; }

        public virtual IEnumerable<LogAppEvent> LogAppEvents { get; set; }
    }
}
