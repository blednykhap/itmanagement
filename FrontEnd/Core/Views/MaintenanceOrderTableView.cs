﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class MaintenanceOrderTableView
    {
        public int MaintenanceOrderId { get; set; }

        //public int MaintenanceObjectUnitId { get; set; }
        [UIHint("CompanyStructure")]
        [DisplayName("Элемент структуры компании")]
        public string CompanyStructureId { get; set; }

        [UIHint("Maintenance")]
        [DisplayName("Услуга")]
        public string MaintenanceId { get; set; }

        [UIHint("Company")]
        [DisplayName("Компания")]
        public string CompanyId { get; set; }

        [UIHint("Component")]
        [DisplayName("Компонента")]
        public string ComponentId { get; set; }

        [UIHint("SupportLevel")]
        [DisplayName("Уровень поддержки")]
        public string SupportLevelId { get; set; }

        [UIHint("Tariff")]
        [DisplayName("Тариф")]
        public string TariffId { get; set; }

        [UIHint("Software")]
        [DisplayName("Программное обеспечение")]
        public string SoftwareId { get; set; }

        [UIHint("SoftwareModule")]
        [DisplayName("Модуль ПО")]
        public string SoftwareModuleId { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayName("С")]
        //public DateTime? DateFrom { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayName("По")]
        //public DateTime? DateTo { get; set; }

        [DisplayName("Месяц с")]
        [UIHint("CodeMonthFrom")]
        public int? CodeMonthFrom { get; set; }

        [DisplayName("Месяц по")]
        [UIHint("CodeMonthTo")]
        public int? CodeMonthTo { get; set; }

        [DisplayName("Сумма")]
        [DataType(DataType.Currency)]
        public decimal? Total { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
