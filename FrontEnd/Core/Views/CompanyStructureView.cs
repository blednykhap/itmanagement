﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class CompanyStructureView
    {
        public int CompanyStructureId { get; set; }

        public string Name { get; set; }
    }
}
