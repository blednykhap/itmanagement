﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class CompanyView
    {
        public int CompanyId { get; set; }

        [DisplayName("Компания")]
        public string Name { get; set; }

        [DisplayName("Наименование в BMC Remedy")]
        public string BMC { get; set; }

        [DisplayName("Id в EKC")]
        public int? EKC { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
