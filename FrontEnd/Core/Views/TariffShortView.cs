﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class TariffShortView
    {
        public int TariffId { get; set; }

        public string Name { get; set; }
    }
}
