﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Views
{
    public class ContractorSoftwareView
    {
        public int ContractorSoftwareId { get; set; }

        [UIHint("OriginalContractor")]
        [DisplayName("Разработчик")]
        public string OriginalContractorId { get; set; }

        [UIHint("GeneralContractor")]
        [DisplayName("Разработчик,испр.")]
        public string GeneralContractorId { get; set; }

        [UIHint("OriginalSoftware")]
        [DisplayName("Продукт")]
        public string OriginalSoftwareId { get; set; }

        [UIHint("GeneralSoftware")]
        [DisplayName("Продукт,испр.")]
        public string GeneralSoftwareId { get; set; }

        [UIHint("SoftwareType")]
        [DisplayName("Вид ПО")]
        public string SoftwareTypeId { get; set; }
    }
}
