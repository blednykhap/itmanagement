﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Models;
using Core.Providers;
using Core.Views;


namespace Core.Filters
{
    public class LogUserActivityFilter : ActionFilterAttribute
    {
        private readonly IBaseRepository<SecArea> _rSecArea;
        private readonly IBaseRepository<SecController> _rSecController;
        private readonly IBaseRepository<SecAction> _rSecAction;
        /*private readonly IBaseRepository<Employee> _rEmployee;*/
        private readonly IBaseRepository<LogUserActivity> _rLogUserActivity;
        /*private readonly IBaseRepository<Role> _rRole;*/

        public LogUserActivityFilter(IBaseRepository<SecArea> rSecArea, IBaseRepository<SecController> rSecController,
            IBaseRepository<SecAction> rSecAction,/* IBaseRepository<Employee> rEmployee,*/
            IBaseRepository<LogUserActivity> rLogUserActivity/*, IBaseRepository<Role> rRole*/)
        {
            _rSecArea = rSecArea;
            _rSecController = rSecController;
            _rSecAction = rSecAction;
            //_rEmployee = rEmployee;
            _rLogUserActivity = rLogUserActivity;
            //_rRole = rRole;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var eDate = DateTime.Now;
            var authid = filterContext.HttpContext.Request.LogonUserIdentity.Name;
            var computerIP = filterContext.HttpContext.Request.UserHostAddress;
            var queryString = filterContext.HttpContext.Request.QueryString.ToString();
            
            //string rolename = filterContext.HttpContext.Session["RoleName"].ToString();
            //string rolename = "Admin";

            #region var area = ""
            string areaName = "";
            if (filterContext.RouteData.DataTokens["area"] != null)
            {
                areaName = filterContext.RouteData.DataTokens["area"].ToString();
            }
            SecArea area = _rSecArea.GetList(p => p.Name.ToUpper() == areaName.ToUpper()).FirstOrDefault();
            if (area == null)
            {
                _rSecArea.Create(new SecArea() { Name = areaName });
                area = _rSecArea.GetList(p => p.Name.ToUpper() == areaName.ToUpper()).FirstOrDefault();
            }
            #endregion
            #region var controller = ""
            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            SecController controller = _rSecController.GetList(p => p.Name.ToUpper() == controllerName.ToUpper()).FirstOrDefault();
            if (controller == null)
            {
                _rSecController.Create(new SecController() { Name = controllerName });
                controller = _rSecController.GetList(p => p.Name.ToUpper() == controllerName.ToUpper()).FirstOrDefault();
            }
            #endregion
            #region var action = ""
            var actionName = filterContext.ActionDescriptor.ActionName;
            SecAction action = _rSecAction.GetList(p => p.Name.ToUpper() == actionName.ToUpper()).FirstOrDefault();
            if (action == null)
            {
                _rSecAction.Create(new SecAction() { Name = actionName });
                action = _rSecAction.GetList(p => p.Name.ToUpper() == actionName.ToUpper()).FirstOrDefault();
            }
            #endregion

            _rLogUserActivity.Create(new LogUserActivity()
                        {
                            EventDate = eDate,
                            Authid = authid,
                            ComputerIP = computerIP,
                            SecAreaId = area.SecAreaId,
                            SecControllerId = controller.SecControllerId,
                            SecActionId = action.SecActionId,
                            QueryString = queryString,
                            Result = true
                        });

            #region AccessFilter 

            //var employee = _rEmployee.GetList(p => (p.Authid.ToUpper() == authid.ToUpper() && p.IsActive == true)).FirstOrDefault();
            
            //var role = _rRole.GetList(r => (r.SecArea.Name.ToUpper() == area.Name.ToUpper())).FirstOrDefault();
            //var role = _rRole.GetList(r => (r.SecAreaId == area.SecAreaId)).FirstOrDefault();

            //if (employee != null)
            //{
            //    if (area.Name == "" || (role != null && (rolename == role.Name || rolename == "Admin")))
            //    {
            //        _rLogUserActivity.Create(new LogUserActivity()
            //      {
            //          EventDate = eDate,
            //          Authid = authid,
            //          ComputerIP = computerIP,
            //          SecAreaId = area.SecAreaId,
            //          SecControllerId = controller.SecControllerId,
            //          SecActionId = action.SecActionId,
            //          QueryString = queryString /* + role.GroupAD*/,
            //          Result = true
            //      });
            //    }
            //    else
            //    {
            //        _rLogUserActivity.Create(new LogUserActivity()
            //        {
            //            EventDate = eDate,
            //            Authid = authid,
            //            ComputerIP = computerIP,
            //            SecAreaId = area.SecAreaId,
            //            SecControllerId = controller.SecControllerId,
            //            SecActionId = action.SecActionId,
            //            QueryString = queryString,
            //            Result = false
            //        });
            //        filterContext.Result = new ViewResult
            //        {
            //            ViewName = "AccessDenied"
            //        };
            //    }
            //}

            //else
            //{
            //    _rLogUserActivity.Create(new LogUserActivity()
            //                {
            //                    EventDate = eDate,
            //                    Authid = authid,
            //                    ComputerIP = computerIP,
            //                    SecAreaId = area.SecAreaId,
            //                    SecControllerId = controller.SecControllerId,
            //                    SecActionId = action.SecActionId,
            //                    QueryString = queryString,
            //                    Result = false
            //                });

            //    filterContext.Result = new ViewResult
            //    {
            //        ViewName = "AccessDenied"
            //    };
            //}

            #endregion
        }
            
    }
}
