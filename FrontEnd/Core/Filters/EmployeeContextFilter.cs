﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using Core.Models;
using Core.Providers;

namespace Core.Filters
{
    public class EmployeeContextFilter : ActionFilterAttribute
    {
        private readonly IBaseRepository<Employee> _rEmployee;        
        private readonly IBaseRepository<EmployeeProfile> _rEmployeeProfile;
        private readonly ActiveDirectoryProvider _rActiveDirectoryProvider;
        private readonly IBaseRepository<Role> _rRole;
        //private int employeeId;

        public EmployeeContextFilter(IBaseRepository<Employee> rEmployee, IBaseRepository<EmployeeProfile> rEmployeeProfile,
             ActiveDirectoryProvider rActiveDirectoryProvider, IBaseRepository<Role> rRole)
        {
            _rEmployee = rEmployee;
            _rEmployeeProfile = rEmployeeProfile;
            _rActiveDirectoryProvider = rActiveDirectoryProvider;
            _rRole = rRole;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["EmployeeId"] == null)
            {
                var authid = filterContext.HttpContext.Request.LogonUserIdentity.Name;
                var employee = _rEmployee.GetList(p => p.Authid.Trim().ToUpper() == authid.Trim().ToUpper()).FirstOrDefault();
                if (employee != null)
                {
                    //employeeId = employee.EmployeeId;
                    filterContext.HttpContext.Session["EmployeeId"] = employee.EmployeeId;

                    var employeeProfile = _rEmployeeProfile.GetList(p => p.EmployeeId == employee.EmployeeId).FirstOrDefault();
                    _rEmployee.Detach(employee);
                    if (employeeProfile != null && employeeProfile.Company != null)
                    {
                        filterContext.HttpContext.Session["PreferCompanyId"] = employeeProfile.CompanyId;
                        _rEmployeeProfile.Detach(employeeProfile);
                    }
                }
                //else
                //{
                //    employeeId = 0;
                //}
            }

            if (filterContext.HttpContext.Session["CurrentCodeMonth"] == null)
            {
                var codeMonth = DateTime.Now.Year * 12 + DateTime.Now.Month;
                filterContext.HttpContext.Session["CurrentCodeMonth"] = codeMonth;
            }

            if (filterContext.HttpContext.Session["IsDisplayActiveOnly"] == null &&
                filterContext.HttpContext.Session["EmployeeId"] != null)
            {
                var employeeId = Convert.ToInt32(filterContext.HttpContext.Session["EmployeeId"]);
                var employeeProfile = _rEmployeeProfile.GetList(p => p.EmployeeId == employeeId).FirstOrDefault();

                if (employeeProfile != null)
                {
                    filterContext.HttpContext.Session["IsDisplayActiveOnly"] = employeeProfile.IsDisplayActiveOnly;
                    _rEmployeeProfile.Detach(employeeProfile);
                }
            }

            //if (filterContext.HttpContext.Session["RoleName"] == null)
            //{
            //    var authid = filterContext.HttpContext.Request.LogonUserIdentity.Name;

            //    foreach (var role in _rRole.GetAll().ToList())
            //    {
            //        if (_rActiveDirectoryProvider.UserInGroup(authid, role.GroupAD))
            //        {
            //            filterContext.HttpContext.Session["RoleName"] = role.Name;
            //            break;
            //        }
            //    }
            //}
        }
    }
}
