﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Models;

namespace Core.Filters
{    
    // don't handel error in pages, work with controller only    
    public class LogErrorFilter : IExceptionFilter
    {        
        private readonly IBaseRepository<LogError> _rLogError;
        //private readonly IMailRepository rMail;

        public LogErrorFilter(IBaseRepository<LogError> rLogError/*, IMailRepository rMail*/)
        {            
            _rLogError = rLogError;
            //this.rMail = rMail;
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                var eventDate = DateTime.Now;
                var innerException = "";
                var source = filterContext.Exception.Source;
                var stackTrace = filterContext.Exception.StackTrace;
                var authid = filterContext.HttpContext.Request.LogonUserIdentity.Name;
                var computerIP = filterContext.HttpContext.Request.UserHostAddress;
                var message = filterContext.Exception.Message;

                if (filterContext.Exception.InnerException != null)
                {
                    if (filterContext.Exception.InnerException.InnerException != null)
                    {
                        innerException = filterContext.Exception.InnerException.InnerException.Message;
                    }
                    else
                    {
                        innerException = filterContext.Exception.InnerException.Message;
                    }
                }

                // todo: link it with UserTracking to know about place where errro appear

                _rLogError.Create(new LogError()
                {
                    EventDate = eventDate,
                    Authid = authid,
                    ComputerIP = computerIP,
                    Message = message,
                    InnerException = innerException,
                    Source = source,
                    StackTrace = stackTrace
                });

                //var mailDeveloper = rSystemParameter.GetValue("MailDeveloper");
                //var wwwAddress = rSystemParameter.GetValue("wwwAddress");

                //var description = "На портале Admin OnLine возникла ошибка! <br/> <br/>" +
                //    "<strong>Authid: </strong>" + authid + " <br/> " +
                //    "<strong>Computer IP: </strong>" + computerIP + " <br/> " +
                //    "<strong>Message: </strong>" + message + " <br/> " +
                //    "<strong>InnerException: </strong>" + innerException + " <br/> " +
                //    "<strong>Source: </strong>" + source + "<br/>" +
                //    "<strong>Stack: </strong>" + stackTrace + "<br/>" +
                //    "<strong>Data: </strong>" + filterContext.Exception.Data + "<br/> <br/>" +
                //    wwwAddress + "/AppError";

                //rMail.ErrorWarning(mailDeveloper, description);
            }
        }
    }


}
