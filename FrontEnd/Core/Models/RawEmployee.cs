﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RawEmployee
    {
        public int RawEmployeeId { get; set; }

        [MaxLength(100)]
        [DisplayName("Учетная запись (ЕКС)")]        
        public string RawAuthid { get; set; }

        [MaxLength(100)]
        [DisplayName("Фамилия (ЕКС)")]        
        public string RawSurname { get; set; }

        [MaxLength(100)]
        [DisplayName("Имя (ЕКС)")]        
        public string RawFirstname { get; set; }

        [MaxLength(100)]
        [DisplayName("Отчество (ЕКС)")]        
        public string RawMiddlename { get; set; }

        public int? EmployeeId { get; set; }
        [DisplayName("Сотрудник (база)")]
        public virtual Employee Employee { get; set; }

        public int? CompanyId { get; set; }
        [DisplayName("Компания (база)")]
        public virtual Company Company { get; set; }
        
        [MaxLength(100)]
        [DisplayName("МВЗ (ЕКС)")]        
        public string RawCostCenter { get; set; }

        public int? CostCenterId { get; set; }
        [DisplayName("МВЗ (база)")]  
        public virtual CostCenter CostCenter { get; set; }

        [MaxLength(500)]
        [DisplayName("Должность (ЕКС)")]        
        public string RawPosition { get; set; }

        public int? PositionId { get; set; }
        [DisplayName("МВЗ (база)")]  
        public virtual Position Position { get; set; }

        [MaxLength(500)]
        [DisplayName("Подразделение (ЕКС)")]        
        public string RawDepartmentCode { get; set; }

        [MaxLength(2000)]
        [DisplayName("Путь кодов подразделений (ЕКС)")]
        public string RawDepartmentCodes { get; set; }

        [MaxLength(4000)]
        [DisplayName("Путь наименований подразделений (ЕКС)")]
        public string RawDepartmentNames { get; set; }

        public int? DepartmentId { get; set; }
        [DisplayName("Подразделение (база)")]
        public virtual Department Department { get; set; }

        [MaxLength(100)]
        [DisplayName("E-Mail (ЕКС)")]
        public string RawEMail { get; set; }

        [DisplayName("Дата увольнения (ЕКС)")]
        public DateTime? RawDischargeDate { get; set; }
    }
}
