﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SecArea
    {
        public int SecAreaId { get; set; }

        [MaxLength(100)]
        [DisplayName("Область")]
        public string Name { get; set; }

        [MaxLength(500)]
        [DisplayName("Описание")]
        public string Description { get; set; }
    }
}
