﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SecAction
    {
        public int SecActionId { get; set; }

        [MaxLength(100)]
        [DisplayName("Действие")]
        public string Name { get; set; }

        [MaxLength(500)]
        [DisplayName("Описание")]
        public string Description { get; set; }
    }
}
