﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ServiceLine
    {
        public int ServiceLineId { get; set; }

        [MaxLength(100)]
        [DisplayName("Код")]
        public string Code { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
