﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Role
    {
        public int RoleId { get; set; }

        [MaxLength(100)]
        [DisplayName("Роль")]
        public string Name {get; set;}

        public int SecAreaId { get; set; }
        public virtual SecArea SecArea { get; set; }

        [MaxLength(100)]
        [DisplayName("Группа AD")]
        public string GroupAD { set; get; }
    }
}
