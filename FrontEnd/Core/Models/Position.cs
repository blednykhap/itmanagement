﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Position
    {
        public int PositionId { get; set; }

        //[MaxLength(100)]
        //public string ExtPositionId { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
