﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Tariff
    {
        public int TariffId { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        public int ComponentId { get; set; }
        public virtual Component Component { get; set; }

        public int SupportLevelId { get; set; }
        public virtual SupportLevel SupportLevel { get; set; }

        public int MeasureId { get; set; }
        public virtual Measure Measure { get; set; }

        public decimal Price { get; set; }
    }
}
