﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;

//TODO Remove Doubled SoftwareTypes

namespace Core.Models
{
    public class ITManagementDBContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<CostCenter> CostCenters { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Department> Departments { get; set; }        
        public DbSet<Maintenance> Maintenances { get; set; }
        public DbSet<SupportLevel> SupportLevels { get; set; }
        public DbSet<SystemParameter> SystemParameters { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<Component> Components { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<ScheduledTask> ScheduledTasks { get; set; }
        public DbSet<ExecutedTask> ExecutedTasks { get; set; }
        public DbSet<ServiceLine> ServiceLines { get; set; }
        public DbSet<LogAppEvent> LogAppEvents { get; set; }
        public DbSet<LogDataChange> LogDataChanges { get; set; }
        public DbSet<LogUserActivity> LogUserActivities { get; set; }
        public DbSet<SecArea> SecAreas { get; set; }
        public DbSet<SecAction> SecActions { get; set; }
        public DbSet<SecController> SecControllers { get; set; }
        public DbSet<EmployeeMaintenance> EmployeeMaintenances { get; set; }
        public DbSet<LoadADMaintenance> LoadADMaintenances { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<EmployeeProfile> EmployeeProfiles { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<HelpArticle> HelpArticles { get; set; }
        public DbSet<Software> Softwares { get; set; }
        public DbSet<Contractor> Contractors { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<License> Licenses { get; set; }
        public DbSet<SoftwareType> SoftwareTypes { get; set; }
        //public DbSet<ContractorSoftwareLicense> ContractorSoftwareLicenses { get; set; }
        public DbSet<RawSystem> RawSystems { get; set; }
        public DbSet<RawSystemMAC> RawSystemMACs { get; set; }
        //public DbSet<RawSystemIP> RawSystemIPs { get; set; }
        public DbSet<RawInstalledSoftware> RawInstalledSoftwares { get; set; }
        public DbSet<ComputerSystem> ComputerSystems { get; set; }
        public DbSet<InstalledSoftware> InstalledSoftwares { get; set; }
        public DbSet<ContractorSoftware> ContractorSoftwares { get; set; }
        public DbSet<LicenseFile> LicenseFiles { get; set; }
        public DbSet<RawSAPMaintenance> RawSAPMaintenances { get; set; }
        public DbSet<RawCostCenter> RawCostCenters { get; set; }
        public DbSet<RawEmployee> RawEmployees { get; set; }
        public DbSet<RawMaintenanceByUser> RawMaintenanceByUsers { get; set; }
        public DbSet<CompanyStructure> CompanyStructures { get; set; }
        public DbSet<MaintenanceOrder> MaintenanceOrders { get; set; }
        public DbSet<RawADMaintenance> RawADMaintenances { get; set; }
        public DbSet<LogError> LogErrors { get; set; }
        public DbSet<SoftwareModule> SoftwareModules { get; set; }
        public DbSet<Subdivision> Subdivisions { get; set; }  
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>()
                .HasOptional(p => p.ParentDepartment)
                .WithMany()
                .HasForeignKey(p => p.ParentDepartmentId);
            modelBuilder.Entity<CompanyStructure>()
                .HasOptional(p => p.ParentCompanyStructure)
                .WithMany()
                .HasForeignKey(p => p.ParentCompanyStructureId);
            modelBuilder.Entity< Subdivision>()
                .HasOptional(p => p.ParentSubdivision)
                .WithMany()
                .HasForeignKey(p => p.ParentSubdivisionId);
            //modelBuilder.Entity<MaintenanceObjectUnit>()
            //    .HasOptional(p => p.ParentMaintenanceObjectUnit)
            //    .WithMany()
            //    .HasForeignKey(p => p.ParentMaintenanceObjectUnitId);
        }
    }
}