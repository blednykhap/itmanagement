﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Picture
    {
        public int PictureId { get; set; }

        public DateTime MakeDate { get; set; }

        [MaxLength(500)]
        [DisplayName("Имя файла")]
        public string FileName { get; set; }

        public byte[] FileContent { get; set; }

        [MaxLength(100)]
        [DisplayName("Тип файла")]
        public string FileType { get; set; }
    }
}
