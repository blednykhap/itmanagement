﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class EmployeeMaintenance
    {
        public int EmployeeMaintenanceId { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
