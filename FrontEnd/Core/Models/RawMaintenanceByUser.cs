﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RawMaintenanceByUser
    {
        public int RawMaintenanceByUserId { get; set; }

        [DisplayName("Компания (база)")]
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [MaxLength(50)]
        [DisplayName("Логин (файл)")]
        public string RawLogin { get; set; }

        [MaxLength(1000)]
        [DisplayName("ФИО (файл)")]
        public string RawFIO { get; set; }

        [MaxLength(1000)]
        [DisplayName("Подразделение (файл)")]
        public string RawDepartment { get; set; }

        [MaxLength(50)]
        [DisplayName("МВЗ (файл)")]
        public string RawCostCenter { get; set; }

        [MaxLength(200)]
        [DisplayName("Логин (файл)")]
        public string RawMaintenance { get; set; }

        [MaxLength(50)]
        [DisplayName("Уровень поддержки (файл)")]
        public string RawSupportLevel { get; set; }

        [MaxLength(50)]
        [DisplayName("ПО (файл)")]
        public string RawSoftware { get; set; }

        public int? CompanyStructureId { get; set; }
        public virtual CompanyStructure CompanyStructure { get; set; }

        public int? MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        public int? ComponentId { get; set; }
        public virtual Component Component { get; set; }

        public int? SoftwareId { get; set; }
        public virtual Software Software { get; set; }

        public int? SoftwareModuleId { get; set; }
        public virtual SoftwareModule SoftwareModule { get; set; }

        public int? SupportLevelId { get; set; }
        public virtual SupportLevel SupportLevel { get; set; }

        [DisplayName("Месяц с")]
        public int? CodeMonthFrom { get; set; }

        [DisplayName("Месяц по")]
        public int? CodeMonthTo { get; set; }
    }
}
