﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Subdivision
    {
        public int SubdivisionId { get; set; }

        public int? ParentSubdivisionId { get; set; }
        public virtual Subdivision ParentSubdivision { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [MaxLength(100)]
        [DisplayName("Код")]
        public string Code { get; set; }

        [MaxLength(240)]
        [DisplayName("Наименование")]
        [Required(ErrorMessage = "Необходимо заполнить Наименование")]
        public string Name { get; set; }

        [DisplayName("Корень")]
        public bool IsRoot { get; set; }

        [DisplayName("Активен")]
        public bool IsActive { get; set; }
    }
}

