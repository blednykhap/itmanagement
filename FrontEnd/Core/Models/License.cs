﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class License
    {
        public int LicenseId { get; set; }

        [MaxLength(100)]
        [DisplayName("Код")]
        public string Code { get; set; }

        [MaxLength(1000)]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Количество лицензий")]
        public int Amount { get; set; }

        //[DisplayName("Дата")]
        // [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }

        [DisplayName("Номер договора")]
        [MaxLength(100)]
        public string ContractNumber { get; set; }

        [DisplayName("Дата договора")]
        [DataType(DataType.Date)]
        public DateTime ContractDate { get; set; }

        //public virtual ICollection<LicenseFile> LicenseFiles { get; set; }

        public int? ContractorId { get; set; }
        public virtual Contractor Contractor { get; set; }

        public int? SoftwareId { get; set; }
        public virtual Software Software { get; set; }
    }
}
