﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class LicenseFile
    {
        public int LicenseFileId { get; set; }

        public int LicenseId { get; set; }
        public virtual License License { get; set; }

        [MaxLength(500)]
        [DisplayName("Имя файла")]
        public string FileName { get; set; }

        [MaxLength(100)]
        [DisplayName("Тип файла")]
        public string FileType { get; set; }

        [DisplayName("Файл договора")]
        public byte[] FileContent { get; set; }
    }
}
