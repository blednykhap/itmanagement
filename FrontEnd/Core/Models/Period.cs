﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace Business.Others
namespace Core.Models
{
    public class Period
    {
        public int CodeMonth { get; set; }
        public string Name { get; set; }        
    }
}
