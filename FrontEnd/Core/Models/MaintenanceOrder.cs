﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class MaintenanceOrder
    {
        public int MaintenanceOrderId { get; set; }

        [DisplayName("Дата создания документа")]
        public DateTime MakeDate { get; set; }

        public int CompanyStructureId { get; set; }
        public virtual CompanyStructure CompanyStructure { get; set; }

        public int MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int? ComponentId { get; set; }
        public virtual Component Component { get; set; }

        public int? SupportLevelId { get; set; }
        public virtual SupportLevel SupportLevel { get; set; }

        public int? TariffId { get; set; }
        public virtual Tariff Tariff { get; set; }

        public int? SoftwareId { get; set; }
        public virtual Software Software { get; set; }

        public int? SoftwareModuleId { get; set; }
        public virtual SoftwareModule SoftwareModule { get; set; }

        [DisplayName("Начало предоставления услуги")]
        public int? CodeMonthFrom { get; set; }

        [DisplayName("Окончание предоставления услуги")]
        public int? CodeMonthTo { get; set; }

        public decimal? Total { get; set; }

        [MaxLength(1000)]
        [DisplayName("Комментарий")]        
        public string Comment { get; set; }
    }
}
