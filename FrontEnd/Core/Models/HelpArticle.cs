﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class HelpArticle
    {
        public int HelpArticleId { get; set; }

        [MaxLength(1000)]
        [DisplayName("Заголовок")]
        public string Title { get; set; }

        [DisplayName("Автор")]
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [DisplayName("Содержимое")]
        public string Content { get; set; }
    }
}
