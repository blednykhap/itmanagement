﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SoftwareModule
    {
        public int SoftwareModuleId { get; set; }

        public int? SoftwareId { get; set; }
        [DisplayName("Программное обеспечение")]
        public virtual Software Software { get; set; }

        [MaxLength(100)]
        [DisplayName("Код")]
        public string Code { get; set; }

        [MaxLength(1000)]
        [DisplayName("Наименование")]
        public string Name { get; set; }
    }
}
