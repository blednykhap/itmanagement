﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Models
{
    public class ContractorSoftware
    {
        public int ContractorSoftwareId { get; set; }

        public int? OriginalContractorId { get; set; }
        [ForeignKey("OriginalContractorId")]
        public virtual Contractor OriginalContractor { get; set; }

        public int? GeneralContractorId { get; set; }
        [ForeignKey("GeneralContractorId")]
        public virtual Contractor GeneralContractor { get; set; }

        public int? OriginalSoftwareId { get; set; }
        [ForeignKey("OriginalSoftwareId")]
        public virtual Software OriginalSoftware { get; set; }

        public int? GeneralSoftwareId { get; set; }
        [ForeignKey("GeneralSoftwareId")]
        public virtual Software GeneralSoftware { get; set; }

        public int? SoftwareTypeId { get; set; }
        public virtual SoftwareType SoftwareType { get; set; }
    }
}
