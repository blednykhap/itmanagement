﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CompanyStructure
    {
        public int CompanyStructureId { get; set; }

        public int? ParentCompanyStructureId { get; set; }
        public virtual CompanyStructure ParentCompanyStructure { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int? DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public int? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [MaxLength(100)]
        [DisplayName("Создан")]
        public string CreatedBy { get; set; }

        [DisplayName("Активен")]
        public bool IsActive { get; set; }

        //public string Name {
        //    get { 
        //    }
        //}
    }
}
