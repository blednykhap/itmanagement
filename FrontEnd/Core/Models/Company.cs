﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Company
    {
        public int CompanyId { get; set; }

        [MaxLength(500)]
        [DisplayName("Компания")]
        public string Name { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование в BMC Remedy")]
        public string BMC { get; set; }

        [DisplayName("Id в EKC")]
        public int? EKC { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование в SAP")]
        public string SAP { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        public virtual ICollection<Department> Departments { get; set; }

        public virtual ICollection<CompanyMaintenance> Maintenances { get; set; }
    }
}
