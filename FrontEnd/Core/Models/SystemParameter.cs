﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SystemParameter
    {
        public int SystemParameterId { get; set; }

        [MaxLength(2000)]
        [DisplayName("Описание параметра")]
        public string Description { get; set; }

        [MaxLength(100)]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [MaxLength(100)]
        [DisplayName("Значение")]
        public string Value { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
