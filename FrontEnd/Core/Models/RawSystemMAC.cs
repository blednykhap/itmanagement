﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RawSystemMAC
    {
        public int RawSystemMACId { get; set; }

        public DateTime MakeDate { get; set; }

        public int? ResourceId { get; set; }

        [MaxLength(100)]
        public string MAC_Addresses0 { get; set; }
    }
}
