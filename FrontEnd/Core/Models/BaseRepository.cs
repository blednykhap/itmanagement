﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Core;
using Core.Models;

namespace Core.Models
{
    public interface IBaseRepository<T> : IDisposable
    {
        void Detach(T entity);

        T Get(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetList(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();

        bool Create(T entity);
        bool Update(T entity);
        bool Delete(T entity);
        void BulkDelete(string sqlTableName);
    }

    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private ITManagementDBContext _context;

        public BaseRepository(ITManagementDBContext context)
        {
            _context = context;
        }

        public void Detach(T entity)
        {
            _context.Entry(entity).State = EntityState.Detached;           
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate).SingleOrDefault();
        }

        public IEnumerable<T> GetList(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public bool Create(T entity)
        {
            _context.Set<T>().Add(entity);
            return SaveChanges();
        }

        public bool Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            return SaveChanges();
        }

        public bool Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            return SaveChanges();
        }

        public void BulkDelete(string sqlTableName)
        {
            var sql = "DELETE FROM " + sqlTableName.Trim().ToUpper();
            _context.Database.ExecuteSqlCommand(sql);
        }

        private bool SaveChanges()
        {
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch
            {
                // TODO: Track exceptions
                return false;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
