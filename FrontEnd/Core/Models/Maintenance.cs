﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Maintenance
    {
        public int MaintenanceId { get; set; }

        public int ServiceLineId { get; set; }
        public virtual ServiceLine ServiceLine { get; set; }

        [MaxLength(100)]
        [DisplayName("Код")]
        public string Code { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [MaxLength(1000)]
        [DisplayName("Группы в AD (;)")]
        public string ContainerAD { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        [DisplayName("Круглосуточная")]
        public bool Is24Hours { get; set; }
    }
}
