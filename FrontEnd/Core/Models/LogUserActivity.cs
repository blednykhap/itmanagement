﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class LogUserActivity
    {
        public int LogUserActivityId { get; set; }

        [DisplayName("Дата события")]
        public DateTime EventDate { get; set; }

        [MaxLength(100)]
        [DisplayName("Учетная запись")]
        public string Authid { get; set; }

        [MaxLength(100)]
        [DisplayName("IP компьютера")]
        public string ComputerIP { get; set; }

        public int SecAreaId { get; set; }
        public virtual SecArea SecArea { get; set; }

        public int SecControllerId { get; set; }
        public virtual SecController SecController { get; set; }

        public int SecActionId { get; set; }
        public virtual SecAction SecAction { get; set; }

        [MaxLength(500)]
        [DisplayName("Строка параметров")]
        public string QueryString { get; set; }

        [DisplayName("Успех доступа")]
        public bool Result { get; set; }
    }
}
