﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ScheduledTask
    {
        public int ScheduledTaskId { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayName("Дата начала")]
        public DateTime StartDate { get; set; }

        [MaxLength(500)]
        [DisplayName("Выполняемая процедура")]
        public string Action { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayName("Дата окончания")]
        public DateTime EndDate { get; set; }

        [DisplayName("Период")]
        public int Period { get; set; }

        [DisplayName("Активна")]
        public bool IsActive { get; set; }

        public virtual ICollection<ExecutedTask> ExecutedTasks { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
