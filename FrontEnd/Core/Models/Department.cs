﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }

        public int? ParentDepartmentId { get; set; }
        public virtual Department ParentDepartment { get; set; }

        [Required(ErrorMessage = "Необходимо выбрать Организацию")]
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        //[Required(ErrorMessage = "Необходимо выбрать МВЗ")]
        public int? CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        [MaxLength(100)]
        [DisplayName("Код (из ЕКС)")]
        public string Code { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование")]
        [Required(ErrorMessage = "Необходимо заполнить Наименование")]
        public string Name { get; set; }

        [DisplayName("Корень")]
        public bool IsRoot { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
        
        [MaxLength(100)]
        [DisplayName("Создан")]
        public string CreatedBy { get; set; }

        [DisplayName("Активен")]
        public bool IsActive { get; set; }

        /*[Required(ErrorMessage = "Необходимо выбрать Обособленное подразделение")]*/
        public int? SubdivisionId { get; set; }
        public virtual Subdivision Subdivision  { get; set; }
    }
}
