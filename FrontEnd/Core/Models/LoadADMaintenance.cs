﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Models
{
    public class LoadADMaintenance
    {
        public int LoadADMaintenanceId { get; set; }

        public int MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        [MaxLength(100)]
        [DisplayName("Учетная запись в AD")]
        public string Authid { get; set; }
    }
}
