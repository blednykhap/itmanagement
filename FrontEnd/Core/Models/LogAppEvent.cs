﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class LogAppEvent
    {
        public int LogAppEventId { get; set; }

        public DateTime MakeDate { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public virtual ICollection<LogDataChange> LogDataChanges { get; set; }

        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
