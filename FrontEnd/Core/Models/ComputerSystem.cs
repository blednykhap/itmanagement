﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ComputerSystem
    {
        [Key]
        [DisplayName("ID Системы")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ResourceId { get; set; }

        //public int ComputerSystemId { get; set; }

        public DateTime MakeDate { get; set; }

        //public int? ResourceId { get; set; }

        public DateTime? AgentTime { get; set; }

        //[MaxLength(100)]
        //public string User_Domain0 { get; set; }

        //[MaxLength(100)]
        //public string User_Name0 { get; set; }

        public int? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [MaxLength(100)]
        [DisplayName("Имя компьютера")]
        public string NetbiosName { get; set; }

        [MaxLength(100)]
        [DisplayName("Операционная система")]
        public string OperatingSystem { get; set; }

        public bool IsActive { get; set; }
    }
}
