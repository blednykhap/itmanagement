﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RawSystem
    {
        public int RawSystemId { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int? ResourceId { get; set; }

        public DateTime? AgentTime { get; set; }

        [MaxLength(100)]
        public string User_Domain0 { get; set; }

        [MaxLength(100)]
        public string User_Name0 { get; set; }

        public int? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [MaxLength(100)]
        public string Netbios_Name0 { get; set; }

        [MaxLength(100)]
        public string Operating_System_Name_and0 { get; set; }
    }
}
