﻿//using Core.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class EmployeeProfile
    {
        public int EmployeeProfileId { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int? CompanyId { get; set; }
        [DisplayName("Компания по умолчанию")]
        public virtual Company Company { get; set; }

        [NotMapped]
        public int? CodeMonth { get; set; }
        [NotMapped]
        [DisplayName("Текущий учетный период")]
        public Period Period { get; set; }

        [DisplayName("Отображать только актвные записи")]
        public bool IsDisplayActiveOnly { get; set; }
    }
}
