﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RawSAPMaintenance
    {
        public int RawSAPMaintenanceId { get; set; }

        //public DateTime? DatePeriod { get; set; }

        [DisplayName("Компания (файл)")]
        public string RawCompany { get; set; }

        [DisplayName("Компания (база)")]
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [MaxLength(100)]
        public string Module { get; set; }

        [DisplayName("Логин")]
        [MaxLength(100)]
        public string RawLogin { get; set; }

        public string RawFullname { get; set; }

        //public int? EmployeeId { get; set; }
        //public virtual Employee Employee { get; set; }

        public int? CompanyStructureId { get; set; }
        public virtual CompanyStructure CompanyStructure { get; set; }

        public int? MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        public int? ComponentId { get; set; }
        public virtual Component Component { get; set; }

        public int? SoftwareId { get; set; }
        public virtual Software Software { get; set; }

        [DisplayName("Месяц с")]
        public int? CodeMonthFrom { get; set; }

        [DisplayName("Месяц по")]
        public int? CodeMonthTo { get; set; }
    }
}
