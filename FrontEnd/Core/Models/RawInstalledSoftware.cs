﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RawInstalledSoftware
    {
        public int RawInstalledSoftwareId { get; set; }

        //public DateTime MakeDate { get; set; }

        public int? MachineId { get; set; }
        
        public int? InstanceKey { get; set; }

        /*public int? InstallDirectoryValidatio0 { get; set; }

        public int? RevisionId { get; set; }
    
        public int? AgentId { get; set; }

        public DateTime? TimeKey { get; set; }

        [MaxLength(100)]
        public string SoftwareCode00 { get; set; }

        [MaxLength(100)]
        public string ProductCode00 { get; set; }

        [MaxLength(100)]
        public string CM_DSLID00 { get; set; }*/

        [MaxLength(255)]
        public string ProductName00 { get; set; }

        /*[MaxLength(500)]
        public string ARPDisplayName00 { get; set; }*/

        [MaxLength(48)]
        public string ProductVersion00 { get; set; }

        [MaxLength(255)]
        public string Publisher00 { get; set; }

        /*public int? VersionMajor00 { get; set; }

        public int? VersionMinor00 { get; set; }*/

        [MaxLength(16)]
        public string ServicePack00 { get; set; }

        /*public int? Language00 { get; set; }

        [MaxLength(100)]
        public string ProductID00 { get; set; }

        [MaxLength(500)]
        public string InstalledLocation00 { get; set; }

        [MaxLength(500)]
        public string InstallSource00 { get; set; }

        [MaxLength(500)]
        public string UninstallString00 { get; set; }

        [MaxLength(500)]
        public string LocalPackage00 { get; set; }

        [MaxLength(100)]
        public string UpgradeCode00 { get; set; }*/

        public DateTime? InstallDate00 { get; set; }

        /*[MaxLength(100)]
        public string RegisteredUser00 { get; set; }

        [MaxLength(100)]
        public string SoftwarePropertiesHash00 { get; set; }

        [MaxLength(100)]
        public string SoftwarePropertiesHashEx00 { get; set; }*/

        public int? SoftwareId { get; set; }
        public virtual Software Software { get; set; }

        public int? ContractorId { get; set; }
        public virtual Contractor Contractor { get; set; }
    }
}
