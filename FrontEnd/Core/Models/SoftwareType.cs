﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SoftwareType
    {
        public int SoftwareTypeId { get; set; }

        [MaxLength(50)]
        [DisplayName("Код")]
        public string Code { get; set; }

        [MaxLength(100)]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Лицензирование")]
        public bool IsNeedLicense { get; set; }        
    }
}
