﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }

        [MaxLength(100)]        
        [DisplayName("Учетная запись")]
        [Required(ErrorMessage = "Необходимо заполнить Учетную запись")]
        public string Authid { get; set; }

        [MaxLength(100)]
        [DisplayName("Фамилия")]
        //[Required(ErrorMessage = "Необходимо заполнить Фамилию")]
        public string Surname { get; set; }

        [MaxLength(100)]
        [DisplayName("Имя")]
        //[Required(ErrorMessage = "Необходимо заполнить Имя")]
        public string Firstname { get; set; }

        [MaxLength(100)]
        [DisplayName("Отчество")]        
        public string Middlename { get; set; }

        [MaxLength(500)]        
        [DisplayName("ФИО")]
        [Required(ErrorMessage = "Необходимо заполнить Фамилию Имя Отчество")]
        public string Fullname { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        //[Required(ErrorMessage = "Необходимо выбрать МВЗ")]
        public int? CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        //[Required(ErrorMessage = "Необходимо выбрать Должность")]
        public int? PositionId { get; set; }
        public virtual Position Position { get; set; }

        //[Required(ErrorMessage = "Необходимо выбрать Подразделение")]
        public int? DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public virtual ICollection<ComputerSystem> ComputerSystems { get; set; }

        [MaxLength(100)]
        [DisplayName("E-Mail")]
        public string EMail { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Дата увольнения")]
        public DateTime? DischargeDate { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }

        [MaxLength(100)]
        [DisplayName("Создан")]
        public string CreatedBy { get; set; }

        [DisplayName("Активен")]
        public bool IsActive { get; set; }
    }
}
