﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class RawCostCenter
    {
        public int RawCostCenterId { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [MaxLength(100)]
        [DisplayName("Код в файле")]
        [Required(ErrorMessage = "Необходимо заполнить Код")]
        public string ExcelCode { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование в файле")]
        //[Required(ErrorMessage = "Необходимо заполнить Наименование")]
        public string ExcelName { get; set; }

        public int? CostCenterId { get; set; }
        [DisplayName("Код в базе")]
        public virtual CostCenter CostCenter { get; set; }

        //[MaxLength(100)]
        //[DisplayName("Код")]
        ////[Required(ErrorMessage = "Необходимо заполнить Код")]
        //public string BaseCode { get; set; }

        //[MaxLength(500)]
        //[DisplayName("Наименование")]
        ////[Required(ErrorMessage = "Необходимо заполнить Наименование")]
        //public string ExcelName { get; set; }

        //[MaxLength(500)]
        //[DisplayName("Комментарий")]
        //public string Comment { get; set; }
    }
}
