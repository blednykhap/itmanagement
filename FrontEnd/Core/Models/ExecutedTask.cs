﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ExecutedTask
    {
        public int ExecutedTaskId { get; set; }

        public int ScheduledTaskId { get; set; }
        public virtual ScheduledTask ScheduledTask { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayName("Дата выполнения")]
        public DateTime ExecutedDate { get; set; }

        public bool IsSuccessful { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
