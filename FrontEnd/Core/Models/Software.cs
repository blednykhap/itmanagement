﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Software
    {
        public int SoftwareId { get; set; }

        [MaxLength(240)]
        [DisplayName("Программное обеспечение")]
        public string Name { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
