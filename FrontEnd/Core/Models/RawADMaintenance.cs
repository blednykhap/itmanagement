﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Models
{
    public class RawADMaintenance
    {
        public int RawADMaintenanceId { get; set; }

        public int MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        [MaxLength(100)]
        [DisplayName("Учетная запись (AD)")]
        public string RawAuthid { get; set; }

        public int? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int? CompanyStructureId { get; set; }
        public virtual CompanyStructure CompanyStructure { get; set; }

        public int? ComponentId { get; set; }
        public virtual Component Component { get; set; }

        public int? CodeMonthFrom { get; set; }

        public int? CodeMonthTo { get; set; }
    }
}
