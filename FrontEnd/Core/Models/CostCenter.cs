﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CostCenter
    {
        public int CostCenterId { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [MaxLength(100)]
        [DisplayName("Код")]
        [Required(ErrorMessage = "Необходимо заполнить Код")]
        public string Code { get; set; }

        [MaxLength(500)]
        [DisplayName("Наименование")]
        //[Required(ErrorMessage = "Необходимо заполнить Наименование")]
        public string Name { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
