﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CompanyMaintenance
    {
        public int CompanyMaintenanceId { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int MaintenanceId { get; set; }
        public virtual Maintenance Maintenance { get; set; }

        [MaxLength(500)]
        [DisplayName("Комментарий")]
        public string Comment { get; set; }
    }
}
