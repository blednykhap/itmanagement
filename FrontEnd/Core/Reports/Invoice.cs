﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Reports
{
   public class Invoice
    {
        public string ServiceLine { get; set; }

        public string ServiceLineName { get; set; }

        public string ServiceCode { get; set; }

        public string Component { get; set; }

        public string ServiceName { get; set; }

        public string ServiceNameFull { get; set; }

        public string Measure { get; set; }

        public string SupportLevel { get; set; }

        public decimal Price { get; set; }

        public int Amount { get; set; }

        public decimal Cost { get; set; }

        public string Department { get; set; }

        public string Mvz { get; set; }

        public int SubdivId { get; set; }

        public string SubdivName { get; set; }
    }
}
