﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Reports
{
    public class R24HoursMaintenance
    {
        [DisplayName("Услуга")]
        public string Maintenance { get; set; }

        [DisplayName("Уч. запись")]
        public string Authid { get; set; }

        [DisplayName("Сотрудник")]
        public string Employee { get; set; }
    }
}
