﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Reports
{
    public class Chess
    {
        public string ServiceCode { get; set; }

        public string ServiceName { get; set; }

        public string ServiceNameFull { get; set; }

        public string Component { get; set; }

        public string SupportLevel { get; set; }

        public string Mvz { get; set; }

        public string MvzName { get; set; }

        public string Department { get; set; }

        public string Login { get; set; }

        public string FIO { get; set; }

        public string Position { get; set; }

        public int Flag { get; set; }

        public int SubdivId { get; set; }

        public string SubdivName { get; set; }
    }
}
