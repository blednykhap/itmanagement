﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Reports
{
    public class UsedLicense
    {
        [DisplayName("Разработчик")]
        public string Contractor { get; set; }

        public int? SoftwareId { get; set; }

        [DisplayName("Программа")]
        public string Software { get; set; }

        [DisplayName("Договор")]
        public string Contract { get; set; }

        [DisplayName("Действует")]
        public string DateTo { get; set; }

        [DisplayName("Всего")]
        public int Amount { get; set; }

        [DisplayName("Использовано")]
        public int UsedAmount { get; set; }

        [DisplayName("Остаток")]
        public int Balance { get; set; }      
    }
}
