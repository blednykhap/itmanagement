﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Core.Models;

namespace Core.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            List<string> result = new List<string>();

            //result.Add("Admin");
            //return result.ToArray();

            if (username == "CORP\\evsigaeva")
            {
                result.Add("Admin");
            }

            using (ITManagementDBContext context = new ITManagementDBContext())
            {

                var serverController = context.SystemParameters.Where(p => p.Name == "DomainServerController").FirstOrDefault();
                var rootContainer = context.SystemParameters.Where(p => p.Name == "DomainRootContainer").FirstOrDefault();

                PrincipalContext principalContext =
                    new PrincipalContext(ContextType.Domain, serverController.Value, rootContainer.Value);
                UserPrincipal userPrincipal
                    = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, username);

                GroupPrincipal groupPrincipal;

                foreach (Role role in context.Roles.ToList())
                {
                    groupPrincipal = GroupPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, role.GroupAD);
                    if (groupPrincipal != null && userPrincipal != null)
                    {
                        if (userPrincipal.IsMemberOf(groupPrincipal))
                        {
                            result.Add(role.Name);
                        }
                    }
                }
                return result.ToArray();
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}
