﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Providers
{

    public interface IActiveDirectoryProvider
    {
        string GetUserDisplayName(string employeeAuthid);
    }
    
    public class ActiveDirectoryProvider : IActiveDirectoryProvider
    {
        private readonly IBaseRepository<SystemParameter> _rSystemParameter;

        public ActiveDirectoryProvider(IBaseRepository<SystemParameter> rSystemParameter)
        {
            _rSystemParameter = rSystemParameter;
        }

        public string GetUserDisplayName(string employeeAuthid)
        {
            string result = "";

            try
            {
                var serverController = _rSystemParameter.Get(p => p.Name == "DomainServerController");
                var rootContainer = _rSystemParameter.Get(p => p.Name == "DomainRootContainer");

                PrincipalContext principalContext =
                    new PrincipalContext(ContextType.Domain, serverController.Value, rootContainer.Value);
                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, employeeAuthid);

                result = userPrincipal.DisplayName;
            }
            catch
            {
                result = "Anonymous";
            }
            
            return result;
        }


        public List<string> GetUsersInGroup(string groupName)
        {
            List<string> users = new List<string>();

            try
            {
                var serverController = _rSystemParameter.Get(p => p.Name == "DomainServerController");
                var rootContainer = _rSystemParameter.Get(p => p.Name == "DomainRootContainer");

                PrincipalContext principalContext =
                    new PrincipalContext(ContextType.Domain, serverController.Value, rootContainer.Value);
                GroupPrincipal groupPrincipal
                    = GroupPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, groupName);

                if (groupPrincipal != null)
                {
                    foreach (Principal principalUser in groupPrincipal.GetMembers(true))
                    {
                        users.Add(principalUser.SamAccountName);
                    }
                }
            }
            catch { }

            return users;
        }

        public bool UserInGroup(string authid,string groupName)
        {
            //List<string> users = new List<string>();
            try
            {
                var serverController = _rSystemParameter.Get(p => p.Name == "DomainServerController");
                var rootContainer = _rSystemParameter.Get(p => p.Name == "DomainRootContainer");

                PrincipalContext principalContext =
                    new PrincipalContext(ContextType.Domain, serverController.Value, rootContainer.Value);
                GroupPrincipal groupPrincipal
                    = GroupPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, groupName);
                UserPrincipal userPrincipal
                    = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, authid);

                if (groupPrincipal != null && userPrincipal != null)
                {
                    return userPrincipal.IsMemberOf(groupPrincipal); 
                }
            }
            catch { }

            return false;
        }

    }
}
