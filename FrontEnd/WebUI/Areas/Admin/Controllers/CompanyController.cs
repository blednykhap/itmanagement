﻿using Core.Models;
using Core.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Filters;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class CompanyController : Controller
    {
        private readonly IBaseRepository<Company> _rCompany;

        public CompanyController(IBaseRepository<Company> rCompany)
        {
            _rCompany = rCompany;
        }

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var companies = (from p in _rCompany.GetAll().ToList()
                             select new CompanyView { 
                                 CompanyId = p.CompanyId,
                                 Name = p.Name,
                                 EKC = p.EKC,
                                 BMC = p.BMC
                             });

            return View(new GridModel(companies));
        }

    }
}
