﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class LogAppEventController : Controller
    {
        private readonly IBaseRepository<LogAppEvent> _rLogAppEvent;

        public LogAppEventController(IBaseRepository<LogAppEvent> rLogAppEvent)
        {
            _rLogAppEvent = rLogAppEvent;
        }

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var logs = (from p in _rLogAppEvent.GetAll().ToList()
                        //_rLogAppEvent.GetList(p=>p.LogDataChanges.Count()!=0).ToList()
                        select new LogAppEventView
                         {
                             LogAppEventId = p.LogAppEventId,
                             MakeDate = p.MakeDate,
                             Employee = p.Employee.Authid,
                             LogDataChanges = p.LogDataChanges.Count,
                             Comment = p.Comment,

                         });

            return View(new GridModel(logs.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var log = _rLogAppEvent.Get(p => p.LogAppEventId == id);
            _rLogAppEvent.Delete(log);

            var logs = (from p in _rLogAppEvent.GetAll().ToList()
                            //_rLogAppEvent.GetList(p=>p.LogDataChanges.Count()!=0).ToList()
                        select new LogAppEventView
                        {
                            LogAppEventId = p.LogAppEventId,
                            MakeDate = p.MakeDate,
                            Employee = p.Employee.Authid,
                            LogDataChanges = p.LogDataChanges.Count,
                            Comment = p.Comment
                        });

            return View(new GridModel(logs.ToList()));
        }

        public ActionResult Details(int logAppEventId)
        {
            var logAppEvent = _rLogAppEvent.Get(p => p.LogAppEventId == logAppEventId);
            return View(logAppEvent);
        }

    }
}
