﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Filters;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class SystemParameterController : Controller
    {
        private readonly IBaseRepository<SystemParameter> _rSystemParameter;

        public SystemParameterController(IBaseRepository<SystemParameter> rSystemParameter)
        {
            _rSystemParameter = rSystemParameter;
        }

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var systemParameters = _rSystemParameter.GetAll();

            return View(new GridModel(systemParameters.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(SystemParameter systemParameter)
        {
            if (ModelState.IsValid)
            {
                if (systemParameter.SystemParameterId.Equals(0))
                {
                    _rSystemParameter.Create(systemParameter);
                }
                else
                {
                    _rSystemParameter.Update(systemParameter);
                }
            }

            var systemParameters = _rSystemParameter.GetAll();

            return View(new GridModel(systemParameters.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var systemParameter = _rSystemParameter.Get(p => p.SystemParameterId == id);
            _rSystemParameter.Delete(systemParameter);

            var systemParameters = _rSystemParameter.GetAll();

            return View(new GridModel(systemParameters.ToList()));
        }

    }
}
