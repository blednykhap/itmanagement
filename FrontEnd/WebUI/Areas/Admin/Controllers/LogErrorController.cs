﻿using Core.Filters;
using Core.Models;
using Core.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class LogErrorController : Controller
    {
        private readonly IBaseRepository<LogError> _rLogError;

        public LogErrorController(IBaseRepository<LogError> rLogError)
        {
            _rLogError = rLogError;
        }

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var logErrors = (from p in _rLogError.GetAll().ToList()
                             orderby p.EventDate descending 
                             select new LogErrorView
                             {
                                 LogErrorId = p.LogErrorId,
                                 Authid = p.Authid,
                                 EventDate = p.EventDate,
                                 Message = (p.Message != null ? 
                                    (p.Message.Length > 80 ? p.Message.Substring(0, 80) + "..." : p.Message) :
                                    (p.Message.Length > 80 ? p.InnerException.Substring(0, 80) + "..." : p.InnerException))
                             });

            return View(new GridModel(logErrors));
        }

        public ActionResult Details(int logErrorId)
        {
            var logError = _rLogError.Get(p => p.LogErrorId == logErrorId);
            return View(logError);
        }
    }
}
