﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class PictureController : Controller
    {
        private readonly IBaseRepository<Picture> _rPicture;

        public PictureController(IBaseRepository<Picture> rPicture)
        {
            _rPicture = rPicture;
        }
        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var pictures = (from p in _rPicture.GetAll().ToList()
                             select new PictureView
                             {
                                 PictureId = p.PictureId,
                                 MakeDate = p.MakeDate,
                                 FileName = p.FileName,
                                 FileType = p.FileType
                             });

            return View(new GridModel(pictures.ToList()));
        }

        public void _Upload(HttpPostedFileBase attachments = null)
        {
            if (attachments != null)
            {
                var picture = new Picture();

                var br = new BinaryReader(attachments.InputStream);
                picture.MakeDate = DateTime.Now;
                picture.FileContent = br.ReadBytes(attachments.ContentLength);
                picture.FileName = Path.GetFileName(attachments.FileName);
                picture.FileType = attachments.ContentType;
                _rPicture.Create(picture);                
            }  
        }

        [OutputCache(Duration = 600, Location = OutputCacheLocation.Server, VaryByParam = "pictureId")]
        public FileContentResult GetImage(int pictureId)
        {
            Picture photo = _rPicture.Get(p => p.PictureId == pictureId);
            return File(photo.FileContent, photo.FileType);
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var picture = _rPicture.Get(p => p.PictureId == id);
            _rPicture.Delete(picture);

            var pictures = (from p in _rPicture.GetAll().ToList()
                            select new PictureView
                            {
                                PictureId = p.PictureId,
                                MakeDate = p.MakeDate,
                                FileName = p.FileName,
                                FileType = p.FileType
                            });

            return View(new GridModel(pictures.ToList()));
        }

    }
}
