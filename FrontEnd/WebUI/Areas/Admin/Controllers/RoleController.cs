﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class RoleController : Controller
    {
        private readonly IBaseRepository<Role> _rRole;
        private readonly IBaseRepository<SecArea> _rSecArea;

        public RoleController(IBaseRepository<Role> rRole,
                               IBaseRepository<SecArea> rSecArea)
        {
            _rRole = rRole;
            _rSecArea = rSecArea;            
        }

        public ActionResult Index()
        {
            ViewBag.SecAreas = new SelectList(_rSecArea.GetAll(), "SecAreaId", "Name");
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var roles = (from p in _rRole.GetAll().ToList()
                         select new RoleView
                         {
                             RoleId = p.RoleId,
                             Name = p.Name,
                             SecAreaId = p.SecArea.Name,
                             GroupAD = p.GroupAD
                         });

            return View(new GridModel(roles.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Role role)
        {
            if (ModelState.IsValid)
            {
                if (role.RoleId.Equals(0))
                {
                    _rRole.Create(role);
                }
                else
                {
                    _rRole.Update(role); 
                }

                role.SecArea = _rSecArea.Get(p => p.SecAreaId == role.SecAreaId);
            }

            var roles = (from p in _rRole.GetAll().ToList()
                         select new RoleView
                         {
                             RoleId = p.RoleId,
                             Name = p.Name,
                             SecAreaId = p.SecArea.Name,
                             GroupAD = p.GroupAD
                         });

            return View(new GridModel(roles.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var role = _rRole.Get(p => p.RoleId == id);
            _rRole.Delete(role);

            var roles = (from p in _rRole.GetAll().ToList()
                         select new RoleView
                         {
                             RoleId = p.RoleId,
                             Name = p.Name,
                             SecAreaId = p.SecArea.Name,
                             GroupAD = p.GroupAD
                         });

            return View(new GridModel(roles.ToList()));
        }

    }
}
