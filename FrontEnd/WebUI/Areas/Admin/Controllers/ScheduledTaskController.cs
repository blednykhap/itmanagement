﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Filters;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class ScheduledTaskController : Controller
    {
        private readonly IBaseRepository<ScheduledTask> _rScheduledTask;

        public ScheduledTaskController(IBaseRepository<ScheduledTask> rScheduledTask)
        {
            _rScheduledTask = rScheduledTask;
        }

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var scheduledTasks = _rScheduledTask.GetAll();

            return View(new GridModel(scheduledTasks.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(ScheduledTask scheduledTask)
        {
            if (ModelState.IsValid)
            {
                if (scheduledTask.ScheduledTaskId.Equals(0))
                {
                    _rScheduledTask.Create(scheduledTask);
                }
                else
                {
                    _rScheduledTask.Update(scheduledTask);
                }
            }

            var scheduledTasks = _rScheduledTask.GetAll();

            return View(new GridModel(scheduledTasks.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var scheduledTask = _rScheduledTask.Get(p => p.ScheduledTaskId == id);
            _rScheduledTask.Delete(scheduledTask);

            var scheduledTasks = _rScheduledTask.GetAll();

            return View(new GridModel(scheduledTasks.ToList()));
        }

    }
}
