﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;
using Core.Filters;

namespace WebUI.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class LogUserActivityController : Controller
    {        
        private readonly IBaseRepository<LogUserActivity> _rLogUserActivity;        

        public LogUserActivityController(IBaseRepository<LogUserActivity> rLogUserActivity)
        {
            _rLogUserActivity = rLogUserActivity;            
        }

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var logsUserActs = (from p in _rLogUserActivity.GetAll().ToList()
                                select new LogUserActivityView { 
                                    EventDate = p.EventDate,
                                    ComputerIP=p.ComputerIP,
                                    Authid = p.Authid,
                                    SecArea=p.SecArea.Name,
                                    SecController = p.SecController.Name,
                                    SecAction = p.SecAction.Name,
                                   // QueryString = p.QueryString,
                                    Result = p.Result
                                });

            return View(new GridModel(logsUserActs.ToList()));
        }
    }
}