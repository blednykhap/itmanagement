﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Views;
using Core.Models;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class HelpArticleController : Controller
    {
        private readonly IBaseRepository<HelpArticle> _rHelpArticle;

        public HelpArticleController(IBaseRepository<HelpArticle> rHelpArticle)
        {
            _rHelpArticle = rHelpArticle;
        }

        public ActionResult _Index(int currentPage = 1)
        {
            int itemsPerSheet = 5;

            var helpArticles = new HelpArticleListView
            {
                PagingInfo = new PagingInfo()
                {
                    CurrentPage = currentPage,
                    TotalItems = _rHelpArticle.GetAll().Count(),
                    ItemsPerSheet = itemsPerSheet
                },
                HelpArticles = _rHelpArticle.GetAll().ToList()
                    .OrderBy(p => p.HelpArticleId).Skip((currentPage - 1) * itemsPerSheet).Take(itemsPerSheet)
            };

            return PartialView(helpArticles);
        }

        public ActionResult Details(int helpArticleId)
        {
            var helpArticle = _rHelpArticle.Get(p => p.HelpArticleId == helpArticleId);
            return View(helpArticle);
        }

        public ActionResult _Article(string helpArticleContent)
        {
            switch (helpArticleContent)
            {
                case "Article1":
                    {
                        return PartialView("_Article1");                     
                    }
                case "Article2":
                    {
                        return PartialView("_Article2");                       
                    }
                case "Article3":
                    {
                        return PartialView("_Article3");                       
                    }
                case "Article4":
                    {
                        return PartialView("_Article4");                       
                    }
                case "Article5":
                    {
                        return PartialView("_Article5");
                    }
                case "Article6":
                    {
                        return PartialView("_Article6");
                    }
                default:
                    {
                        return PartialView("_Blank");
                    }
            }
        }

        public ActionResult _GeneralActions()
        {
            return PartialView();
        }

    }
}
