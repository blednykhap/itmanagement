﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class SoftwareTypeController : Controller
    {
        private readonly IBaseRepository<SoftwareType> _rSoftwareType;

        public SoftwareTypeController(IBaseRepository<SoftwareType> rSoftwareType)
        {
            _rSoftwareType = rSoftwareType;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var softwareTypes = _rSoftwareType.GetAll();

            return View(new GridModel(softwareTypes.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(SoftwareType softwareType)
        {
            if (ModelState.IsValid)
            {
                if (softwareType.SoftwareTypeId.Equals(0))
                {
                    _rSoftwareType.Create(softwareType);
                }
                else
                {
                    _rSoftwareType.Update(softwareType);
                }
            }

            var softwareTypes = _rSoftwareType.GetAll();

            return View(new GridModel(softwareTypes.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var softwareType = _rSoftwareType.Get(p => p.SoftwareTypeId == id);
            _rSoftwareType.Delete(softwareType);

            var softwareTypes = _rSoftwareType.GetAll();

            return View(new GridModel(softwareTypes.ToList()));
        }

        #endregion

    }
}
