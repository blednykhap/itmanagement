﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Reports;
using System.Collections;
using Telerik.Web.Mvc.Extensions;
using Core.Models;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class PrintController : Controller
    {
        private readonly IExcelForm _excelForm;
        private readonly IReportRepository _reportRepository;
        private readonly IBaseRepository<SystemParameter> _rSystemParameter;

        private int excelMaxRow;

        public PrintController(IExcelForm excelForm, IReportRepository reportRepository,
            IBaseRepository<SystemParameter> rSystemParameter)
        {
            _excelForm = excelForm;
            _reportRepository = reportRepository;
            _rSystemParameter = rSystemParameter;

            var systemParameter = _rSystemParameter.GetList(p => p.Name == "ExcelMaxRow").FirstOrDefault();
            excelMaxRow = (systemParameter != null ? int.Parse(systemParameter.Value) : 4000);
        }

        #region UsedLicense

        public ActionResult _UsedLicense(int page, string orderBy, string filter)
        {
            IEnumerable usedLincese = _reportRepository.UsedLicenseReport().AsQueryable()
                .ToGridModel(page, excelMaxRow, orderBy, string.Empty, filter).Data;

            var result = _excelForm.UsedLicenseExcel(usedLincese);

            return File(result, "application/vnd.ms-excel", "UsedLicense.xls");
        }

        #endregion

        #region InstalledSoftware

        public ActionResult _InstalledSoftware(int page, string orderBy, string filter)
        {
            IEnumerable installedSoftware = _reportRepository.InstalledSoftwareReport().AsQueryable()
                .ToGridModel(page, excelMaxRow, orderBy, string.Empty, filter).Data;

            var result = _excelForm.InstalledSoftwareExcel(installedSoftware);

            return File(result, "application/vnd.ms-excel", "InstalledSoftware.xls");
        }

        #endregion

        #region MaintenanceCrossReport

        //public ActionResult _MaintenanceCross(int companyId = 0, int codeMonth = 0)
        //{
        //    if (companyId == 0 || codeMonth == 0)
        //    {
        //        TempData["Message"] = "Не заполнены параметры отчета";
        //        return RedirectToAction("");
        //    }
        //    else
        //    {
        //        //var result = _excelForm.MaintenanceCross();
        //        return File(result, "application/vnd.ms-excel", "MaintenanceCrossReport.xls");
        //    }
            
        //}

        #endregion

        #region R24HoursMaintenance

        public ActionResult _R24HoursMaintenance(int page, string orderBy, string filter)
        {
            var period = Convert.ToInt32(Session["CurrentCodeMonth"]);
            IEnumerable r24HoursMaintenance = _reportRepository.R24HoursMaintenanceReport(period).AsQueryable()
                .ToGridModel(page, excelMaxRow, orderBy, string.Empty, filter).Data;

            var result = _excelForm.R24HoursMaintenanceExcel(r24HoursMaintenance);

            return File(result, "application/vnd.ms-excel", "InstalledSoftware.xls");
        }

        #endregion

    }
}
