﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class ComputerSystemController : Controller
    {
        private readonly IBaseRepository<ComputerSystem> _rComputerSystem;

        public ComputerSystemController(IBaseRepository<ComputerSystem> rComputerSystem)
        {
            _rComputerSystem = rComputerSystem;
        }

        [NoCacheFilter]
        public ActionResult _Details(int employeeId)
        {
            var computerSystem = _rComputerSystem.GetList(p => p.EmployeeId == employeeId && p.IsActive == true).ToList();

            if (computerSystem != null)
            {
                return PartialView(computerSystem);
            }
            else
            {
                return Content("Данные не найдены.");
            }            
        }

    }
}
