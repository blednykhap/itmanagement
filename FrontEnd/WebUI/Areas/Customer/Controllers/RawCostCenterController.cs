﻿using Core.Models;
using Core.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using System.IO;
using System.Data.OleDb;
using System.Data;
using Business.ServerSide;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class RawCostCenterController : Controller
    {
        private readonly IBaseRepository<RawCostCenter> _rRawCostCenter;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<CostCenter> _rCostCenter;
        private readonly IImportDataServer _sImportDataServer;

        public RawCostCenterController(IBaseRepository<RawCostCenter> rRawCostCenter,
            IBaseRepository<Company> rCompany, IBaseRepository<CostCenter> rCostCenter,
            IImportDataServer sImportDataServer)
        {
            _rRawCostCenter = rRawCostCenter;
            _rCompany = rCompany;
            _rCostCenter = rCostCenter;
            _sImportDataServer = sImportDataServer;
        }

        public ActionResult Index()
        {
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.CostCenters = new SelectList(_rCostCenter.GetAll(), "CostCenterId", "Code");
            return View();
        }

        #region Import

        public void _LoadFile(HttpPostedFileBase attachments, int companyId = 0)
        {
            if (attachments != null && companyId != 0 && companyId != 1)
            {
                _rRawCostCenter.BulkDelete("RawCostCenters");

                var fileName = attachments.FileName;
                string connectionString = "";

                FileInfo fileInfo = new FileInfo(fileName);

                if (fileInfo.Extension == ".xlsx")
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties=Excel 12.0", fileName);
                }
                else
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0", fileName);
                }

                var adapter = new OleDbDataAdapter("SELECT * FROM [Лист1$]", connectionString);                
                var dataSet = new DataSet();

                adapter.Fill(dataSet, "CostCenters");
                DataTable dataTable = dataSet.Tables["CostCenters"];
                string excelCode = "";

                foreach (DataRow row in dataTable.Rows)
                {
                    excelCode = row.ItemArray[0].ToString().TrimStart('0');
                    _rRawCostCenter.Create(new RawCostCenter() { 
                        CompanyId = companyId,
                        ExcelCode = excelCode,
                        ExcelName = row.ItemArray[1].ToString()
                    });
                }
            }
        }

        public ContentResult _Parse()
        {
            _sImportDataServer.ParseCostCenter();
            return Content("Данные обработаны");
        }

        public ContentResult _Load()
        {
            _sImportDataServer.LoadCostCenter();
            return Content("Данные обработаны");
        }

        #endregion

        #region CRUD

        [GridAction]
        public ActionResult _Index(/*int companyId = 0*/)
        {
            var rawCostCenters = (from p in _rRawCostCenter.GetAll().ToList()
                               //   where p.CompanyId == companyId
                                  select new RawCostCenterView { 
                                      RawCostCenterId = p.RawCostCenterId,
                                      ExcelCode = p.ExcelCode,
                                      ExcelName = p.ExcelName,
                                      CompanyId = p.Company != null ? p.Company.Name : "",
                                      CostCenterId = p.CostCenter != null ? p.CostCenter.Code : ""                                       
                                  });

            return View(new GridModel(rawCostCenters));
        }

        [GridAction]
        public ActionResult _Update(RawCostCenter rawCostCenter)
        {

            _rRawCostCenter.Update(rawCostCenter);
            rawCostCenter.Company = _rCompany.Get(p => p.CompanyId == rawCostCenter.CompanyId);
            rawCostCenter.CostCenter = _rCostCenter.Get(p => p.CostCenterId == rawCostCenter.CostCenterId);

            var rawCostCenters = (from p in _rRawCostCenter.GetAll().ToList()
                                  select new RawCostCenterView
                                  {
                                      RawCostCenterId = p.RawCostCenterId,
                                      ExcelCode = p.ExcelCode,
                                      ExcelName = p.ExcelName,
                                      CompanyId = p.Company != null ? p.Company.Name : "",
                                      CostCenterId = p.CostCenter != null ? p.CostCenter.Code : ""  
                                  });

            return View(new GridModel(rawCostCenters));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var rawCostCenter = _rRawCostCenter.Get(p => p.RawCostCenterId == id);
            _rRawCostCenter.Delete(rawCostCenter);

            var rawCostCenters = (from p in _rRawCostCenter.GetAll().ToList()
                                  select new RawCostCenterView
                                  {
                                      RawCostCenterId = p.RawCostCenterId,
                                      ExcelCode = p.ExcelCode,
                                      ExcelName = p.ExcelName,
                                      CompanyId = p.Company != null ? p.Company.Name : "",
                                      CostCenterId = p.CostCenter != null ? p.CostCenter.Code : ""  
                                  });

            return View(new GridModel(rawCostCenters));
        }

        #endregion

    }
}
