﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class InstalledSoftwareController : Controller
    {
        private readonly IBaseRepository<InstalledSoftware> _rInstalledSoftware;
        private readonly IBaseRepository<ComputerSystem> _rComputerSystem;

        public InstalledSoftwareController(IBaseRepository<InstalledSoftware> rInstalledSoftware,
            IBaseRepository<ComputerSystem> rComputerSystem)
        {
            _rInstalledSoftware = rInstalledSoftware;
            _rComputerSystem = rComputerSystem;
        }

        public ActionResult _Index(int currentPage = 1, int dataId = 0)
        {
            int itemsPerSheet = 12;
            ViewBag.EmployeeId = dataId;

            var computerSystem = _rComputerSystem
                .GetList(p => p.EmployeeId == dataId && p.IsActive == true).FirstOrDefault();

            if (computerSystem != null)
            {
                var data = (from p in _rInstalledSoftware
                                .GetList(t => t.ResourceId == computerSystem.ResourceId && t.Software != null).ToList()
                            select new InstalledSoftwareView
                            {
                                InstalledSoftwareId = p.InstalledSoftwareId,
                                Software = p.Software.Name
                            });

                var installedSoftwares = new InstalledSoftwareListView
                {
                    PagingInfo = new PagingInfo()
                    {
                        CurrentPage = currentPage,
                        TotalItems = data.Count(),
                        ItemsPerSheet = itemsPerSheet
                    },
                    InstalledSoftwares = data.OrderBy(p => p.InstalledSoftwareId)
                        .Skip((currentPage - 1) * itemsPerSheet).Take(itemsPerSheet)
                };

                return PartialView(installedSoftwares);
            }
            else
            {
                return Content("Данные не найдены.");
            }

        }

    }
}
