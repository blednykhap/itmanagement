﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class ServiceLineController : Controller
    {
        private readonly IBaseRepository<ServiceLine> _rServiceLine;

        public ServiceLineController(IBaseRepository<ServiceLine> rServiceLine)
        {
            _rServiceLine = rServiceLine;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var serviceLines = _rServiceLine.GetAll();

            return View(new GridModel(serviceLines.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(ServiceLine serviceLine)
        {
            if (ModelState.IsValid)
            {
                if (serviceLine.ServiceLineId.Equals(0))
                {
                    _rServiceLine.Create(serviceLine);
                }
                else
                {
                    _rServiceLine.Update(serviceLine);
                }
            }

            var serviceLines = _rServiceLine.GetAll();

            return View(new GridModel(serviceLines.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var serviceLine = _rServiceLine.Get(p => p.ServiceLineId == id);
            _rServiceLine.Delete(serviceLine);

            var serviceLines = _rServiceLine.GetAll();

            return View(new GridModel(serviceLines.ToList()));
        }

        #endregion

    }
}
