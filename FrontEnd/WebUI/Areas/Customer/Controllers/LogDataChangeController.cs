﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Telerik.Web.Mvc;
using Core.Views;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class LogDataChangeController : Controller
    {
        public readonly IBaseRepository<LogDataChange> _rLogDataChange;

        public LogDataChangeController(IBaseRepository<LogDataChange> rLogDataChange)
        {
            _rLogDataChange = rLogDataChange;
        }

        [ChildActionOnly]
        public ActionResult _Index(int logAppEventId)
        {
            ViewBag.LogAppEventId = logAppEventId;            
            return PartialView();
        }

        [GridAction]
        public ActionResult AjaxIndex(int logAppEventId)
        {
            var logDataChanges = (from p in _rLogDataChange.GetList(p => p.LogAppEventId == logAppEventId).ToList()
                                  select new LogDataChangeView { 
                                      TargetTable = p.TargetTable,
                                      DML = (p.DML == "INSERT" ? 
                                                "Вставка" : 
                                                (p.DML == "ERROR" ? "Ошибка" : "Обновление")),
                                      Comment = p.Comment
                                  });

            return View(new GridModel(logDataChanges.ToList()));
        }
    }
}
