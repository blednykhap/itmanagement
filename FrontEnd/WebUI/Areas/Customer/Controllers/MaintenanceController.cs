﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Views;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class MaintenanceController : Controller
    {       
        private readonly IBaseRepository<Maintenance> _rMaintenance;        
        private readonly IBaseRepository<ServiceLine> _rServiceLine;

        public MaintenanceController(IBaseRepository<Maintenance> rMaintenance, IBaseRepository<ServiceLine> rServiceLine)
        {
            _rMaintenance = rMaintenance;            
            _rServiceLine = rServiceLine;
        }

        [HttpPost]
        public JsonResult _Selection()
        {
            var maintenance = _rMaintenance.GetAll().ToList();
            maintenance.Add(new Maintenance { MaintenanceId = 0, Name = "Пустое значение" });

            return new JsonResult { Data = new SelectList(maintenance.OrderBy(p => p.MaintenanceId), "MaintenanceId", "Name") };
        }

        public ActionResult Index()
        {            
            ViewBag.ServiceLines = new SelectList(_rServiceLine.GetAll(), "ServiceLineId", "Code");

            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var maintenances = (from p in _rMaintenance.GetAll().ToList()
                                select new MaintenanceView {
                                    MaintenanceId = p.MaintenanceId,
                                    Code = p.Code,            
                                    Name = p.Name,
                                    ServiceLineId = p.ServiceLine.Code,
                                    ContainerAD = p.ContainerAD,
                                    Is24Hours = p.Is24Hours,
                                        //!string.IsNullOrEmpty(p.ContainerAD) ?
                                        //    (p.ContainerAD.Trim().Length > 40 ? p.ContainerAD.Trim().Substring(0, 25) + " ..." : p.ContainerAD.Trim()) : 
                                        //    "",
                                    Comment = p.Comment
                                });
            return View(new GridModel(maintenances.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Maintenance maintenance)
        {

            if (ModelState.IsValid)
            {
                if (maintenance.MaintenanceId.Equals(0))
                {
                    _rMaintenance.Create(maintenance);
                }
                else
                {
                    _rMaintenance.Update(maintenance);
                }

                maintenance.ServiceLine = _rServiceLine.Get(p => p.ServiceLineId == maintenance.ServiceLineId);                
            }

            var maintenances = (from p in _rMaintenance.GetAll().ToList()
                                select new MaintenanceView
                                {
                                    MaintenanceId = p.MaintenanceId,
                                    Code = p.Code,
                                    Name = p.Name,
                                    ServiceLineId = p.ServiceLine.Code,
                                    ContainerAD = p.ContainerAD,
                                    Is24Hours = p.Is24Hours,
                                    Comment = p.Comment
                                });
            return View(new GridModel(maintenances.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var maintenance = _rMaintenance.Get(p => p.MaintenanceId == id);
            _rMaintenance.Delete(maintenance);            

            var maintenances = (from p in _rMaintenance.GetAll().ToList()
                                select new MaintenanceView
                                {
                                    MaintenanceId = p.MaintenanceId,
                                    Code = p.Code,
                                    Name = p.Name,
                                    ServiceLineId = p.ServiceLine.Code,
                                    ContainerAD = p.ContainerAD,
                                    Is24Hours = p.Is24Hours,
                                    Comment = p.Comment
                                });
            return View(new GridModel(maintenances.ToList()));
        }

        #endregion

    }
}
