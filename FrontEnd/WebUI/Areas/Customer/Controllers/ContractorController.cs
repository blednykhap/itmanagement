﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class ContractorController : Controller
    {
        private readonly IBaseRepository<Contractor> _rContractor;

        public ContractorController(IBaseRepository<Contractor> rContractor)
        {
            _rContractor = rContractor;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            return View(new GridModel(_rContractor.GetAll().ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Contractor contractor)
        {
            if (ModelState.IsValid)
            {
                if (contractor.ContractorId.Equals(0))
                {
                    _rContractor.Create(contractor);
                }
                else
                {
                    _rContractor.Update(contractor);
                }
            }

            return View(new GridModel(_rContractor.GetAll().ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var contractor = _rContractor.Get(p => p.ContractorId == id);
            _rContractor.Delete(contractor);

            return View(new GridModel(_rContractor.GetAll().ToList()));
        }

        #endregion

    }
}
