﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class CompanyController : Controller
    {
        private readonly IBaseRepository<Company> _rCompany;

        public CompanyController(IBaseRepository<Company> rCompany)
        {
            _rCompany = rCompany;
        }

        [HttpPost]
        public JsonResult _Index()
        {
            int companyId;

            if (Session["PreferCompanyId"] != null)
            {
                companyId = int.Parse(Session["PreferCompanyId"].ToString());
            }
            else
            {
                companyId = 0;
            }

            return new JsonResult { Data = new SelectList(_rCompany.GetAll(), "CompanyId", "Name", companyId) };
        }

    }
}
