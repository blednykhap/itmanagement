﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class ContractorSoftwareController : Controller
    {
        private readonly IBaseRepository<ContractorSoftware> _rContractorSoftware;
        private readonly IBaseRepository<Contractor> _rContractor;
        private readonly IBaseRepository<Software> _rSoftware;
        private readonly IBaseRepository<SoftwareType> _rSoftwareType;

        public ContractorSoftwareController(IBaseRepository<ContractorSoftware> rContractorSoftware,
                                             IBaseRepository<Contractor> rContractor,
                                             IBaseRepository<Software> rSoftware,
                                             IBaseRepository<SoftwareType> rSoftwareType)
        {
            _rContractorSoftware = rContractorSoftware;
            _rContractor = rContractor;
            _rSoftware = rSoftware;
            _rSoftwareType = rSoftwareType;

        }

        public ActionResult Index()
        {
            ViewBag.Contractors = new SelectList(_rContractor.GetAll(), "ContractorId", "Name");
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");
            ViewBag.SoftwareTypes = new SelectList(_rSoftwareType.GetAll(), "SoftwareTypeId", "Name");

            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var contrSofts = (from p in _rContractorSoftware.GetAll().ToList()
                              select new ContractorSoftwareView
                              {
                                  ContractorSoftwareId = p.ContractorSoftwareId,
                                  OriginalContractorId = p.OriginalContractorId != null ? p.OriginalContractor.Name : "",
                                  GeneralContractorId = p.GeneralContractorId != null ? p.GeneralContractor.Name : "",
                                  OriginalSoftwareId = p.OriginalSoftwareId != null ? p.OriginalSoftware.Name : "",
                                  GeneralSoftwareId = p.GeneralSoftwareId != null ? p.GeneralSoftware.Name : "",
                                  SoftwareTypeId = p.SoftwareTypeId != null ? p.SoftwareType.Name : ""
                              });
            return View(new GridModel(contrSofts.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(ContractorSoftware contrSoft)
        {
            if (ModelState.IsValid)
            {
                if (contrSoft.ContractorSoftwareId.Equals(0))
                {
                    _rContractorSoftware.Create(contrSoft);
                }
                else
                {
                    _rContractorSoftware.Update(contrSoft);
                }

                contrSoft.OriginalContractor = _rContractor.Get(p => p.ContractorId == contrSoft.OriginalContractorId);
                contrSoft.GeneralContractor = _rContractor.Get(p => p.ContractorId == contrSoft.GeneralContractorId);
                contrSoft.OriginalSoftware = _rSoftware.Get(p => p.SoftwareId == contrSoft.OriginalSoftwareId);
                contrSoft.GeneralSoftware = _rSoftware.Get(p => p.SoftwareId == contrSoft.GeneralSoftwareId);
                contrSoft.SoftwareType = _rSoftwareType.Get(p => p.SoftwareTypeId == contrSoft.SoftwareTypeId);
            }

            var contrSofts = (from p in _rContractorSoftware.GetAll().ToList()
                              select new ContractorSoftwareView
                              {
                                  ContractorSoftwareId = p.ContractorSoftwareId,
                                  OriginalContractorId = p.OriginalContractorId != null ? p.OriginalContractor.Name : "",
                                  GeneralContractorId = p.GeneralContractorId != null ? p.GeneralContractor.Name : "",
                                  OriginalSoftwareId = p.OriginalSoftwareId != null ? p.OriginalSoftware.Name : "",
                                  GeneralSoftwareId = p.GeneralSoftwareId != null ? p.GeneralSoftware.Name : "",
                                  SoftwareTypeId = p.SoftwareTypeId != null ? p.SoftwareType.Name : ""
                              });
            return View(new GridModel(contrSofts.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var contrSoft = _rContractorSoftware.Get(p => p.ContractorSoftwareId == id);
            _rContractorSoftware.Delete(contrSoft);

            var contrSofts = (from p in _rContractorSoftware.GetAll().ToList()
                              select new ContractorSoftwareView
                              {
                                  ContractorSoftwareId = p.ContractorSoftwareId,
                                  OriginalContractorId = p.OriginalContractorId != null ? p.OriginalContractor.Name : "",
                                  GeneralContractorId = p.GeneralContractorId != null ? p.GeneralContractor.Name : "",
                                  OriginalSoftwareId = p.OriginalSoftwareId != null ? p.OriginalSoftware.Name : "",
                                  GeneralSoftwareId = p.GeneralSoftwareId != null ? p.GeneralSoftware.Name : "",
                                  SoftwareTypeId = p.SoftwareTypeId != null ? p.SoftwareType.Name : ""
                              });
            return View(new GridModel(contrSofts.ToList()));
        }

        #endregion

    }
}
