﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class EmployeeController : Controller
    {
        private readonly IBaseRepository<Employee> _rEmployee;
        private readonly IBaseRepository<Department> _rDepartment;        
        private readonly IBaseRepository<CostCenter> _rCostCenter;
        private readonly IBaseRepository<Position> _rPosition;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<CompanyStructure> _rCompanyStructure;

        public EmployeeController(IBaseRepository<Employee> rEmployee, IBaseRepository<Department> rDepartment,
            IBaseRepository<CostCenter> rCostCenter, IBaseRepository<Position> rPosition,
            IBaseRepository<Company> rCompany, IBaseRepository<CompanyStructure> rCompanyStructure)
        {
            _rEmployee = rEmployee;
            _rDepartment = rDepartment;
            _rCostCenter = rCostCenter;
            _rPosition = rPosition;
            _rCompany = rCompany;
            _rCompanyStructure = rCompanyStructure;
        }

        public ActionResult Index()
        {            
            IEnumerable<DepartmentShortView> departments =
                (from p in _rDepartment.GetList(p => p.IsActive == true).ToList()
                 select new DepartmentShortView { 
                     DepartmentId = p.DepartmentId,
                     Name = (p.CostCenter != null ? p.CostCenter.Code + " - " : "") + p.Name
                 });
            ViewBag.Departments = new SelectList(departments, "DepartmentId", "Name");

            ViewBag.Positions = new SelectList(_rPosition.GetAll(), "PositionId", "Name");
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");

            IEnumerable<CostCenterShortView> costCenters =
                (from p in _rCostCenter.GetAll().ToList()
                 select new CostCenterShortView
                 {
                     CostCenterId = p.CostCenterId,
                     Name = string.Concat(p.Code, " - ", p.Name)
                 });
            ViewBag.CostCenters = new SelectList(costCenters.ToList(), "CostCenterId", "Name");            
            
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index(int companyId = 0)
        {
            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            var employees = (from p in _rEmployee.GetAll().ToList()
                             where p.CompanyId == companyId && (isDisplayActiveOnly ? p.IsActive == true : true)
                             select new EmployeeView {
                                 EmployeeId = p.EmployeeId,
                                 Authid = p.Authid,
                                 CostCenterId = (p.CostCenter != null ? string.Concat(p.CostCenter.Code, " - ", p.CostCenter.Name) : ""),
                                 DepartmentId = (p.Department != null ?
                                    (p.Department.CostCenter != null ? p.Department.CostCenter.Code + " - " : "") + p.Department.Name : ""),
                                 CompanyId = (p.Company != null ? p.Company.Name : ""),
                                 DischargeDate = p.DischargeDate,                                 
                                 Surname = p.Surname,
                                 Firstname = p.Firstname,
                                 Middlename = p.Middlename,
                                 Fullname = p.Fullname,
                                 PositionId = (p.Position != null ? p.Position.Name : ""),
                                 Status = (p.IsActive ? "Работает" : "Уволен"),
                                 IsActive = p.IsActive
                                 //ComputerSystem = (p.ComputerSystems != null ? 
                                 //   p.ComputerSystems.Where(s => s.IsActive == true).FirstOrDefault() : null)
                             });

            return View(new GridModel(employees.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Employee employee)
        {
            int companyId = 0;

            if (ModelState.IsValid)
            {
                if (employee.EmployeeId.Equals(0))
                {
                    if (employee.DischargeDate == null)
                    {
                        employee.IsActive = true;
                        employee.DischargeDate = Convert.ToDateTime("31.12.2099 23:59:59");
                    }
                    else
                    {
                        if (employee.DischargeDate > DateTime.Now)
                        {
                            employee.IsActive = true;
                        }
                    }
                    _rEmployee.Create(employee);
                }
                else
                {
                    if (employee.DischargeDate != null && employee.DischargeDate > DateTime.Now)
                    {
                        employee.IsActive = true;
                    }
                    else
                    {
                        employee.IsActive = false;
                    }
                    _rEmployee.Update(employee);
                }

                // TODO Check it!
                if (employee.DepartmentId != null)
                {
                    var companyStructure = _rCompanyStructure.Get(p => p.EmployeeId == employee.EmployeeId);
                    var parentCompanyStructure = _rCompanyStructure.Get(p => p.DepartmentId == employee.DepartmentId);
                    if (companyStructure == null && parentCompanyStructure != null)
                    {
                        _rCompanyStructure.Create(new CompanyStructure
                        {
                            CompanyId = employee.CompanyId,
                            CreatedBy = "EKC",
                            EmployeeId = employee.EmployeeId,
                            IsActive = true,
                            ParentCompanyStructureId = parentCompanyStructure.CompanyStructureId
                        });
                    }
                    else
                    {
                        if (parentCompanyStructure != null)
                        {
                            companyStructure.ParentCompanyStructureId = parentCompanyStructure.CompanyStructureId;
                            _rCompanyStructure.Update(companyStructure);
                        }
                    }
                }

                companyId = employee.CompanyId;
                employee.CostCenter = _rCostCenter.Get(p => p.CostCenterId == employee.CostCenterId);
                employee.Department = _rDepartment.Get(p => p.DepartmentId == employee.DepartmentId);
                employee.Position = _rPosition.Get(p => p.PositionId == employee.PositionId);
                employee.Company = _rCompany.Get(p => p.CompanyId == employee.CompanyId);
            }

            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            var employees = (from p in _rEmployee.GetAll().ToList()                             
                             where p.CompanyId == companyId && (isDisplayActiveOnly ? p.IsActive == true : true)
                             select new EmployeeView
                             {
                                 EmployeeId = p.EmployeeId,
                                 Authid = p.Authid,
                                 CostCenterId = (p.CostCenter != null ? string.Concat(p.CostCenter.Code, " - ", p.CostCenter.Name) : ""),
                                 DepartmentId = (p.Department != null ?
                                    (p.Department.CostCenter != null ? p.Department.CostCenter.Code + " - " : "") + p.Department.Name : ""),
                                 CompanyId = (p.Company != null ? p.Company.Name : ""),
                                 DischargeDate = p.DischargeDate,                                 
                                 Surname = p.Surname,
                                 Firstname = p.Firstname,
                                 Middlename = p.Middlename,
                                 Fullname = p.Fullname,
                                 PositionId = (p.Position != null ? p.Position.Name : ""),
                                 Status = (p.IsActive ? "Работает" : "Уволен"),
                                 IsActive = p.IsActive
                             });

            return View(new GridModel(employees.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var employee = _rEmployee.Get(p => p.EmployeeId == id);
            var companyStructure = _rCompanyStructure.Get(p => p.EmployeeId == id);
            int companyId = employee.CompanyId;
            if (companyStructure != null)
            {
                _rCompanyStructure.Delete(companyStructure);
            }
            _rEmployee.Delete(employee);

            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            var employees = (from p in _rEmployee.GetAll().ToList()
                             where p.CompanyId == companyId && (isDisplayActiveOnly ? p.IsActive == true : true)                             
                             select new EmployeeView
                             {
                                 Authid = p.Authid,
                                 CostCenterId = (p.CostCenter != null ? string.Concat(p.CostCenter.Code, " - ", p.CostCenter.Name) : ""),
                                 DepartmentId = (p.Department != null ?
                                    (p.Department.CostCenter != null ? p.Department.CostCenter.Code + " - " : "") + p.Department.Name : ""),
                                 CompanyId = (p.Company != null ? p.Company.Name : ""),
                                 DischargeDate = p.DischargeDate,
                                 EmployeeId = p.EmployeeId,
                                 Surname = p.Surname,
                                 Firstname = p.Firstname,
                                 Middlename = p.Middlename,
                                 Fullname = p.Fullname,
                                 PositionId = (p.Position != null ? p.Position.Name : ""),
                                 Status = (p.IsActive ? "Работает" : "Уволен"),
                                 IsActive = p.IsActive
                             });

            return View(new GridModel(employees.ToList()));
        }

        #endregion

    }
}
