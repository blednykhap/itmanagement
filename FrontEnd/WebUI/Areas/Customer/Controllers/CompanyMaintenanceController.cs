﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class CompanyMaintenanceController : Controller
    {
        private readonly IBaseRepository<CompanyMaintenance> _rCompanyMaintenance;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<Maintenance> _rMaintenance;

        public CompanyMaintenanceController(IBaseRepository<CompanyMaintenance> rCompanyMaintenance, IBaseRepository<Company> rCompany,
            IBaseRepository<Maintenance> rMaintenance)
        {
            _rCompanyMaintenance = rCompanyMaintenance;
            _rCompany = rCompany;
            _rMaintenance = rMaintenance;
        }

        public ActionResult Index()
        {
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.Maintenances = new SelectList(_rMaintenance.GetAll(), "MaintenanceId", "Name");

            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index(int companyId = 0)
        {            
            var companyMaintenances = (from p in _rCompanyMaintenance.GetAll().ToList()
                                       where p.CompanyId == companyId
                                       select new CompanyMaintenanceView
                                       {
                                           CompanyMaintenanceId = p.CompanyMaintenanceId,
                                           CompanyId = p.Company.Name,
                                           MaintenanceId = p.Maintenance.Name,
                                           MaintenanceCode = p.Maintenance.Code,
                                           ServiceLineName = p.Maintenance.ServiceLine.Code
                                       });

            return View(new GridModel(companyMaintenances.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(CompanyMaintenance companyMaintenance)
        {
            if (ModelState.IsValid)
            {
                if (companyMaintenance.CompanyMaintenanceId.Equals(0))
                {
                    _rCompanyMaintenance.Create(companyMaintenance);
                }
                else
                {
                    _rCompanyMaintenance.Update(companyMaintenance);
                }

                companyMaintenance.Company = _rCompany.Get(p => p.CompanyId == companyMaintenance.CompanyId);
                companyMaintenance.Maintenance = _rMaintenance.Get(p => p.MaintenanceId == companyMaintenance.MaintenanceId);                
            }

            var companyMaintenances = (from p in _rCompanyMaintenance.GetAll().ToList()
                                       where p.CompanyId == companyMaintenance.CompanyId
                                       select new CompanyMaintenanceView
                                       {
                                           CompanyMaintenanceId = p.CompanyMaintenanceId,
                                           CompanyId = p.Company.Name,
                                           MaintenanceId = p.Maintenance.Name,
                                           MaintenanceCode = p.Maintenance.Code,
                                           ServiceLineName = p.Maintenance.ServiceLine.Code
                                       });

            return View(new GridModel(companyMaintenances.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var companyMaintenance = _rCompanyMaintenance.Get(p => p.CompanyMaintenanceId == id);
            var companyId = companyMaintenance.CompanyId;
            _rCompanyMaintenance.Delete(companyMaintenance);

            var companyMaintenances = (from p in _rCompanyMaintenance.GetAll().ToList()
                                       where p.CompanyId == companyId
                                       select new CompanyMaintenanceView
                                       {
                                           CompanyMaintenanceId = p.CompanyMaintenanceId,
                                           CompanyId = p.Company.Name,
                                           MaintenanceId = p.Maintenance.Name,
                                           MaintenanceCode = p.Maintenance.Code,
                                           ServiceLineName = p.Maintenance.ServiceLine.Code
                                       });

            return View(new GridModel(companyMaintenances.ToList()));
        }

        #endregion

    }
}
