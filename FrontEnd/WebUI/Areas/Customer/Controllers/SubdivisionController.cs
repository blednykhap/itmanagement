﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class SubdivisionController : Controller
    {
        private readonly IBaseRepository<Subdivision> _rSubdivision;

        public SubdivisionController(IBaseRepository<Subdivision> rSubdivision)
        {
            _rSubdivision = rSubdivision;
        }

        [HttpPost]
        public JsonResult _Index()
        {
            int SubdivisionId;

            if (Session["PreferSubdivisionId"] != null)
            {
                SubdivisionId = int.Parse(Session["SubdivisionCompanyId"].ToString());
            }
            else
            {
                SubdivisionId = 0;
            }

            return new JsonResult { Data = new SelectList(_rSubdivision.GetAll(), "SubdivisionId", "Name", SubdivisionId) };
        }

    }
}

