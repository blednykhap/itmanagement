﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Telerik.Web.Mvc;
using Core.Views;
using Business.Others;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class MaintenanceOrderController : Controller
    {
        private readonly IBaseRepository<MaintenanceOrder> _rMaintenanceOrder;
        private readonly IBaseRepository<Maintenance> _rMaintenance;        
        private readonly IBaseRepository<CompanyStructure> _rCompanyStructure;
        private readonly IBaseRepository<Tariff> _rTariff;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<Component> _rComponent;
        private readonly IBaseRepository<Software> _rSoftware;
        private readonly IBaseRepository<SupportLevel> _rSupportLevel;
        private readonly IBaseRepository<SoftwareModule> _rSoftwareModule;
        private readonly ICalendarLogic _lCalendar;
        private int _currentCodeMonth;

        public MaintenanceOrderController(IBaseRepository<MaintenanceOrder> rMaintenanceOrder,
            IBaseRepository<Maintenance> rMaintenance, IBaseRepository<Company> rCompany,
            IBaseRepository<Tariff> rTariff, IBaseRepository<CompanyStructure> rCompanyStructure,
            IBaseRepository<Component> rComponent, IBaseRepository<Software> rSoftware,
            IBaseRepository<SupportLevel> rSupportLevel, IBaseRepository<SoftwareModule> rSoftwareModule, 
            ICalendarLogic lCalendar)
        {
            _rMaintenanceOrder = rMaintenanceOrder;
            _rMaintenance = rMaintenance;
            _rCompanyStructure = rCompanyStructure;
            _rTariff = rTariff;
            _rCompany = rCompany;
            _rComponent = rComponent;
            _rSoftware = rSoftware;
            _rSupportLevel = rSupportLevel;
            _rSoftwareModule = rSoftwareModule;
            _lCalendar = lCalendar;            
        }

        public ActionResult _Index(int currentPage = 1, int dataId = 0)
        {
            int itemsPerSheet = 12;
            ViewBag.EmployeeId = dataId;

            var data = _rMaintenanceOrder.GetList(p => p.CompanyStructure.EmployeeId == dataId);
            _currentCodeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);

            if (data.Any())
            {
                var maintenanceOrders = new MaintenanceOrderListView
                {
                    PagingInfo = new PagingInfo()
                    {
                        CurrentPage = currentPage,
                        TotalItems = data.Count(),
                        ItemsPerSheet = itemsPerSheet
                    },
                    MaintenanceOrders = (from p in data.OrderByDescending(p => p.MaintenanceOrderId)
                                                .Skip((currentPage - 1) * itemsPerSheet).Take(itemsPerSheet).ToList()
                                         where p.CodeMonthFrom <= _currentCodeMonth && _currentCodeMonth <= p.CodeMonthTo
                                         select new MaintenanceOrderView
                                         {
                                             MaintenanceOrderId = p.MaintenanceOrderId,
                                             MaintenanceId = p.Maintenance.Name
                                         })
                };

                return PartialView(maintenanceOrders);
            }
            else
            {
                return Content("Данные не найдены.");
            }
        }

        #region CRUD

        #region Hierarchy View

        public ActionResult IndexHierarchy()
        {
            ViewBag.Maintenances = new SelectList(_rMaintenance.GetAll(), "MaintenanceId", "Name");
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.Components = new SelectList(_rComponent.GetAll(), "ComponentId", "Code");
            ViewBag.SupportLevels = new SelectList(_rSupportLevel.GetAll(), "SupportLevelId", "Name");                        
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");
            ViewBag.Periods = new SelectList(_lCalendar.GetPeriods(), "CodeMonth", "Name");
            ViewBag.SoftwareModules = new SelectList(_rSoftwareModule.GetAll(), "SoftwareModuleId", "Name");

            return View();
        }

        [GridAction]
        public ActionResult _IndexHierarchy(int companyStructureId = 0)
        {
            _currentCodeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);

            var maintenanceOrders = (from p in _rMaintenanceOrder.GetAll().ToList()
                                     where p.CompanyStructureId == companyStructureId &&
                                        p.CodeMonthFrom <= _currentCodeMonth && _currentCodeMonth <= p.CodeMonthTo
                                     select new MaintenanceOrderHierarchyView
                                     {
                                         MaintenanceOrderId = p.MaintenanceOrderId,                                            
                                         CompanyStructureId = p.CompanyStructureId,
                                         MaintenanceId = p.Maintenance.Name,
                                         CompanyId = p.Company.Name,
                                         ComponentId = (p.Component != null ? p.Component.Code : ""),
                                         SupportLevelId = (p.SupportLevel != null ? p.SupportLevel.Name : ""),
                                         SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                         SoftwareModuleId = (p.SoftwareModule != null ? p.SoftwareModule.Name : ""),
                                         TariffId = (p.Tariff != null ?
                                             p.Tariff.Maintenance.Name + " - " + p.Tariff.SupportLevel.Name + " - " + p.Tariff.Price.ToString() : ""),                                         
                                         CodeMonthFrom = p.CodeMonthFrom,
                                         CodeMonthTo = p.CodeMonthTo,
                                         Total = p.Total,
                                         Comment = p.Comment
                                     });

            return View(new GridModel(maintenanceOrders.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdateHierarchy(MaintenanceOrder maintenanceOrder)
        {
            _currentCodeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);

            if (ModelState.IsValid)
            {
                maintenanceOrder.MakeDate = DateTime.Now;
                if (maintenanceOrder.MaintenanceOrderId.Equals(0))
                {
                    _rMaintenanceOrder.Create(maintenanceOrder);
                }
                else
                {
                    _rMaintenanceOrder.Update(maintenanceOrder);
                }

                maintenanceOrder.Maintenance = _rMaintenance.Get(p => p.MaintenanceId == maintenanceOrder.MaintenanceId);
                maintenanceOrder.CompanyStructure = _rCompanyStructure.Get(p => p.CompanyStructureId == maintenanceOrder.CompanyStructureId);
                maintenanceOrder.Tariff = _rTariff.Get(p => p.TariffId == maintenanceOrder.TariffId);
                maintenanceOrder.Company = _rCompany.Get(p => p.CompanyId == maintenanceOrder.CompanyId);
                maintenanceOrder.Component = _rComponent.Get(p => p.ComponentId == maintenanceOrder.ComponentId);
                maintenanceOrder.Software = _rSoftware.Get(p => p.SoftwareId == maintenanceOrder.SoftwareId);
                maintenanceOrder.SupportLevel = _rSupportLevel.Get(p => p.SupportLevelId == maintenanceOrder.SupportLevelId);
                maintenanceOrder.SoftwareModule = _rSoftwareModule.Get(p => p.SoftwareModuleId == maintenanceOrder.SoftwareModuleId);
            }

            var maintenanceOrders = (from p in _rMaintenanceOrder.GetAll().ToList()
                                     where p.CompanyStructureId == maintenanceOrder.CompanyStructureId &&
                                        p.CodeMonthFrom <= _currentCodeMonth && _currentCodeMonth <= p.CodeMonthTo
                                     select new MaintenanceOrderHierarchyView
                                     {
                                         MaintenanceOrderId = p.MaintenanceOrderId,                                            
                                         CompanyStructureId = p.CompanyStructureId,
                                         MaintenanceId = p.Maintenance.Name,
                                         CompanyId = p.Company.Name,
                                         ComponentId = (p.Component != null ? p.Component.Code : ""),
                                         SupportLevelId = (p.SupportLevel != null ? p.SupportLevel.Name : ""),
                                         SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                         SoftwareModuleId = (p.SoftwareModule != null ? p.SoftwareModule.Name : ""),
                                         TariffId = (p.Tariff != null ? 
                                             p.Tariff.Maintenance.Name + " - " + p.Tariff.SupportLevel.Name + " - " + p.Tariff.Price.ToString() : ""),                                         
                                         CodeMonthFrom = p.CodeMonthFrom,
                                         CodeMonthTo = p.CodeMonthTo,
                                         Total = p.Total,
                                         Comment = p.Comment
                                     });

            return View(new GridModel(maintenanceOrders.ToList()));
        }

        [GridAction]
        public ActionResult _DeleteHierarchy(int id)
        {
            var maintenanceOrder = _rMaintenanceOrder.Get(p => p.MaintenanceOrderId == id);
            var companyStructureId = maintenanceOrder.CompanyStructureId;
            _currentCodeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);

            _rMaintenanceOrder.Delete(maintenanceOrder);

            var maintenanceOrders = (from p in _rMaintenanceOrder.GetAll().ToList()                                        
                                     where p.CompanyStructureId == companyStructureId &&
                                        p.CodeMonthFrom <= _currentCodeMonth && _currentCodeMonth <= p.CodeMonthTo
                                     select new MaintenanceOrderHierarchyView
                                     {
                                         MaintenanceOrderId = p.MaintenanceOrderId,                                            
                                         CompanyStructureId = p.CompanyStructureId,
                                         MaintenanceId = p.Maintenance.Name,
                                         CompanyId = p.Company.Name,
                                         ComponentId = (p.Component != null ? p.Component.Code : ""),
                                         SupportLevelId = (p.SupportLevel != null ? p.SupportLevel.Name : ""),
                                         SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                         SoftwareModuleId = (p.SoftwareModule != null ? p.SoftwareModule.Name : ""),
                                         TariffId = (p.Tariff != null ?
                                             p.Tariff.Maintenance.Name + " - " + p.Tariff.SupportLevel.Name + " - " + p.Tariff.Price.ToString() : ""),                                         
                                         CodeMonthFrom = p.CodeMonthFrom,
                                         CodeMonthTo = p.CodeMonthTo,
                                         Total = p.Total,
                                         Comment = p.Comment
                                     });

            return View(new GridModel(maintenanceOrders.ToList()));
        }        

        #endregion

        #region Table View

        public ActionResult IndexTable()
        {            
            ViewBag.Periods = new SelectList(_lCalendar.GetPeriods(), "CodeMonth", "Name");
            ViewBag.Maintenances = new SelectList(_rMaintenance.GetAll(), "MaintenanceId", "Name");
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.Components = new SelectList(_rComponent.GetAll(), "ComponentId", "Code");
            ViewBag.SupportLevels = new SelectList(_rSupportLevel.GetAll(), "SupportLevelId", "Name");
            ViewBag.SoftwareModules = new SelectList(_rSoftwareModule.GetAll(), "SoftwareModuleId", "Name");

            IEnumerable<CompanyStructureView> companyStructures =
                (from p in _rCompanyStructure.GetAll().ToList()
                 select new CompanyStructureView
                 {
                     CompanyStructureId = p.CompanyStructureId,                     
                     Name = (p.Department != null ? p.Department.Name : p.Employee.Fullname)
                 });
            ViewBag.CompanyStructures = new SelectList(companyStructures.ToList(), "CompanyStructureId", "Name");
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");            
            
            return View();
        }

        [GridAction]
        public ActionResult _IndexTable(int companyId = 0)
        {
            _currentCodeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);

            var maintenanceOrders = (from p in _rMaintenanceOrder.GetAll().ToList()
                                     where p.CompanyId == companyId &&
                                        p.CodeMonthFrom <= _currentCodeMonth && _currentCodeMonth <= p.CodeMonthTo
                                     select new MaintenanceOrderTableView
                                     {
                                         MaintenanceOrderId = p.MaintenanceOrderId,                                            
                                         CompanyStructureId =
                                             (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname),
                                         MaintenanceId = p.Maintenance.Name, 
                                         CompanyId = p.Company.Name,
                                         ComponentId = (p.Component != null ? p.Component.Code : ""),
                                         SupportLevelId = (p.SupportLevel != null ? p.SupportLevel.Name : ""),
                                         SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                         SoftwareModuleId = (p.SoftwareModule != null ? p.SoftwareModule.Name : ""),
                                         TariffId = (p.Tariff != null ?
                                             p.Tariff.Maintenance.Name + " - " + p.Tariff.SupportLevel.Name + " - " + p.Tariff.Price.ToString() : ""),                                         
                                         CodeMonthFrom = p.CodeMonthFrom,
                                         CodeMonthTo = p.CodeMonthTo,
                                         Total = p.Total,
                                         Comment = p.Comment
                                     });

            return View(new GridModel(maintenanceOrders.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdateTable(MaintenanceOrder maintenanceOrder)
        {
            _currentCodeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);

            if (ModelState.IsValid)
            {
                maintenanceOrder.MakeDate = DateTime.Now;
                if (maintenanceOrder.MaintenanceOrderId.Equals(0))
                {
                    _rMaintenanceOrder.Create(maintenanceOrder);
                }
                else
                {
                    _rMaintenanceOrder.Update(maintenanceOrder);
                }

                maintenanceOrder.Maintenance = _rMaintenance.Get(p => p.MaintenanceId == maintenanceOrder.MaintenanceId);
                maintenanceOrder.CompanyStructure = _rCompanyStructure.Get(p => p.CompanyStructureId == maintenanceOrder.CompanyStructureId);
                maintenanceOrder.Tariff = _rTariff.Get(p => p.TariffId == maintenanceOrder.TariffId);
                maintenanceOrder.Company = _rCompany.Get(p => p.CompanyId == maintenanceOrder.CompanyId);
                maintenanceOrder.Component = _rComponent.Get(p => p.ComponentId == maintenanceOrder.ComponentId);
                maintenanceOrder.Software = _rSoftware.Get(p => p.SoftwareId == maintenanceOrder.SoftwareId);
                maintenanceOrder.SupportLevel = _rSupportLevel.Get(p => p.SupportLevelId == maintenanceOrder.SupportLevelId);
                maintenanceOrder.SoftwareModule = _rSoftwareModule.Get(p => p.SoftwareModuleId == maintenanceOrder.SoftwareModuleId);
            }

            var maintenanceOrders = (from p in _rMaintenanceOrder.GetAll().ToList()
                                     where p.CodeMonthFrom <= _currentCodeMonth && _currentCodeMonth <= p.CodeMonthTo
                                     select new MaintenanceOrderTableView
                                     {
                                         MaintenanceOrderId = p.MaintenanceOrderId,                                            
                                         CompanyStructureId =
                                             (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname),
                                         MaintenanceId = p.Maintenance.Name,
                                         CompanyId = p.Company.Name,
                                         ComponentId = (p.Component != null ? p.Component.Code : ""),
                                         SupportLevelId = (p.SupportLevel != null ? p.SupportLevel.Name : ""),
                                         SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                         SoftwareModuleId = (p.SoftwareModule != null ? p.SoftwareModule.Name : ""),
                                         TariffId = (p.Tariff != null ?
                                             p.Tariff.Maintenance.Name + " - " + p.Tariff.SupportLevel.Name + " - " + p.Tariff.Price.ToString() : ""),                                         
                                         CodeMonthFrom = p.CodeMonthFrom,
                                         CodeMonthTo = p.CodeMonthTo,
                                         Total = p.Total,
                                         Comment = p.Comment
                                     });

            return View(new GridModel(maintenanceOrders.ToList()));
        }

        [GridAction]
        public ActionResult _DeleteTable(int id)
        {
            var maintenanceOrder = _rMaintenanceOrder.Get(p => p.MaintenanceOrderId == id);            
            _rMaintenanceOrder.Delete(maintenanceOrder);
            _currentCodeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);

            var maintenanceOrders = (from p in _rMaintenanceOrder.GetAll().ToList()
                                     where p.CodeMonthFrom <= _currentCodeMonth && _currentCodeMonth <= p.CodeMonthTo
                                     select new MaintenanceOrderTableView
                                     {
                                         MaintenanceOrderId = p.MaintenanceOrderId,                                            
                                         CompanyStructureId =
                                             (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname),
                                         MaintenanceId = p.Maintenance.Name,
                                         CompanyId = p.Company.Name,
                                         ComponentId = (p.Component != null ? p.Component.Code : ""),
                                         SupportLevelId = (p.SupportLevel != null ? p.SupportLevel.Name : ""),
                                         SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                         SoftwareModuleId = (p.SoftwareModule != null ? p.SoftwareModule.Name : ""),
                                         TariffId = (p.Tariff != null ?
                                             p.Tariff.Maintenance.Name + " - " + p.Tariff.SupportLevel.Name + " - " + p.Tariff.Price.ToString() : ""),                                         
                                         CodeMonthFrom = p.CodeMonthFrom,
                                         CodeMonthTo = p.CodeMonthTo,
                                         Total = p.Total,
                                         Comment = p.Comment
                                     });

            return View(new GridModel(maintenanceOrders.ToList()));
        }

        #endregion

        #endregion

    }
}
