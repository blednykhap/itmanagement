﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Views;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class CostCenterController : Controller
    {
        private readonly IBaseRepository<CostCenter> _rCostCenter;
        private readonly IBaseRepository<Company> _rCompany;

        public CostCenterController(IBaseRepository<CostCenter> rCostCenter, IBaseRepository<Company> rCompany)
        {
            _rCostCenter = rCostCenter;
            _rCompany = rCompany;
        }

        public ActionResult Index()
        {
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index(int companyId = 0)
        {
            var costCenters = (from p in _rCostCenter.GetAll().ToList()
                               where p.CompanyId == companyId
                               select new CostCenterView
                               {
                                   CostCenterId = p.CostCenterId,
                                   CompanyId = p.Company.Name,
                                   Code = p.Code,
                                   Name = p.Name,
                                   Comment = p.Comment
                               });
            return View(new GridModel(costCenters));
        }

        [GridAction]
        public ActionResult _CreateUpdate(CostCenter costCenter)
        {
            if (ModelState.IsValid)
            {
                if (costCenter.CostCenterId.Equals(0))
                {
                    _rCostCenter.Create(costCenter);
                }
                else
                {
                    _rCostCenter.Update(costCenter);
                }

                costCenter.Company = _rCompany.Get(p => p.CompanyId == costCenter.CompanyId);
            }

            var costCenters = (from p in _rCostCenter.GetAll().ToList()
                               where p.CompanyId == costCenter.CompanyId
                               select new CostCenterView
                               {
                                   CostCenterId = p.CostCenterId,
                                   CompanyId = p.Company.Name,
                                   Code = p.Code,
                                   Name = p.Name,
                                   Comment = p.Comment
                               });
            return View(new GridModel(costCenters));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var costCenter = _rCostCenter.Get(p => p.CostCenterId == id);
            var companyId = costCenter.CompanyId;
            _rCostCenter.Delete(costCenter);
            
            var costCenters = (from p in _rCostCenter.GetAll().ToList()
                               where p.CompanyId == companyId
                               select new CostCenterView
                               {
                                   CostCenterId = p.CostCenterId,
                                   CompanyId = p.Company.Name,
                                   Code = p.Code,
                                   Name = p.Name,
                                   Comment = p.Comment
                               });
            return View(new GridModel(costCenters));
        }

        #endregion

    }
}
