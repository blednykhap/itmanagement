﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Models;
using Core.Views;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class TariffController : Controller
    {
        private readonly IBaseRepository<Tariff> _rTariff;
        private readonly IBaseRepository<SupportLevel> _rSupportLevel;
        private readonly IBaseRepository<Measure> _rMeasure;
        private readonly IBaseRepository<Maintenance> _rMaintenance;
        private readonly IBaseRepository<Component> _rComponent;
        private readonly IBaseRepository<Company> _rCompany;

        public TariffController(IBaseRepository<Tariff> rTariff, IBaseRepository<SupportLevel> rSupportLevel,
            IBaseRepository<Measure> rMeasure, IBaseRepository<Maintenance> rMaintenance,
            IBaseRepository<Component> rComponent, IBaseRepository<Company> rCompany)
        {
            _rTariff = rTariff;
            _rSupportLevel = rSupportLevel;
            _rMeasure = rMeasure;
            _rMaintenance = rMaintenance;
            _rComponent = rComponent;
            _rCompany = rCompany;
        }

        [HttpPost]
        public void _ClearSession()
        {
            Session["CompanyId"] = null;
            Session["MaintenanceId"] = null;
            Session["ComponentId"] = null;
            Session["SupportLevelId"] = null;
        }

        [HttpPost]
        public JsonResult _Selection(int? companyId, int? maintenanceId, int? componentId, int? supportLevelId)
        {                         
            List<TariffShortView> tariffs = new List<TariffShortView>();

            if (companyId != null) Session["CompanyId"] = companyId;
            if (maintenanceId != null) Session["MaintenanceId"] = maintenanceId;
            if (componentId != null) Session["ComponentId"] = componentId;
            if (supportLevelId != null) Session["SupportLevelId"] = supportLevelId;

            if (Session["CompanyId"] != null) companyId = Convert.ToInt32(Session["CompanyId"]);
            if (Session["MaintenanceId"] != null) maintenanceId = Convert.ToInt32(Session["MaintenanceId"]);
            if (Session["ComponentId"] != null) componentId = Convert.ToInt32(Session["ComponentId"]);
            if (Session["SupportLevelId"] != null) supportLevelId = Convert.ToInt32(Session["SupportLevelId"]);

            if (maintenanceId != null && componentId != null && supportLevelId != null)
            {
                tariffs =
                    (from p in _rTariff.GetList(p => p.CompanyId == companyId && p.MaintenanceId == maintenanceId && 
                        p.ComponentId == componentId && p.SupportLevelId == supportLevelId).ToList()
                     select new TariffShortView
                     {
                         TariffId = p.TariffId,
                         Name = p.Maintenance.Name + " - " + p.Component.Code + " - " + p.SupportLevel.Name + " - " + p.Price.ToString()
                     }).ToList();
            }
           
            return Json(new SelectList(tariffs, "TariffId", "Name"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.SupportLevels = new SelectList(_rSupportLevel.GetAll(), "SupportLevelId", "Name"); 
            ViewBag.Measures = new SelectList(_rMeasure.GetAll(), "MeasureId", "Name");
            ViewBag.Maintenances = new SelectList(_rMaintenance.GetAll(), "MaintenanceId", "Name");
            ViewBag.Components = new SelectList(_rComponent.GetAll(), "ComponentId", "Code");

            return View();
        }

        [HttpPost]
        public ContentResult GetPrice(int id = 0)
        {
            var tariff = _rTariff.Get(p => p.TariffId == id);
            
            if (tariff != null)
            {
                return Content(tariff.Price.ToString());
            }
            else
            {
                return Content("0");
            }            
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index(int companyId = 0, int maintenanceId = 0)
        {
            var tariffs = (from p in _rTariff.GetAll().ToList()
                           where p.CompanyId == companyId && (maintenanceId == 0 ? true : p.MaintenanceId == maintenanceId)
                           select new TariffView { 
                               TariffId = p.TariffId,
                               CompanyId = p.Company.Name,
                               ComponentId = p.Component.Code,
                               SupportLevelId = p.SupportLevel.Name,
                               MeasureId = p.Measure.Name,
                               MaintenanceId = p.Maintenance.Name,
                               Price = p.Price
                           });

            return View(new GridModel(tariffs.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Tariff tariff)
        {
            if (ModelState.IsValid)
            {
                if (tariff.TariffId.Equals(0))
                {
                    _rTariff.Create(tariff);
                }
                else
                {
                    _rTariff.Update(tariff);
                }

                tariff.Company = _rCompany.Get(p => p.CompanyId == tariff.CompanyId);
                tariff.Component = _rComponent.Get(p => p.ComponentId == tariff.ComponentId);
                tariff.SupportLevel = _rSupportLevel.Get(p => p.SupportLevelId == tariff.SupportLevelId);
                tariff.Measure = _rMeasure.Get(p => p.MeasureId == tariff.MeasureId);
                tariff.Maintenance = _rMaintenance.Get(p => p.MaintenanceId == tariff.MaintenanceId);
            }

            var tariffs = (from p in _rTariff.GetAll().ToList()
                           where p.CompanyId == tariff.CompanyId
                           select new TariffView
                           {
                               TariffId = p.TariffId,
                               CompanyId = p.Company.Name,
                               ComponentId = p.Component.Code,
                               SupportLevelId = p.SupportLevel.Name,
                               MeasureId = p.Measure.Name,
                               MaintenanceId = p.Maintenance.Name,
                               Price = p.Price
                           });

            return View(new GridModel(tariffs.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var tariff = _rTariff.Get(p => p.TariffId == id);
            var companyId = tariff.CompanyId;
            _rTariff.Delete(tariff);

            var tariffs = (from p in _rTariff.GetAll().ToList()
                           where p.CompanyId == companyId
                           select new TariffView
                           {
                               TariffId = p.TariffId,
                               CompanyId = p.Company.Name,
                               ComponentId = p.Component.Code,
                               SupportLevelId = p.SupportLevel.Name,
                               MeasureId = p.Measure.Name,
                               MaintenanceId = p.Maintenance.Name,
                               Price = p.Price
                           });

            return View(new GridModel(tariffs.ToList()));
        }

        #endregion

    }
}
