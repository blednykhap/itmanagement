﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Others;
using Core.Filters;
using Core.Models;
using Core.Views;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class EmployeeProfileController : Controller
    {
        private readonly IBaseRepository<EmployeeProfile> _rEmployeeProfile;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly ICalendarLogic _lCalendar; 

        public EmployeeProfileController(IBaseRepository<EmployeeProfile> rEmployeeProfile, IBaseRepository<Company> rCompany,
            ICalendarLogic lCalendar)
        {
            _rEmployeeProfile = rEmployeeProfile;
            _rCompany = rCompany;
            _lCalendar = lCalendar;
        }

        public ActionResult Edit()
        {
            int employeeId = int.Parse(Session["EmployeeId"].ToString());
            int currentCodeMonth = int.Parse(Session["CurrentCodeMonth"].ToString());
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.Periods = new SelectList(_lCalendar.GetPeriods(), "CodeMonth", "Name", currentCodeMonth);

            EmployeeProfile employeeProfile = _rEmployeeProfile.GetList(p => p.EmployeeId == employeeId).FirstOrDefault();
            
            if (employeeProfile == null)
            {
                employeeProfile = new EmployeeProfile();
                employeeProfile.EmployeeId = employeeId;
            }
            
            return View(employeeProfile);
        }

        [HttpPost]
        public ActionResult Edit(EmployeeProfile employeeProfile)
        {
            if (ModelState.IsValid)
            {
                if (employeeProfile.EmployeeProfileId.Equals(0))
                {
                    _rEmployeeProfile.Create(employeeProfile);
                }
                else
                {
                    _rEmployeeProfile.Update(employeeProfile);
                }

                Session["PreferCompanyId"] = employeeProfile.CompanyId;
                Session["CurrentCodeMonth"] = employeeProfile.CodeMonth;
                Session["IsDisplayActiveOnly"] = employeeProfile.IsDisplayActiveOnly;
            }

            return RedirectToAction("Edit");
        }
    }
}
