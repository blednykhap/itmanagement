﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Microsoft.Win32;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class LicenseFileController : Controller
    {
        private readonly IBaseRepository<LicenseFile> _rLicenseFile;

        public LicenseFileController(IBaseRepository<LicenseFile> rLicenseFile)
        {
            _rLicenseFile = rLicenseFile;
        }

        [GridAction]
        public ActionResult _Index(int licenseId)
        {
            var licenseFiles = (from p in _rLicenseFile.GetList(p => p.LicenseId == licenseId).ToList()
                                select new LicenseFileView
                                {
                                    LicenseId = p.LicenseId,
                                    LicenseFileId = p.LicenseFileId,
                                    FileName = p.FileName,
                                    FileType = p.FileType
                                });

            return View(new GridModel(licenseFiles));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var licenseFile = _rLicenseFile.Get(p => p.LicenseFileId == id);
            var licenseId = licenseFile.LicenseId;
            _rLicenseFile.Delete(licenseFile);

            var licenseFiles = (from p in _rLicenseFile.GetList(p => p.LicenseId == licenseId).ToList()
                                select new LicenseFileView
                                {
                                    LicenseId = p.LicenseId,
                                    LicenseFileId = p.LicenseFileId,
                                    FileName = p.FileName,
                                    FileType = p.FileType
                                });

            return View(new GridModel(licenseFiles));
        }

        public void UploadFile(HttpPostedFileBase attachments, int licenseId = 0)
        {            
            var licenseFile = new LicenseFile();

            if (licenseId != 0 && attachments != null)
            {
                var br = new BinaryReader(attachments.InputStream);
                
                licenseFile.LicenseId = licenseId;
                licenseFile.FileContent = br.ReadBytes(attachments.ContentLength);
                licenseFile.FileName = Path.GetFileName(attachments.FileName);
                licenseFile.FileType = attachments.ContentType;
                _rLicenseFile.Create(licenseFile);
            }
        }

        public FileContentResult GetFile(int licenseFileId)
        {            
            var licenseFile = _rLicenseFile.Get(p => p.LicenseFileId == licenseFileId);

            if (licenseFile != null && licenseFile.FileContent != null)
            {
                return File(licenseFile.FileContent, licenseFile.FileType, licenseFile.FileName);
            }
            else
            {
                return null;
            }
        }

        public void UploadFiles(HttpPostedFileBase attachments)
        {
            if (attachments != null)
            {
                string[] filepaths = Directory.GetFiles(Path.GetDirectoryName(attachments.FileName));
                LicenseFile licenseFile;

                foreach (var filepath in filepaths)
                {
                    var filename = Path.GetFileName(filepath);
                    licenseFile = _rLicenseFile.GetList(p => p.FileName.Trim().ToUpper() == filename.Trim().ToUpper()).FirstOrDefault();
                    if (licenseFile != null)
                    {                        
                        var fileMime = System.Web.MimeMapping.GetMimeMapping(filename);

                        using (FileStream fs = new FileStream(filepath, FileMode.Open))
                        {
                            byte[] bytes = new byte[fs.Length];
                            fs.Read(bytes, 0, (int)fs.Length);                        

                            licenseFile.FileContent = bytes;
                            licenseFile.FileType = fileMime;

                            _rLicenseFile.Update(licenseFile);
                        }
                    }
                }
            }
        }

    }
}
