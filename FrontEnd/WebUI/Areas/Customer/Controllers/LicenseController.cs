﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Telerik.Web.Mvc;
using Core.Views;
using System.IO;
using Core.Filters;

// TODO \\aisuu.corp.tnk-bp.ru\www\aisuu\htdocs\SUU\files
namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class LicenseController : Controller
    {
        private readonly IBaseRepository<License> _rLicense;
        private readonly IBaseRepository<Contractor> _rContractor;
        private readonly IBaseRepository<Software> _rSoftware;

        public LicenseController(IBaseRepository<License> rLicense, IBaseRepository<Contractor> rContractor,
            IBaseRepository<Software> rSoftware)
        {
            _rLicense = rLicense;
            _rContractor = rContractor;
            _rSoftware = rSoftware;
        }

        public ActionResult Index()
        {
            ViewBag.Contractors = new SelectList(_rContractor.GetAll(), "ContractorId", "Name");
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var licenses = (from p in _rLicense.GetAll().ToList()
                            select new LicenseView
                            {
                                LicenseId = p.LicenseId,
                                Code = p.Code,
                                DateTo = p.DateTo,
                                Name = p.Name,
                                ContractorId = (p.Contractor != null ? p.Contractor.Name : ""),
                                SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                Amount = p.Amount,
                                ContractNumber = p.ContractNumber,
                                ContractDate = p.ContractDate
                            });

            return View(new GridModel(licenses.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(License license)
        {
            if (ModelState.IsValid)
            {
                if (license.LicenseId.Equals(0))
                {
                    _rLicense.Create(license);
                }
                else
                {
                    _rLicense.Update(license);
                }

                license.Contractor = _rContractor.Get(p => p.ContractorId == license.ContractorId);
                license.Software = _rSoftware.Get(p => p.SoftwareId == license.SoftwareId);
            }

            var licenses = (from p in _rLicense.GetAll().ToList()
                            select new LicenseView
                            {
                                LicenseId = p.LicenseId,
                                Code = p.Code,
                                DateTo = p.DateTo,
                                Name = p.Name,
                                ContractorId = (p.Contractor != null ? p.Contractor.Name : ""),
                                SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                Amount = p.Amount,
                                ContractNumber = p.ContractNumber,
                                ContractDate = p.ContractDate
                            });

            return View(new GridModel(licenses.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var license = _rLicense.Get(p => p.LicenseId == id);
            _rLicense.Delete(license);

            var licenses = (from p in _rLicense.GetAll().ToList()
                            select new LicenseView
                            {
                                LicenseId = p.LicenseId,
                                Code = p.Code,
                                DateTo = p.DateTo,
                                Name = p.Name,
                                ContractorId = (p.Contractor != null ? p.Contractor.Name : ""),
                                SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                Amount = p.Amount,
                                ContractNumber = p.ContractNumber,
                                ContractDate = p.ContractDate
                            });

            return View(new GridModel(licenses.ToList()));
        }

        #endregion

    }
}
