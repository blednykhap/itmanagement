﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class PositionController : Controller
    {
        private readonly IBaseRepository<Position> _rPosition;

        public PositionController(IBaseRepository<Position> rPosition)
        {
            _rPosition = rPosition;
        }

        public ActionResult Index()
        {
            var positions = _rPosition.GetAll();
            return View(positions.ToList());
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var positions = _rPosition.GetAll();
            return View(new GridModel(positions.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Position position)
        {
            if (ModelState.IsValid)
            {
                if (position.PositionId.Equals(0))
                {
                    _rPosition.Create(position);
                }
                else
                {
                    _rPosition.Update(position);
                }
            }

            var positions = _rPosition.GetAll();
            return View(new GridModel(positions.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var position = _rPosition.Get(p => p.PositionId == id);
            _rPosition.Delete(position);

            var positions = _rPosition.GetAll();
            return View(new GridModel(positions.ToList()));
        }

        #endregion

    }
}
