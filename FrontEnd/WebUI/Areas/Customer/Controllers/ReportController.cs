﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Reports;
using Core.Filters;
using Telerik.Web.Mvc;
using Core.Models;
using Core.Reports;
using Microsoft.Reporting.WebForms;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class ReportController : Controller
    {
        private readonly IReportRepository _rReport;
        private readonly IBaseRepository<MaintenanceOrder> _rMaintenanceOrder;
        private readonly IExcelForm _fExcel;
        private Department ListDepartments = new Department(); 

        public ReportController(IReportRepository rReport, IBaseRepository<MaintenanceOrder> rMaintenanceOrder,
            IExcelForm fExcel)
        {
            _rReport = rReport;
            _rMaintenanceOrder = rMaintenanceOrder;
            _fExcel = fExcel;
        }

        #region InstalledSoftware

        public ActionResult InstalledSoftware()
        {            
            return View();
        }

        [GridAction]
        public ActionResult _InstalledSoftware()
        {
            var data = _rReport.InstalledSoftwareReport();

            return View(new GridModel(data));
        }

        #endregion

        #region UsedLicenseByUser

        public ActionResult UsedLicenseByUser(int softwareId)
        {
            ViewBag.SoftwareId = softwareId;
            return View();
        }

        [GridAction]
        public ActionResult _UsedLicenseByUser(int softwareId)
        {
            var data = _rReport.InstalledSoftwareReport(softwareId);

            return View(new GridModel(data));
        }

        #endregion

        #region UsedLicense

        public ActionResult UsedLicense()
        {
            return View();
        }

        [GridAction]
        public ActionResult _UsedLicense()
        {
            var data = _rReport.UsedLicenseReport();

            return View(new GridModel(data));
        }

        #endregion

        #region MaintenanceCrossReport

        public ActionResult MaintenanceCross()
        {
            return View();
        }

        public ActionResult _MaintenanceCross(int companyId = 0, int codeMonth = 0)
        {
            var x = _rMaintenanceOrder.GetList(p => p.CodeMonthFrom <= codeMonth && codeMonth <= p.CodeMonthTo && p.CompanyId == companyId).ToList() 
                        .Select(p => p.CompanyStructure).Distinct().Count() + 1;

            var y = _rMaintenanceOrder.GetList(p => p.CodeMonthFrom <= codeMonth && codeMonth <= p.CodeMonthTo && p.CompanyId == companyId).ToList()
                        .Select(p => p.Maintenance).Distinct().Count() + 3;
            
            string[,] array = 
                new string[x, y];
            int i = 1;
            int j = 0;

            foreach (var companyStructure in _rMaintenanceOrder.GetList(p => p.CodeMonthFrom <= codeMonth && codeMonth <= p.CodeMonthTo && p.CompanyId == companyId).ToList()
                .Select(p => p.CompanyStructure).Distinct().OrderBy(p => p.EmployeeId))
            {
                j = 3;
                array[i, 0] = (companyStructure.Employee != null ? companyStructure.Employee.Authid : "");
                array[i, 1] = (companyStructure.Employee != null ? companyStructure.Employee.Fullname : companyStructure.Department.Name);
                array[i, 2] = (companyStructure.Employee != null ? 
                                    (companyStructure.Employee.CostCenter != null ? companyStructure.Employee.CostCenter.Code : "") : 
                                    (companyStructure.Department.CostCenter != null ? companyStructure.Department.CostCenter.Code : ""));
                foreach (var maintenance in _rMaintenanceOrder.GetList(p => p.CodeMonthFrom <= codeMonth && codeMonth <= p.CodeMonthTo && p.CompanyId == companyId).ToList()
                    .Select(p => p.Maintenance).Distinct().OrderBy(p => p.MaintenanceId))
                {
                    array[0, j] = maintenance.Name;
                    if (_rMaintenanceOrder.GetList(p => p.CodeMonthFrom <= codeMonth && codeMonth <= p.CodeMonthTo && p.CompanyId == companyId &&
                        p.CompanyStructureId == companyStructure.CompanyStructureId && p.MaintenanceId == maintenance.MaintenanceId).Any())
                    {
                        array[i, j] = "1";
                    }
                    else
                    {
                        array[i, j] = "";
                    }                    
                    j++;
                }
                i++;
            }

            var result = _fExcel.MaintenanceCross(array, x, y);

            return File(result, "application/vnd.ms-excel", "MaintenanceCross.xls");
        }

        #endregion

        #region R24HoursMaintenance

        public ActionResult R24HoursMaintenance()
        {
            return View();
        }

        [GridAction]
        public ActionResult _R24HoursMaintenance()
        {
            var period = Convert.ToInt32(Session["CurrentCodeMonth"]);
            var data = _rReport.R24HoursMaintenanceReport(period);

            return View(new GridModel(data));
        }

        #endregion

        #region Invoice
        public ActionResult Invoice()
        {

            return View();
        }


        public ActionResult _Invoice(int codeMonth, int companyId, int subdivisionId, string ifSec)
        {
            var userId = Convert.ToInt32(Session["EmployeeId"]);
            var data = _rReport.InvoiceReport(userId, codeMonth, companyId,subdivisionId, ifSec);

            /*         var result = _fExcel.InvoiceExcel(data);

                     return File(result, "application/vnd.ms-excel", "Invoice.xls");*/
            ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();
            rv.ProcessingMode = ProcessingMode.Local;

            rv.LocalReport.ReportPath = Server.MapPath("~/Templates/ReportInvoice.rdlc");

            ReportParameter[] reportParameter = new ReportParameter[3];
            reportParameter[0] = new ReportParameter("PeriodReport", codeMonth.ToString());
            reportParameter[1] = new ReportParameter("UserReport", userId.ToString());
            reportParameter[2] = new ReportParameter("SubdivisionReport", subdivisionId.ToString());

            rv.LocalReport.SetParameters(reportParameter);

            rv.LocalReport.Refresh();

            rv.LocalReport.DataSources.Clear();
            rv.LocalReport.DataSources.Add(new ReportDataSource("DataSetInvoice", data));

            string reportType = "excel";
            string mimeType;
            string encoding;
            //string deviceInfo = "deviceInfo";
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            //Render the report            
            renderedBytes = rv.LocalReport.Render(reportType, null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            return File(renderedBytes, "application/vnd.ms-excel", "Invoice.xls");

        }

        #endregion

        #region ChessReport

        public ActionResult Chess()
        {
            return View();
        }


        public ActionResult _Chess(int codeMonth, int companyId, int subdivisionId)
        {
            var data = _rReport.ChessReport(codeMonth, companyId, subdivisionId);

            ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();
            rv.ProcessingMode = ProcessingMode.Local;

            rv.LocalReport.ReportPath = Server.MapPath("~/Templates/ReportChess.rdlc");

            ReportParameter[] reportParameter = new ReportParameter[2];
            reportParameter[0] = new ReportParameter("PeriodReport", codeMonth.ToString());
            reportParameter[1] = new ReportParameter("SubdivisionReport", subdivisionId.ToString());

            rv.LocalReport.SetParameters(reportParameter);

            rv.LocalReport.Refresh();

            rv.LocalReport.DataSources.Clear();
            rv.LocalReport.DataSources.Add(new ReportDataSource("DataSetChessReport", data));

            string reportType = "excel";
            string mimeType;
            string encoding;
            //string deviceInfo = "deviceInfo";
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            //Render the report            
            renderedBytes = rv.LocalReport.Render(reportType, null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            DateTime baseDate = new DateTime(1, 1, 1); 
            DateTime newDate = baseDate.AddDays(codeMonth);
            //newDate = DateTime.Now.AddDays(0);
            return File(renderedBytes, "application/vnd.ms-excel", "Chess.xls");
            
        }

        #endregion

    }
}
