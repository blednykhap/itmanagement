﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    public class SoftwareModuleController : Controller
    {
        private readonly IBaseRepository<SoftwareModule> _rSoftwareModule;
        private readonly IBaseRepository<Software> _rSoftware;

        public SoftwareModuleController(IBaseRepository<SoftwareModule> rSoftwareModule,
            IBaseRepository<Software> rSoftware)
        {
            _rSoftwareModule = rSoftwareModule;
            _rSoftware = rSoftware;
        }

        public ActionResult Index()
        {
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var softwareModules = (from p in _rSoftwareModule.GetAll().ToList()
                                   select new SoftwareModuleView
                                   {
                                       SoftwareModuleId = p.SoftwareModuleId,
                                       Code = p.Code,
                                       Name = p.Name,
                                       SoftwareId = (p.Software != null ? p.Software.Name : "")
                                   });

            return View(new GridModel(softwareModules));
        }

        [GridAction]
        public ActionResult _CreateUpdate(SoftwareModule softwareModule)
        {
            if (ModelState.IsValid)
            {
                if (softwareModule.SoftwareModuleId.Equals(0))
                {
                    _rSoftwareModule.Create(softwareModule);
                }
                else
                {
                    _rSoftwareModule.Update(softwareModule);
                }
                softwareModule.Software = _rSoftware.Get(p => p.SoftwareId == softwareModule.SoftwareId);
            }

            var softwareModules = (from p in _rSoftwareModule.GetAll().ToList()
                                   select new SoftwareModuleView
                                   {
                                       SoftwareModuleId = p.SoftwareModuleId,
                                       Code = p.Code,
                                       Name = p.Name,
                                       SoftwareId = (p.Software != null ? p.Software.Name : "")
                                   });

            return View(new GridModel(softwareModules));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {            
            var softwareModule = _rSoftwareModule.Get(p => p.SoftwareModuleId == id);
            _rSoftwareModule.Delete(softwareModule);

            var softwareModules = (from p in _rSoftwareModule.GetAll().ToList()
                                   select new SoftwareModuleView
                                   {
                                       SoftwareModuleId = p.SoftwareModuleId,
                                       Code = p.Code,
                                       Name = p.Name,
                                       SoftwareId = (p.Software != null ? p.Software.Name : "")
                                   });

            return View(new GridModel(softwareModules));
        }

        #endregion

    }
}
