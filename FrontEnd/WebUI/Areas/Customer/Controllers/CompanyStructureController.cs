﻿using System.Collections;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc.UI;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class CompanyStructureController : Controller
    {
        private readonly IBaseRepository<CompanyStructure> _rCompanyStructure;

        public CompanyStructureController(IBaseRepository<CompanyStructure> rCompanyStructure)
        {
            _rCompanyStructure = rCompanyStructure;
        }

        [HttpPost]        
        public ActionResult _Index(TreeViewItem node, int companyId = 0)
        {
            //int? parentId = !string.IsNullOrEmpty(node.Value) ? (int?)Convert.ToInt32(node.Value) : 1;
            int? parentId = !string.IsNullOrEmpty(node.Value) ? (int?)Convert.ToInt32(node.Value) : 6;
            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            IEnumerable nodes = (from p in _rCompanyStructure.GetAll().ToList()
                                 where (p.ParentCompanyStructureId == parentId || (parentId == null && p.ParentCompanyStructureId == null))
                                    && p.CompanyId == companyId && (isDisplayActiveOnly ? p.IsActive == true : true)
                                 select new TreeViewItemModel
                                 {
                                     Text = (p.Department != null ? p.Department.Name : p.Employee.Fullname),
                                     Value = p.CompanyStructureId.ToString(),
                                     LoadOnDemand = _rCompanyStructure.GetList(t => t.ParentCompanyStructureId == p.CompanyStructureId).Any(),
                                     Enabled = true,
                                     ImageUrl = (p.Department != null ? "/Images/Others/Department.png" : "/Images/Others/Employee.png")                                     
                                 });
            return new JsonResult { Data = nodes };
        }

    }
}
