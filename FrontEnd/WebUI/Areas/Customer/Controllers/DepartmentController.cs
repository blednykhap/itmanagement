﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc;
using Core.Views;
using Telerik.Web.Mvc.UI;
using System.Collections;
using Core.Filters;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class DepartmentController : Controller
    {
        private readonly IBaseRepository<Department> _rDepartment;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<CostCenter> _rCostCenter;

        public DepartmentController(IBaseRepository<Department> rDepartment, IBaseRepository<Company> rCompany,
            IBaseRepository<CostCenter> rCostCenter)
        {
            _rDepartment = rDepartment;
            _rCompany = rCompany;
            _rCostCenter = rCostCenter;
        }

        public ActionResult IndexTable()
        {
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.ParentDepartments = new SelectList(_rDepartment.GetList(p => p.IsActive == true), "DepartmentId", "Name");
            ViewBag.CostCenters = new SelectList(_rCostCenter.GetAll(), "CostCenterId", "Name");

            return View();
        }

        public ActionResult IndexHierarchy()
        {            
            return View();
        }

        #region CRUD

        [HttpPost]
        public ActionResult _IndexHierarchy(TreeViewItem node, int companyId = 0)
        {
            //int? parentId = !string.IsNullOrEmpty(node.Value) ? (int?)Convert.ToInt32(node.Value) : 1;         
            int? parentId = !string.IsNullOrEmpty(node.Value) ? (int?)Convert.ToInt32(node.Value) : 4958;
            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            IEnumerable nodes = (from p in _rDepartment.GetAll().ToList()
                                 where (p.ParentDepartmentId == parentId || (parentId == null && p.ParentDepartmentId == null))
                                    && p.CompanyId == companyId && (isDisplayActiveOnly ? p.IsActive == true : true)
                                 select new TreeViewItemModel { 
                                     Text = p.Name + (p.CostCenter != null ? " (МВЗ:" + p.CostCenter.Code + ")" : "" ),
                                     Value = p.DepartmentId.ToString(),                                     
                                     LoadOnDemand = _rDepartment.GetList(t => p.IsActive == true && t.ParentDepartmentId == p.DepartmentId).Any(),
                                     Enabled = true,
                                     ImageUrl = "/Images/Others/Department.png"                                     
                                 });
            return new JsonResult { Data = nodes };
        }

        [GridAction]
        public ActionResult _Index()
        {
            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            var departments = (from p in _rDepartment.GetAll().ToList()
                               where (isDisplayActiveOnly ? p.IsActive == true : true)
                               select new DepartmentView()
                               {
                                   DepartmentId = p.DepartmentId,
                                   Name = p.Name,
                                   CompanyId = p.Company.Name,
                                   ParentDepartmentId = p.ParentDepartment != null ? p.ParentDepartment.Name : "",
                                   CostCenterId = p.CostCenter.Name
                               });

            return View(new GridModel(departments.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Department department)
        {
            if (ModelState.IsValid)
            {
                if (department.DepartmentId.Equals(0))
                {
                    _rDepartment.Create(department);
                }
                else
                {
                    _rDepartment.Update(department);
                }

                department.Company = _rCompany.Get(p => p.CompanyId == department.CompanyId);
                department.ParentDepartment = _rDepartment.Get(p => p.DepartmentId == department.ParentDepartmentId);
                department.CostCenter = _rCostCenter.Get(p => p.CostCenterId == department.CostCenterId);
            }

            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            var departments = (from p in _rDepartment.GetAll().ToList()
                               where (isDisplayActiveOnly ? p.IsActive == true : true)
                               select new DepartmentView()
                               {
                                   DepartmentId = p.DepartmentId,
                                   Name = p.Name,
                                   CompanyId = p.Company.Name,
                                   ParentDepartmentId = p.ParentDepartment != null ? p.ParentDepartment.Name : "",
                                   CostCenterId = p.CostCenter.Name
                               });

            return View(new GridModel(departments.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var department = _rDepartment.Get(p => p.DepartmentId == id);
            _rDepartment.Delete(department);
            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);

            var departments = (from p in _rDepartment.GetAll().ToList()
                               where (isDisplayActiveOnly ? p.IsActive == true : true)
                               select new DepartmentView()
                               {
                                   DepartmentId = p.DepartmentId,
                                   Name = p.Name,
                                   CompanyId = p.Company.Name,
                                   ParentDepartmentId = p.ParentDepartment != null ? p.ParentDepartment.Name : "",
                                   CostCenterId = p.CostCenter.Name
                               });

            return View(new GridModel(departments.ToList()));
        }

        #endregion

    }
}
