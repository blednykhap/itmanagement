﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class ComponentController : Controller
    {
        private readonly IBaseRepository<Component> _rComponent;
        private readonly IBaseRepository<Measure> _rMeasure;

        public ComponentController(IBaseRepository<Component> rComponent, IBaseRepository<Measure> rMeasure)
        {
            _rComponent = rComponent;
            _rMeasure = rMeasure;
        }

        public ActionResult Index()
        {            
            return View();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var components = _rComponent.GetAll();

            return View(new GridModel(components.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(Component component)
        {
            if (ModelState.IsValid)
            {
                if (component.ComponentId.Equals(0))
                {
                    _rComponent.Create(component);
                }
                else
                {
                    _rComponent.Update(component);
                }                
            }

            var components = _rComponent.GetAll();

            return View(new GridModel(components.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var component = _rComponent.Get(p => p.ComponentId == id);
            _rComponent.Delete(component);

            var components = _rComponent.GetAll();

            return View(new GridModel(components.ToList()));
        }

        #endregion

    }
}
