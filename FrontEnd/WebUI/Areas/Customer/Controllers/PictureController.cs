﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class PictureController : Controller
    {
        private readonly IBaseRepository<Picture> _rPicture;

        public PictureController(IBaseRepository<Picture> rPicture)
        {
            _rPicture = rPicture;
        }
      
        public ActionResult SlideShow()
        {
            var pictures = _rPicture.GetAll();

            return PartialView(pictures.ToList());
        }

        [OutputCache(Duration = 600, Location = OutputCacheLocation.Server, VaryByParam = "pictureId")]
        public FileContentResult GetImage(int pictureId)
        {
            Picture photo = _rPicture.Get(p => p.PictureId == pictureId);
            return File(photo.FileContent, photo.FileType);
        }
    }
}
