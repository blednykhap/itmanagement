﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class LogAppEventController : Controller
    {
        private readonly IBaseRepository<LogAppEvent> _rLogAppEvent;

        public LogAppEventController(IBaseRepository<LogAppEvent> rLogAppEvent)
        {
            _rLogAppEvent = rLogAppEvent;
        }

      
        //[ChildActionOnly]
        public ActionResult _Index(int currentPage = 1)
        {
            int itemsPerSheet = 5;

            var logAppEvents = new LogAppEventView()
            {
                PagingInfo = new PagingInfo()
                {
                    CurrentPage = currentPage,
                    //TotalItems = _rLogAppEvent.GetAll().Count(),
                    TotalItems = _rLogAppEvent.GetList(p => p.LogDataChanges.Count() != 0).Count(),
                    ItemsPerSheet = itemsPerSheet
                },
                LogAppEvents = _rLogAppEvent.GetList(p=>p.LogDataChanges.Count()!=0).ToList()
                  .OrderByDescending(p => p.LogAppEventId).Skip((currentPage - 1) * itemsPerSheet).Take(itemsPerSheet)
              /*  LogAppEvents = _rLogAppEvent.GetAll().ToList()
                    .OrderByDescending(p => p.LogAppEventId).Skip((currentPage - 1) * itemsPerSheet).Take(itemsPerSheet) */                   
            };

            return PartialView(logAppEvents);
        }

        public ActionResult Details(int logAppEventId)
        {
            var logAppEvent = _rLogAppEvent.Get(p => p.LogAppEventId == logAppEventId);
            return View(logAppEvent);
        }

    }
}
