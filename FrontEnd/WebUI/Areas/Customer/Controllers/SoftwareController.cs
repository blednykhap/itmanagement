﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Areas.Customer.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class SoftwareController : Controller
    {
        private readonly IBaseRepository<Software> _rSoftware;

        public SoftwareController(IBaseRepository<Software> rSoftware)
        {
            _rSoftware = rSoftware;
        }

        public ActionResult Index()
        {
            return View();
        }

        [GridAction]
        public ActionResult _Index()
        {
            var softwares = (from p in _rSoftware.GetAll()
                             select new SoftwareView
                             {
                                 SoftwareId = p.SoftwareId,
                                 Name = p.Name.ToString(System.Globalization.CultureInfo.InvariantCulture),
                                 Comment = p.Comment
                             });

            return View(new GridModel(softwares.ToList()));
        }

    }
}
