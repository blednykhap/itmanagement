[assembly: WebActivator.PreApplicationStartMethod(typeof(WebUI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(WebUI.App_Start.NinjectWebCommon), "Stop")]

namespace WebUI.App_Start
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Mvc.FilterBindingSyntax;

    using Core.Models;
    using Core.Providers;
    using Core.Filters;
    using Business.Reports;
    using Business.ServerSide;
    using Business.Others;
    
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IBaseRepository<Employee>>().To<BaseRepository<Employee>>().InRequestScope();
            kernel.Bind<IBaseRepository<CostCenter>>().To<BaseRepository<CostCenter>>().InRequestScope();
            kernel.Bind<IBaseRepository<Position>>().To<BaseRepository<Position>>().InRequestScope();
            kernel.Bind<IBaseRepository<Department>>().To<BaseRepository<Department>>().InRequestScope();            
            kernel.Bind<IBaseRepository<Maintenance>>().To<BaseRepository<Maintenance>>().InRequestScope();
            kernel.Bind<IBaseRepository<SupportLevel>>().To<BaseRepository<SupportLevel>>().InRequestScope();
            kernel.Bind<IBaseRepository<SystemParameter>>().To<BaseRepository<SystemParameter>>().InRequestScope();
            kernel.Bind<IBaseRepository<Measure>>().To<BaseRepository<Measure>>().InRequestScope();
            kernel.Bind<IBaseRepository<Component>>().To<BaseRepository<Component>>().InRequestScope();
            kernel.Bind<IBaseRepository<Company>>().To<BaseRepository<Company>>().InRequestScope();
            kernel.Bind<IBaseRepository<ScheduledTask>>().To<BaseRepository<ScheduledTask>>().InRequestScope();
            kernel.Bind<IBaseRepository<ExecutedTask>>().To<BaseRepository<ExecutedTask>>().InRequestScope();
            kernel.Bind<IBaseRepository<ServiceLine>>().To<BaseRepository<ServiceLine>>().InRequestScope();
            kernel.Bind<IBaseRepository<LogAppEvent>>().To<BaseRepository<LogAppEvent>>().InRequestScope();
            kernel.Bind<IBaseRepository<LogDataChange>>().To<BaseRepository<LogDataChange>>().InRequestScope();
            kernel.Bind<IBaseRepository<SecArea>>().To<BaseRepository<SecArea>>().InRequestScope();
            kernel.Bind<IBaseRepository<SecController>>().To<BaseRepository<SecController>>().InRequestScope();
            kernel.Bind<IBaseRepository<SecAction>>().To<BaseRepository<SecAction>>().InRequestScope();
            kernel.Bind<IBaseRepository<LogUserActivity>>().To<BaseRepository<LogUserActivity>>().InRequestScope();            
            //kernel.Bind<IBaseRepository<EmployeeMaintenance>>().To<BaseRepository<EmployeeMaintenance>>().InRequestScope();
            kernel.Bind<IBaseRepository<CompanyMaintenance>>().To<BaseRepository<CompanyMaintenance>>().InRequestScope();            
            kernel.Bind<IBaseRepository<Tariff>>().To<BaseRepository<Tariff>>().InRequestScope();
            kernel.Bind<IBaseRepository<EmployeeProfile>>().To<BaseRepository<EmployeeProfile>>().InRequestScope();
            kernel.Bind<IBaseRepository<Picture>>().To<BaseRepository<Picture>>().InRequestScope();
            kernel.Bind<IBaseRepository<HelpArticle>>().To<BaseRepository<HelpArticle>>().InRequestScope();
            kernel.Bind<IBaseRepository<Software>>().To<BaseRepository<Software>>().InRequestScope();
            kernel.Bind<IBaseRepository<Contractor>>().To<BaseRepository<Contractor>>().InRequestScope();            
            kernel.Bind<IBaseRepository<Role>>().To<BaseRepository<Role>>().InRequestScope();
            kernel.Bind<IBaseRepository<License>>().To<BaseRepository<License>>().InRequestScope();
            kernel.Bind<IBaseRepository<SoftwareType>>().To<BaseRepository<SoftwareType>>().InRequestScope();
            //kernel.Bind<IBaseRepository<ContractorSoftwareLicense>>().To<BaseRepository<ContractorSoftwareLicense>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawSystem>>().To<BaseRepository<RawSystem>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawSystemMAC>>().To<BaseRepository<RawSystemMAC>>().InRequestScope();
            //kernel.Bind<IBaseRepository<RawSystemIP>>().To<BaseRepository<RawSystemIP>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawInstalledSoftware>>().To<BaseRepository<RawInstalledSoftware>>().InRequestScope();
            kernel.Bind<IBaseRepository<ComputerSystem>>().To<BaseRepository<ComputerSystem>>().InRequestScope();
            kernel.Bind<IBaseRepository<InstalledSoftware>>().To<BaseRepository<InstalledSoftware>>().InRequestScope();
            kernel.Bind<IBaseRepository<ContractorSoftware>>().To<BaseRepository<ContractorSoftware>>().InRequestScope();
            //kernel.Bind<IBaseRepository<AppRole>>().To<BaseRepository<AppRole>>().InRequestScope();
            kernel.Bind<IBaseRepository<LicenseFile>>().To<BaseRepository<LicenseFile>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawSAPMaintenance>>().To<BaseRepository<RawSAPMaintenance>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawCostCenter>>().To<BaseRepository<RawCostCenter>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawEmployee>>().To<BaseRepository<RawEmployee>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawMaintenanceByUser>>().To<BaseRepository<RawMaintenanceByUser>>().InRequestScope();
            kernel.Bind<IBaseRepository<CompanyStructure>>().To<BaseRepository<CompanyStructure>>().InRequestScope();
            kernel.Bind<IBaseRepository<MaintenanceOrder>>().To<BaseRepository<MaintenanceOrder>>().InRequestScope();
            kernel.Bind<IBaseRepository<RawADMaintenance>>().To<BaseRepository<RawADMaintenance>>().InRequestScope();
            kernel.Bind<IBaseRepository<LogError>>().To<BaseRepository<LogError>>().InRequestScope();
            kernel.Bind<IBaseRepository<SoftwareModule>>().To<BaseRepository<SoftwareModule>>().InRequestScope();
            kernel.Bind<IBaseRepository<Subdivision>>().To<BaseRepository<Subdivision>>().InRequestScope();            

            kernel.Bind<IReportRepository>().To<ReportRepository>().InRequestScope();
            kernel.Bind<IExcelForm>().To<ExcelForm>().InRequestScope();
            kernel.Bind<IImportDataServer>().To<ImportDataServer>().InRequestScope();
            kernel.Bind<ICalendarLogic>().To<CalendarLogic>().InRequestScope();
            
            kernel.Bind<IActiveDirectoryProvider>().To<ActiveDirectoryProvider>().InRequestScope();

            kernel.BindFilter<EmployeeContextFilter>(FilterScope.Global, 0).InRequestScope();
            kernel.BindFilter<LogUserActivityFilter>(FilterScope.Global, 0).InRequestScope();
            kernel.BindFilter<LogErrorFilter>(FilterScope.Global, 0).InRequestScope();
            
            //kernel.BindFilter<LogErrorFilter>(FilterScope.Global, 0).InRequestScope();
        }        
    }
}

