namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_230 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.RawSystems", "MakeDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RawSystems", "MakeDate", c => c.DateTime(nullable: false));
        }
    }
}
