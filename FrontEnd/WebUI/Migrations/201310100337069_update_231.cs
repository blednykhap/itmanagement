namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_231 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RawSystems", "CompanyId", c => c.Int());
            AddForeignKey("dbo.RawSystems", "CompanyId", "dbo.Companies", "CompanyId");
            CreateIndex("dbo.RawSystems", "CompanyId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.RawSystems", new[] { "CompanyId" });
            DropForeignKey("dbo.RawSystems", "CompanyId", "dbo.Companies");
            DropColumn("dbo.RawSystems", "CompanyId");
        }
    }
}
