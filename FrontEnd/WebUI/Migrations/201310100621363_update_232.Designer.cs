// <auto-generated />
namespace WebUI.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class update_232 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(update_232));
        
        string IMigrationMetadata.Id
        {
            get { return "201310100621363_update_232"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
