namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_232 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.RawInstalledSoftwares", "MakeDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RawInstalledSoftwares", "MakeDate", c => c.DateTime(nullable: false));
        }
    }
}
