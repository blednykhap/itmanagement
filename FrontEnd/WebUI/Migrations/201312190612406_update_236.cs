namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_236 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Subdivisions",
                c => new
                    {
                        SubdivisionId = c.Int(nullable: false, identity: true),
                        ParentSubdivisionId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        Code = c.String(maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 240),
                        IsRoot = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SubdivisionId)
                .ForeignKey("dbo.Subdivisions", t => t.ParentSubdivisionId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.ParentSubdivisionId)
                .Index(t => t.CompanyId);
            
            AddColumn("dbo.Departments", "SubdivisionId", c => c.Int());
            AddForeignKey("dbo.Departments", "SubdivisionId", "dbo.Subdivisions", "SubdivisionId");
            CreateIndex("dbo.Departments", "SubdivisionId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Subdivisions", new[] { "CompanyId" });
            DropIndex("dbo.Subdivisions", new[] { "ParentSubdivisionId" });
            DropIndex("dbo.Departments", new[] { "SubdivisionId" });
            DropForeignKey("dbo.Subdivisions", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Subdivisions", "ParentSubdivisionId", "dbo.Subdivisions");
            DropForeignKey("dbo.Departments", "SubdivisionId", "dbo.Subdivisions");
            DropColumn("dbo.Departments", "SubdivisionId");
            DropTable("dbo.Subdivisions");
        }
    }
}
