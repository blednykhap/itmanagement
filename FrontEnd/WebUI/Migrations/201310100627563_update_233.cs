namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_233 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RawInstalledSoftwares", "SoftwareId", c => c.Int());
            AddColumn("dbo.RawInstalledSoftwares", "ContractorId", c => c.Int());
            AddForeignKey("dbo.RawInstalledSoftwares", "SoftwareId", "dbo.Softwares", "SoftwareId");
            AddForeignKey("dbo.RawInstalledSoftwares", "ContractorId", "dbo.Contractors", "ContractorId");
            CreateIndex("dbo.RawInstalledSoftwares", "SoftwareId");
            CreateIndex("dbo.RawInstalledSoftwares", "ContractorId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.RawInstalledSoftwares", new[] { "ContractorId" });
            DropIndex("dbo.RawInstalledSoftwares", new[] { "SoftwareId" });
            DropForeignKey("dbo.RawInstalledSoftwares", "ContractorId", "dbo.Contractors");
            DropForeignKey("dbo.RawInstalledSoftwares", "SoftwareId", "dbo.Softwares");
            DropColumn("dbo.RawInstalledSoftwares", "ContractorId");
            DropColumn("dbo.RawInstalledSoftwares", "SoftwareId");
        }
    }
}
