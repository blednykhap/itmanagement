namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_234 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Maintenances", "Is24Hours", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Maintenances", "Is24Hours");
        }
    }
}
