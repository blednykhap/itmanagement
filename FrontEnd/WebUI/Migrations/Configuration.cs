namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Core.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ITManagementDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ITManagementDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.

            #region Companies
            context.Companies.AddOrUpdate(
              p => p.Name,
              new Company { Name = "������ ��������" },
              new Company { Name = "��� '�������������'", BMC = "��� �������������", EKC = 231, SAP = "��� �������������" }
            );
            #endregion

            #region Positions
            context.Positions.AddOrUpdate(p => p.Name,
                new Position { Name = "������ ��������"},
                new Position { Name = "������� �������"});
            #endregion

            #region SupportLevels
            context.SupportLevels.AddOrUpdate(p => p.Name,
                new SupportLevel { Name = "�����������"},
                new SupportLevel { Name = "�������"},
                new SupportLevel { Name = "������������"},
                new SupportLevel { Name = "�����������"});
            #endregion

            #region SupportLevels
            context.ServiceLines.AddOrUpdate(p => p.Code,
                new ServiceLine { Code = "APP", Name = "��������� ����������" },
                new ServiceLine { Code = "AUT", Name = "����. ����. ������� �������. ��������������� ���������" },
                new ServiceLine { Code = "CNR", Name = "������� ������. ����" },
                new ServiceLine { Code = "DSK", Name = "���. ��������� ��" },
                new ServiceLine { Code = "OTL", Name = "������� ���������, ������������ � �������������������" },
                new ServiceLine { Code = "PRJ", Name = "��������� ����������" },
                new ServiceLine { Code = "SDA", Name = "���������� ������� �������" },
                new ServiceLine { Code = "SEC", Name = "��������� ����" },
                new ServiceLine { Code = "SFS", Name = "���. ��������� ��" },
                new ServiceLine { Code = "STL", Name = "����������� ����� � �������" });
            #endregion

            #region Measures
            context.Measures.AddOrUpdate(p => p.Name,
                new Measure { Name = "������������" }, 
                new Measure { Name = "�������" },
                new Measure { Name = "��������" },
                new Measure { Name = "�/�" },
                new Measure { Name = "�/�" },
                new Measure { Name = "���. �������" },
                new Measure { Name = "�����������" },
                new Measure { Name = "��." },
                new Measure { Name = "�����������" });
            #endregion

            #region SystemParameter
            //context.SystemParameters.AddOrUpdate(p => p.Name,
            //    new SystemParameter { Name = "DomainController", Description = "���������� ������", Value = "LDAP://DC=corp,DC=tnk-bp,DC=ru" });
            context.SystemParameters.AddOrUpdate(p => p.Name,
                new SystemParameter { Name = "DomainServerController", Description = "������ ����������� ������", Value = "corp.tnk-bp.ru" });
            context.SystemParameters.AddOrUpdate(p => p.Name,
                new SystemParameter { Name = "DomainRootContainer", Description = "�������� ������� ����������� ������", Value = "DC=corp,DC=tnk-bp,DC=ru" });
            context.SystemParameters.AddOrUpdate(p => p.Name,
                new SystemParameter { Name = "ExcelMaxRow", Description = "������������ ���������� ����� � Excel �������", Value = "32000" });
            #endregion

            #region Components

            context.Components.AddOrUpdate(p => p.Code,
                new Component { Code = "ACC", Name = "������������ ������������" },
                new Component { Code = "ACCI", Name = "������������ �������" },
                new Component { Code = "DETECT", Name = "��������� �����������" },
                new Component { Code = "RESOLVE", Name = "���������� �����������" },
                new Component { Code = "AUX", Name = "���������� �����������" },
                new Component { Code = "ENH", Name = "������������ �/�" },
                new Component { Code = "EXP", Name = "��������� ����������� � ������� ���������" },
                new Component { Code = "QDB" },
                new Component { Code = "REP" });
            
//            ACC 
//ACCI
//ITSM
//EXP 
//NSO 
//SMN 
//INF 
//71  
//AUX 
//REP 
//NPR 
//CRM 
//MML 
//1001
//SFS 
//SOEI
//RDMS
//TRA 
//SFW 
//PRI 
//LEA 
//SOE 
//BNK 
//ENH 
//CNR 
//SEC 
//SUP 
//UNP 
//QDB 
            //MEN 

            #endregion

            #region SecAreas
            context.SecAreas.AddOrUpdate(
              p => p.Name,
              new SecArea { SecAreaId = 1, Name = null, Description = "NULL" },
              new SecArea { SecAreaId = 2, Name = "Customer", Description = "NULL" },
              new SecArea { SecAreaId = 3, Name = "Admin", Description = "NULL" }
            );
            #endregion

            #region SoftwareType
            context.SoftwareTypes.AddOrUpdate(p => p.Code,
                new SoftwareType() { Code = "Production", Name = "��������� ��", IsNeedLicense = true },
                new SoftwareType() { Code = "Driver", Name = "��������", IsNeedLicense = false },
                new SoftwareType() { Code = "Codec", Name = "������", IsNeedLicense = false },
                new SoftwareType() { Code = "Freeware", Name = "��������� ��", IsNeedLicense = false },
                new SoftwareType() { Code = "Updates", Name = "����������", IsNeedLicense = false },
                new SoftwareType() { Code = "ManagerUpdates", Name = "�������� ����������", IsNeedLicense = false },
                new SoftwareType() { Code = "Other", Name = "�������������� ��", IsNeedLicense = false }
                );
            #endregion

            #region Software
            context.Softwares.AddOrUpdate(p => p.Name,
                new Software() { Name = "SAP HR ������" },
                new Software() { Name = "SAP US ������" },
                new Software() { Name = "SAP MDM ������" });
            #endregion

            context.SaveChanges();

            #region CostCenters
            context.CostCenters.AddOrUpdate(
              p => p.Code,
              new CostCenter { Code = "000000000", CompanyId = 1, Name = "������ ��������" }
            );
            #endregion

            #region Departments
            context.Departments.AddOrUpdate(
              p => p.Name,
              new Department { CompanyId = 1, CostCenterId = 1, Name = "������ ��������", IsActive = true }
            );
            #endregion

            #region Roles
            context.Roles.AddOrUpdate(
              p => p.Name,              
              new Role { Name = "Customer", GroupAD = "Orenburg_ULU_Customer", SecAreaId = 2 },
              //new Role { Name = "Role_Customer", GroupAD = "all_tnk_bp_users", SecAreaId = 2 },
              //new Role { Name = "Role_Admin", GroupAD = "Orenburg_ULU_Admin", SecAreaId = 3 } 
              //new Role { Name = "Admin", GroupAD = "Orenburg-Sup-Other", SecAreaId = 3 } 
              new Role { Name = "Admin", GroupAD = "Orenburg_ULU_Admin", SecAreaId = 3 } 
            );
            #endregion

            context.SaveChanges();

            #region Employees
            context.Employees.AddOrUpdate(
              p => p.Authid,
              new Employee { Authid = "CORP\\apblednykh", Surname = "�������", Firstname = "������", Middlename = "��������", 
                  Fullname = "������� ������ ��������", CostCenterId = 1, CompanyId = 1, PositionId = 2, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "CORP\\TEBocharova", Surname = "��������", Firstname = "�������", Middlename = "�", 
                  Fullname = "�������� ������� �", CostCenterId = 1, CompanyId = 1, PositionId = 1, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "CORP\\orn_service_ulu", Surname = "ULU", Firstname = "Service", Middlename = "",  
                  Fullname = "��������� �� ����������", CostCenterId = 1, CompanyId = 1, PositionId = 1, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "user_ro", Surname = "BMC", Firstname = "Service", Middlename = "",  
                  Fullname = "��������� �� � BMC Remedy", CompanyId = 1, CostCenterId = 1, PositionId = 1, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "ro_sms05", Surname = "SMS", Firstname = "Service", Middlename = "",  
                  Fullname = "��������� �� � MS SMS", CompanyId = 1, CostCenterId = 1, PositionId = 1, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "css", Surname = "CSS", Firstname = "Service", Middlename = "",  
                  Fullname = "��������� CSS", CompanyId = 1, CostCenterId = 1, PositionId = 1, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "pascal\\sinoptik", Surname = "�������", Firstname = "������", Middlename = "����������", 
                  Fullname = "������� ������ ����������", CompanyId = 1, CostCenterId = 1, PositionId = 2, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "CORP\\aanaidenov", Surname = "��������", Firstname = "���������", Middlename = "�������������", 
                  Fullname = "�������� ��������� �������������", CostCenterId = 1, CompanyId = 1, PositionId = 2, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "CORP\\daolkhov", Surname = "������", Firstname = "�������", Middlename = "�������������", 
                  Fullname = "������ ������� �������������", CostCenterId = 1, CompanyId = 1, PositionId = 2, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "CORP\\gmvyazovova", Surname = "��������", Firstname = "������", Middlename = "����������", 
                  Fullname = "�������� ������ ����������", CostCenterId = 1, CompanyId = 1, PositionId = 1, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "CORP\\mvzayakin", Surname = "������", Firstname = "������", Middlename = "�", 
                  Fullname = "������ ������ �", CostCenterId = 1, CompanyId = 1, PositionId = 1, DepartmentId = 1, IsActive = true },
              new Employee { Authid = "CORP\\mwbobyileva", Surname = "��������", Firstname = "������", Middlename = "����������", 
                  Fullname = "�������� ������ ����������", CostCenterId = 1, CompanyId = 1, PositionId = 1, DepartmentId = 1, IsActive = true }
            );            
            #endregion

            context.SaveChanges();

            #region HelpActicles
            context.HelpArticles.AddOrUpdate(p => p.Content,
                new HelpArticle { EmployeeId = 1, Title = "���� ������ �� �������", Content = "Article1"},
                new HelpArticle { EmployeeId = 1, Title = "�������� ������� �����", Content = "Article2"},
                new HelpArticle { EmployeeId = 1, Title = "���� ������ �� ���������", Content = "Article3" },
                new HelpArticle { EmployeeId = 1, Title = "���������� ������ �� ���������", Content = "Article4" },
                new HelpArticle { EmployeeId = 1, Title = "�������� �������� ����������", Content = "Article5" },
                new HelpArticle { EmployeeId = 1, Title = "�������� ��� �� ����� Excel", Content = "Article6" });
            #endregion

        }
    }
}
