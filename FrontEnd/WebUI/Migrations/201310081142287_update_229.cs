namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_229 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeProfiles", "IsDisplayActiveOnly", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EmployeeProfiles", "IsDisplayActiveOnly");
        }
    }
}
