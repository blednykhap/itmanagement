﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Telerik.Web.Mvc;
using Core.Views;
using Business.ServerSide;

namespace WebUI.Controllers
{
    public class RawInstalledSoftwareController : Controller
    {
        private readonly IBaseRepository<RawInstalledSoftware> _rRawInstalledSoftware;
        private readonly IBaseRepository<Software> _rSoftware;
        private readonly IBaseRepository<Contractor> _rContractor;
        private readonly IImportDataServer _sImportData;

        public RawInstalledSoftwareController(IBaseRepository<RawInstalledSoftware> rRawInstalledSoftware,
            IBaseRepository<Software> rSoftware, IBaseRepository<Contractor> rContractor,
            IImportDataServer sImportData)
        {
            _rRawInstalledSoftware = rRawInstalledSoftware;
            _rSoftware = rSoftware;
            _rContractor = rContractor;
            _sImportData = sImportData;
        }

        public ActionResult Index(string layout)
        {
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");
            ViewBag.Contractors = new SelectList(_rContractor.GetAll(), "ContractorId", "Name");

            if (layout == "Admin")
            {
                return View("Index", "~/Areas/Admin/Views/Shared/_Layout.cshtml");
            }
            else
            {
                return View("Index", "~/Areas/Customer/Views/Shared/_Layout.cshtml");
            }    
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var rawInstalledSoftwares = (from p in _rRawInstalledSoftware.GetAll().ToList()
                                         select new RawInstalledSoftwareView { 
                                             ContractorId = (p.Contractor != null ? p.Contractor.Name : ""),
                                             InstallDate00 = p.InstallDate00,
                                             InstanceKey = p.InstanceKey,
                                             MachineId = p.MachineId,
                                             ProductName00 = p.ProductName00,
                                             ProductVersion00 = p.ProductVersion00,
                                             Publisher00 = p.Publisher00,
                                             RawInstalledSoftwareId = p.RawInstalledSoftwareId,
                                             ServicePack00 = p.ServicePack00,
                                             SoftwareId = (p.Software != null ? p.Software.Name : "")                                             
                                         });
            return View(new GridModel(rawInstalledSoftwares));
        }

        [GridAction]
        public ActionResult _CreateUpdate(RawInstalledSoftware rawInstalledSoftware)
        {
            if (ModelState.IsValid)
            {
                if (rawInstalledSoftware.RawInstalledSoftwareId.Equals(0))
                {
                    _rRawInstalledSoftware.Create(rawInstalledSoftware);
                }
                else
                {
                    _rRawInstalledSoftware.Update(rawInstalledSoftware);
                }

                rawInstalledSoftware.Software = _rSoftware.Get(p => p.SoftwareId == rawInstalledSoftware.SoftwareId);
                rawInstalledSoftware.Contractor = _rContractor.Get(p => p.ContractorId == rawInstalledSoftware.ContractorId);
            }

            var rawInstalledSoftwares = (from p in _rRawInstalledSoftware.GetAll().ToList()
                                         select new RawInstalledSoftwareView
                                         {
                                             ContractorId = (p.Contractor != null ? p.Contractor.Name : ""),
                                             InstallDate00 = p.InstallDate00,
                                             InstanceKey = p.InstanceKey,
                                             MachineId = p.MachineId,
                                             ProductName00 = p.ProductName00,
                                             ProductVersion00 = p.ProductVersion00,
                                             Publisher00 = p.Publisher00,
                                             RawInstalledSoftwareId = p.RawInstalledSoftwareId,
                                             ServicePack00 = p.ServicePack00,
                                             SoftwareId = (p.Software != null ? p.Software.Name : "")
                                         });
            return View(new GridModel(rawInstalledSoftwares));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var rawInstalledSoftware = _rRawInstalledSoftware.Get(p => p.RawInstalledSoftwareId == id);
            _rRawInstalledSoftware.Delete(rawInstalledSoftware);

            var rawInstalledSoftwares = (from p in _rRawInstalledSoftware.GetAll().ToList()
                                         select new RawInstalledSoftwareView
                                         {
                                             ContractorId = (p.Contractor != null ? p.Contractor.Name : ""),
                                             InstallDate00 = p.InstallDate00,
                                             InstanceKey = p.InstanceKey,
                                             MachineId = p.MachineId,
                                             ProductName00 = p.ProductName00,
                                             ProductVersion00 = p.ProductVersion00,
                                             Publisher00 = p.Publisher00,
                                             RawInstalledSoftwareId = p.RawInstalledSoftwareId,
                                             ServicePack00 = p.ServicePack00,
                                             SoftwareId = (p.Software != null ? p.Software.Name : "")
                                         });
            return View(new GridModel(rawInstalledSoftwares));
        }

        #endregion

        #region Import

        [HttpPost]
        public ContentResult _Import()
        {
            return Content(_sImportData.ImportInstalledSoftware());
        }

        [HttpPost]
        public ContentResult _Parse()
        {
            return Content(_sImportData.ParseInstalledSoftware());
        }

        [HttpPost]
        public ContentResult _Load()
        {
            return Content(_sImportData.LoadInstalledSoftware());
        }

        #endregion

    }
}
