﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;
using Business.ServerSide;
using Core.Filters;

namespace WebUI.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class RawEmployeeController : Controller
    {
        private readonly IBaseRepository<RawEmployee> _rRawEmployee;
        private readonly IBaseRepository<Employee> _rEmployee;
        private readonly IBaseRepository<CostCenter> _rCostCenter;
        private readonly IBaseRepository<Position> _rPosition;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<Department> _rDepartment;
        private readonly IImportDataServer _sImportDataServer;

        public RawEmployeeController(IBaseRepository<RawEmployee> rRawEmployee,
            IBaseRepository<Employee> rEmployee, IBaseRepository<CostCenter> rCostCenter,
            IBaseRepository<Position> rPosition, IBaseRepository<Company> rCompany, 
            IBaseRepository<Department> rDepartment, IImportDataServer sImportDataServer)
        {
            _rRawEmployee = rRawEmployee;
            _rEmployee = rEmployee;
            _rCostCenter = rCostCenter;
            _rPosition = rPosition;
            _rCompany = rCompany;
            _rDepartment = rDepartment;
            _sImportDataServer = sImportDataServer;
        }

        public ActionResult Index(string layout)
        {
            var costCenters = (from p in _rCostCenter.GetAll().ToList()
                               select new CostCenterShortView
                               {
                                   CostCenterId = p.CostCenterId,                                   
                                   Name = p.Code + (p.Code != null && p.Name != null ? " - " : "") + p.Name
                               });
            var departments = (from p in _rDepartment.GetAll().ToList()
                               select new DepartmentShortView
                               {
                                   DepartmentId = p.DepartmentId,
                                   Name = p.Code + (p.Code != null && p.Name != null ? " - " : "") + p.Name
                               });

            ViewBag.Employees = new SelectList(_rEmployee.GetAll(), "EmployeeId", "Fullname");
            ViewBag.CostCenters = new SelectList(costCenters, "CostCenterId", "Name");
            ViewBag.Positions = new SelectList(_rPosition.GetAll(), "PositionId", "Name");
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.Departments = new SelectList(departments, "DepartmentId", "Name");

            if (layout == "Admin")
            {
                return View("Index", "~/Areas/Admin/Views/Shared/_Layout.cshtml");
            }
            else
            {
                return View("Index", "~/Areas/Customer/Views/Shared/_Layout.cshtml");
            }
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var rawEmployees = (from p in _rRawEmployee.GetAll().ToList()
                                select new RawEmployeeView { 
                                   RawEmployeeId = p.RawEmployeeId,                                   
                                   RawAuthid = p.RawAuthid,
                                   CompanyId = (p.Company != null ? p.Company.Name : ""),
                                   RawCostCenter = p.RawCostCenter,
                                   CostCenterId = (p.CostCenter != null ? 
                                        (p.CostCenter.Code + (p.CostCenter.Code != null && p.CostCenter.Name != null ? " - " : "") + p.CostCenter.Name) : ""),                                        
                                   RawDepartmentCode = p.RawDepartmentCode,
                                   RawDepartmentCodes = p.RawDepartmentCodes,
                                   RawDepartmentNames = p.RawDepartmentNames,
                                   DepartmentId = (p.Department != null ?
                                        (p.Department.Code + (p.Department.Code != null && p.Department.Name != null ? " - " : "") + p.Department.Name) : "" ),
                                   RawDischargeDate = p.RawDischargeDate,
                                   RawEMail = p.RawEMail,
                                   RawFirstname = p.RawFirstname,
                                   RawMiddlename = p.RawMiddlename,
                                   RawFullname = (p.RawSurname != null ? p.RawSurname + " " : "") +
                                        (p.RawFirstname != null ? p.RawFirstname + " " : "") + (p.RawMiddlename != null ? p.RawMiddlename : ""),
                                   EmployeeId = (p.Employee != null ? p.Employee.Fullname : ""),
                                   RawPosition = p.RawPosition,
                                   PositionId = (p.Position != null ? p.Position.Name : ""),
                                   RawSurname = p.RawSurname                                   
                               });
            return View(new GridModel(rawEmployees));
        }

        [GridAction]
        public ActionResult _CreateUpdate(RawEmployee rawEmployee)
        {
            if (rawEmployee.RawEmployeeId.Equals(0))
            {
                _rRawEmployee.Create(rawEmployee);
            }
            else
            {
                _rRawEmployee.Update(rawEmployee);
            }

            rawEmployee.CostCenter = _rCostCenter.Get(p => p.CostCenterId == rawEmployee.CostCenterId);
            rawEmployee.Employee = _rEmployee.Get(p => p.EmployeeId == rawEmployee.EmployeeId);
            rawEmployee.Position = _rPosition.Get(p => p.PositionId == rawEmployee.PositionId);
            rawEmployee.Company = _rCompany.Get(p => p.CompanyId == rawEmployee.CompanyId);

            var rawEmployees = (from p in _rRawEmployee.GetAll().ToList()
                                select new RawEmployeeView
                                {
                                    RawEmployeeId = p.RawEmployeeId,
                                    RawAuthid = p.RawAuthid,
                                    CompanyId = (p.Company != null ? p.Company.Name : ""),
                                    RawCostCenter = p.RawCostCenter,
                                    CostCenterId = (p.CostCenter != null ?
                                        (p.CostCenter.Code + (p.CostCenter.Code != null && p.CostCenter.Name != null ? " - " : "") + p.CostCenter.Name) : ""),
                                    RawDepartmentCode = p.RawDepartmentCode,
                                    RawDepartmentCodes = p.RawDepartmentCodes,
                                    RawDepartmentNames = p.RawDepartmentNames,
                                    DepartmentId = (p.Department != null ?
                                        (p.Department.Code + (p.Department.Code != null && p.Department.Name != null ? " - " : "") + p.Department.Name) : ""),
                                    RawDischargeDate = p.RawDischargeDate,
                                    RawEMail = p.RawEMail,
                                    RawFirstname = p.RawFirstname,
                                    RawMiddlename = p.RawMiddlename,
                                    RawFullname = (p.RawSurname != null ? p.RawSurname + " " : "") +
                                         (p.RawFirstname != null ? p.RawFirstname + " " : "") + (p.RawMiddlename != null ? p.RawMiddlename : ""),
                                    EmployeeId = (p.Employee != null ? p.Employee.Fullname : ""),
                                    RawPosition = p.RawPosition,
                                    PositionId = (p.Position != null ? p.Position.Name : ""),
                                    RawSurname = p.RawSurname
                                });
            return View(new GridModel(rawEmployees));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var rawEmployee = _rRawEmployee.Get(p => p.RawEmployeeId == id);
            _rRawEmployee.Delete(rawEmployee);

            var rawEmployees = (from p in _rRawEmployee.GetAll().ToList()
                                select new RawEmployeeView
                                {
                                    RawEmployeeId = p.RawEmployeeId,
                                    RawAuthid = p.RawAuthid,
                                    CompanyId = (p.Company != null ? p.Company.Name : ""),
                                    RawCostCenter = p.RawCostCenter,
                                    CostCenterId = (p.CostCenter != null ?
                                        (p.CostCenter.Code + (p.CostCenter.Code != null && p.CostCenter.Name != null ? " - " : "") + p.CostCenter.Name) : ""),
                                    RawDepartmentCode = p.RawDepartmentCode,
                                    RawDepartmentCodes = p.RawDepartmentCodes,
                                    RawDepartmentNames = p.RawDepartmentNames,
                                    DepartmentId = (p.Department != null ?
                                        (p.Department.Code + (p.Department.Code != null && p.Department.Name != null ? " - " : "") + p.Department.Name) : ""),
                                    RawDischargeDate = p.RawDischargeDate,
                                    RawEMail = p.RawEMail,
                                    RawFirstname = p.RawFirstname,
                                    RawMiddlename = p.RawMiddlename,
                                    RawFullname = (p.RawSurname != null ? p.RawSurname + " " : "") +
                                         (p.RawFirstname != null ? p.RawFirstname + " " : "") + (p.RawMiddlename != null ? p.RawMiddlename : ""),
                                    EmployeeId = (p.Employee != null ? p.Employee.Fullname : ""),
                                    RawPosition = p.RawPosition,
                                    PositionId = (p.Position != null ? p.Position.Name : ""),
                                    RawSurname = p.RawSurname
                                });
            return View(new GridModel(rawEmployees));
        }

        #endregion

        #region Import

        public ContentResult _Import()
        {
            _sImportDataServer.ImportEmployee();
            return Content("Данные обработаны");
        }

        public ContentResult _Parse()
        {
            _sImportDataServer.ParseEmployee();
            return Content("Данные обработаны");
        }

        public ContentResult _Load()
        {
            _sImportDataServer.LoadEmployee();
            return Content("Данные обработаны");
        }

        #endregion

    }
}
