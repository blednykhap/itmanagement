﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Others;
using Business.ServerSide;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class RawMaintenanceByUserController : Controller
    {
        private readonly IBaseRepository<RawMaintenanceByUser> _rRawMaintenanceByUser;
        private readonly IBaseRepository<CompanyStructure> _rCompanyStructure;
        private readonly IBaseRepository<Maintenance> _rMaintenance;
        private readonly IBaseRepository<Component> _rComponent;
        private readonly IBaseRepository<Software> _rSoftware;
        private readonly IBaseRepository<SoftwareModule> _rSoftwareModule;
        private readonly IBaseRepository<SupportLevel> _rSupportLevel;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly ImportDataServer _sImportDataServer;
        private readonly ICalendarLogic _lCalendar;

        public RawMaintenanceByUserController(IBaseRepository<RawMaintenanceByUser> rRawMaintenanceByUser,
            IBaseRepository<CompanyStructure> rCompanyStructure, IBaseRepository<Maintenance> rMaintenance,
            IBaseRepository<Software> rSoftware, IBaseRepository<SoftwareModule> rSoftwareModule, 
            IBaseRepository<SupportLevel> rSupportLevel, IBaseRepository<Company> rCompany, 
            ImportDataServer sImportDataServer, IBaseRepository<Component> rComponent, 
            ICalendarLogic lCalendar)
        {
            _rRawMaintenanceByUser = rRawMaintenanceByUser;
            _rCompanyStructure = rCompanyStructure;
            _rMaintenance = rMaintenance;
            _rSoftware = rSoftware;
            _rSoftwareModule = rSoftwareModule;
            _rSupportLevel = rSupportLevel;
            _rCompany = rCompany;
            _sImportDataServer = sImportDataServer;
            _rComponent = rComponent;
            _lCalendar = lCalendar;
        }

        public ActionResult Index(string layout)
        {
            var companyStructures = (from p in _rCompanyStructure.GetAll().ToList()
                                     select new CompanyStructureShortView
                                     {
                                         CompanyStructureId = p.CompanyStructureId,
                                         Name = (p.Department != null ? p.Department.Name : p.Employee.Fullname)
                                     });

            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.CompanyStructures = new SelectList(companyStructures, "CompanyStructureId", "Name");
            ViewBag.Maintenances = new SelectList(_rMaintenance.GetAll(), "MaintenanceId", "Name");
            ViewBag.Components = new SelectList(_rComponent.GetAll(), "ComponentId", "Name");
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");
            ViewBag.SoftwareModules = new SelectList(_rSoftwareModule.GetAll(), "SoftwareModuleId", "Name");
            ViewBag.SupportLevels = new SelectList(_rSupportLevel.GetAll(), "SupportLevelId", "Name");
            ViewBag.Periods = new SelectList(_lCalendar.GetPeriods(), "CodeMonth", "Name");

            if (layout == "Admin")
            {
                return View("Index", "~/Areas/Admin/Views/Shared/_Layout.cshtml");
            }
            else
            {
                return View("Index", "~/Areas/Customer/Views/Shared/_Layout.cshtml");
            }
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var rawMaintenanceByUsers = (from p in _rRawMaintenanceByUser.GetAll().ToList()
                                         select new RawMaintenanceByUserView { 
                                             RawMaintenanceByUserId = p.RawMaintenanceByUserId,                                             
                                             CompanyId = (p.Company != null ? p.Company.Name : ""),
                                             RawLogin = p.RawLogin,
                                             RawFIO = p.RawFIO,
                                             RawDepartment = p.RawDepartment,
                                             RawCostCenter = p.RawCostCenter,
                                             RawMaintenance = p.RawMaintenance,
                                             ComponentId = (p.Component != null ? p.Component.Code : ""),
                                             RawSupportLevel = p.RawSupportLevel,
                                             RawSoftware = p.RawSoftware,                                             
                                             CompanyStructureId = (p.CompanyStructure != null ?
                                                (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname) : ""),
                                             MaintenanceId = (p.Maintenance != null ? p.Maintenance.Name : ""),
                                             SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                             SoftwareModuleId = (p.SoftwareModule != null ? p.SoftwareModule.Name : ""),
                                             SupportLevelId = (p.SupportLevel != null ? p.SupportLevel.Name : ""),
                                             CodeMonthFrom = p.CodeMonthFrom,
                                             CodeMonthTo = p.CodeMonthTo
                                         });

            return View(new GridModel(rawMaintenanceByUsers));
        }

        #endregion

        #region Import

        public void _Import(HttpPostedFileBase attachments, int companyId = 0)
        {
            if (attachments != null && companyId != 0 && companyId != 1)
            {
                _rRawMaintenanceByUser.BulkDelete("RawMaintenanceByUsers");                

                var fileName = attachments.FileName;
                string connectionString = "";

                FileInfo fileInfo = new FileInfo(fileName);

                if (fileInfo.Extension == ".xlsx")
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties=Excel 12.0", fileName);
                }
                else
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0", fileName);
                }

                try
                {
                    var adapter = new OleDbDataAdapter("SELECT * FROM [Лист1$]", connectionString);
                    var dataSet = new DataSet();

                    adapter.Fill(dataSet, "RawMaintenanceByUsers");
                    DataTable dataTable = dataSet.Tables["RawMaintenanceByUsers"];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        _rRawMaintenanceByUser.Create(new RawMaintenanceByUser()
                        {
                            RawFIO = row.ItemArray[0].ToString(),
                            RawLogin = row.ItemArray[1].ToString(),
                            RawDepartment = row.ItemArray[2].ToString(),
                            RawCostCenter = row.ItemArray[3].ToString(),
                            RawMaintenance = row.ItemArray[4].ToString(),
                            RawSoftware = row.ItemArray[5].ToString(),
                            RawSupportLevel = row.ItemArray[6].ToString(),
                            CompanyId = companyId
                        });
                    }
                }
                catch { }
            }
        }

        public ContentResult _Parse()
        {
            return Content(_sImportDataServer.ParseMaintenanceByUser());
        }

        public ContentResult _Load()
        {            
            return Content(_sImportDataServer.LoadMaintenanceByUser());
        }

        #endregion

    }
}
