﻿using Business.Others;
using Core.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    [CustomAuthorize(Roles="Admin, Customer")]
    public class CalendarController : Controller
    {
        private readonly ICalendarLogic _lCalendar;

        public CalendarController(ICalendarLogic lCalendar)
        {
            _lCalendar = lCalendar;
        }

        public JsonResult _CodeMonth()
        {
            //return _lCalendar.GetPeriods();
            var currentPeriod = DateTime.Now.Year * 12 + DateTime.Now.Month;

            return new JsonResult { Data = new SelectList(_lCalendar.GetPeriods(), "CodeMonth", "Name", currentPeriod) };
        }
    }
}
