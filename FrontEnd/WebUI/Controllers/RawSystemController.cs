﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.ServerSide;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class RawSystemController : Controller
    {
        private readonly IBaseRepository<RawSystem> _rRawSystem;
        private readonly IBaseRepository<Employee> _rEmployee;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IImportDataServer _sImportData;

        public RawSystemController(IBaseRepository<RawSystem> rRawSystem,
            IBaseRepository<Employee> rEmployee, IBaseRepository<Company> rCompany,
            IImportDataServer sImportData)
        {
            _rRawSystem = rRawSystem;
            _rEmployee = rEmployee;
            _rCompany = rCompany;
            _sImportData = sImportData;
        }

        public ActionResult Index(string layout)
        {
            var isDisplayActiveOnly = Convert.ToBoolean(Session["IsDisplayActiveOnly"]);
            ViewBag.Employees = new SelectList(_rEmployee.GetList(p => (isDisplayActiveOnly ? p.IsActive == true : true)) , "EmployeeId", "Fullname");
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");

            if (layout == "Admin")
            {
                return View("Index", "~/Areas/Admin/Views/Shared/_Layout.cshtml");
            }
            else
            {
                return View("Index", "~/Areas/Customer/Views/Shared/_Layout.cshtml");
            }    
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var rawSystems = (from p in _rRawSystem.GetAll().ToList()
                              select new RawSystemView
                              {
                                  AgentTime = p.AgentTime,
                                  CompanyId = (p.Company != null ? p.Company.Name : ""),
                                  EmployeeId = (p.Employee != null ? p.Employee.Fullname : ""),
                                  Netbios_Name0 = p.Netbios_Name0,
                                  Operating_System_Name_and0 = p.Operating_System_Name_and0,
                                  RawSystemId = p.RawSystemId,
                                  ResourceId = p.ResourceId,
                                  User_Domain0 = p.User_Domain0,
                                  User_Name0 = p.User_Name0
                              });

            return View(new GridModel(rawSystems));
        }

        [GridAction]
        public ActionResult _CreateUpdate(RawSystem rawSystem)
        {
            if (ModelState.IsValid)
            {
                if (rawSystem.RawSystemId.Equals(0))
                {
                    _rRawSystem.Create(rawSystem);
                }
                else
                {
                    _rRawSystem.Update(rawSystem);
                }
                rawSystem.Company = _rCompany.Get(p => p.CompanyId == rawSystem.CompanyId);
                rawSystem.Employee = _rEmployee.Get(p => p.EmployeeId == rawSystem.EmployeeId);
            }

            var rawSystems = (from p in _rRawSystem.GetAll().ToList()
                              select new RawSystemView
                              {
                                  AgentTime = p.AgentTime,
                                  CompanyId = (p.Company != null ? p.Company.Name : ""),
                                  EmployeeId = (p.Employee != null ? p.Employee.Fullname : ""),
                                  Netbios_Name0 = p.Netbios_Name0,
                                  Operating_System_Name_and0 = p.Operating_System_Name_and0,
                                  RawSystemId = p.RawSystemId,
                                  ResourceId = p.ResourceId,
                                  User_Domain0 = p.User_Domain0,
                                  User_Name0 = p.User_Name0
                              });

            return View(new GridModel(rawSystems));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var rawSystem = _rRawSystem.Get(p => p.RawSystemId == id);
            _rRawSystem.Delete(rawSystem);

            var rawSystems = (from p in _rRawSystem.GetAll().ToList()
                              select new RawSystemView
                              {
                                  AgentTime = p.AgentTime,
                                  CompanyId = (p.Company != null ? p.Company.Name : ""),
                                  EmployeeId = (p.Employee != null ? p.Employee.Fullname : ""),
                                  Netbios_Name0 = p.Netbios_Name0,
                                  Operating_System_Name_and0 = p.Operating_System_Name_and0,
                                  RawSystemId = p.RawSystemId,
                                  ResourceId = p.ResourceId,
                                  User_Domain0 = p.User_Domain0,
                                  User_Name0 = p.User_Name0
                              });

            return View(new GridModel(rawSystems));
        }

        #endregion

        #region Import

        [HttpPost]
        public ContentResult _Import()
        {
            return Content(_sImportData.ImportSystem());
        }

        [HttpPost]
        public ContentResult _Parse()
        {
            return Content(_sImportData.ParseSystem());
        }

        [HttpPost]
        public ContentResult _Load()
        {
            return Content(_sImportData.LoadSystem());
        }

        #endregion

    }
}
