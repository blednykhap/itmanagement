﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Others;
using Business.ServerSide;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class RawSAPMaintenanceController : Controller
    {
        private readonly IBaseRepository<RawSAPMaintenance> _rRawSAPMaintenance;
        private readonly IBaseRepository<CompanyStructure> _rCompanyStructure;
        private readonly IBaseRepository<Maintenance> _rMaintenance;
        private readonly IImportDataServer _sImportDataServer;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<Software> _rSoftware;
        private readonly IBaseRepository<Component> _rComponent;
        private readonly ICalendarLogic _lCalendar;

        public RawSAPMaintenanceController(IBaseRepository<RawSAPMaintenance> rRawSAPMaintenance,
            IBaseRepository<CompanyStructure> rCompanyStructure, IImportDataServer sImportDataServer,
            IBaseRepository<Company> rCompany, IBaseRepository<Software> rSoftware,
            IBaseRepository<Maintenance> rMaintenance, IBaseRepository<Component> rComponent,
            ICalendarLogic lCalendar)
        {
            _rRawSAPMaintenance = rRawSAPMaintenance;
            _rCompanyStructure = rCompanyStructure;
            _rMaintenance = rMaintenance;
            _sImportDataServer = sImportDataServer;
            _rCompany = rCompany;
            _rSoftware = rSoftware;
            _rComponent = rComponent;
            _lCalendar = lCalendar;
        }

        public ActionResult Index(string layout)
        {
            if (layout == "Admin")
            {
                return View("Index", "~/Areas/Admin/Views/Shared/_Layout.cshtml");
            }
            else
            {
                return View("Index", "~/Areas/Customer/Views/Shared/_Layout.cshtml");
            }
        }

        public ActionResult _Grid(string module)
        {
            var companyStructures = (from p in _rCompanyStructure.GetAll().ToList()
                                     select new CompanyStructureShortView
                                     {
                                         CompanyStructureId = p.CompanyStructureId,
                                         Name = (p.Department != null ? p.Department.Name : p.Employee.Fullname)
                                     });

            ViewBag.Module = module;
            ViewBag.CompanyStructures = new SelectList(companyStructures, "CompanyStructureId", "Name");
            ViewBag.Maintenances = new SelectList(_rMaintenance.GetAll(), "MaintenanceId", "Name");
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.Softwares = new SelectList(_rSoftware.GetAll(), "SoftwareId", "Name");
            ViewBag.Components = new SelectList(_rComponent.GetAll(), "ComponentId", "Code");
            ViewBag.Periods = new SelectList(_lCalendar.GetPeriods(), "CodeMonth", "Name");

            return PartialView();
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index(string module)
        {
            var rawSAPMaintenances = (from p in _rRawSAPMaintenance.GetAll().ToList()
                                      where p.Module != null && p.Module.Trim().ToUpper() == module
                                      select new RawSAPMaintenanceView
                                      {
                                          RawSAPMaintenanceId = p.RawSAPMaintenanceId,
                                          CompanyStructureId = (p.CompanyStructure != null ?
                                                (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname) : ""),
                                          Module = p.Module,
                                          MaintenanceId = (p.Maintenance != null ? p.Maintenance.Name : ""),
                                          RawCompany = p.RawCompany,
                                          CompanyId = (p.Company != null ? p.Company.Name : ""),
                                          ComponentId = (p.Component != null ? p.Component.Code : ""),
                                          RawLogin = p.RawLogin,
                                          RawFullname = p.RawFullname,                                                                                    
                                          SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                          CodeMonthFrom = p.CodeMonthFrom,
                                          CodeMonthTo = p.CodeMonthTo
                                      });

            return View(new GridModel(rawSAPMaintenances.ToList()));
        }

        [GridAction]
        public ActionResult _CreateUpdate(RawSAPMaintenance rawSAPMaintenance, string module)
        {

            if (ModelState.IsValid)
            {
                if (rawSAPMaintenance.RawSAPMaintenanceId.Equals(0))
                {
                    _rRawSAPMaintenance.Create(rawSAPMaintenance);
                }
                else
                {
                    _rRawSAPMaintenance.Update(rawSAPMaintenance);                    
                }

                rawSAPMaintenance.CompanyStructure = _rCompanyStructure.Get(p => p.CompanyStructureId == rawSAPMaintenance.CompanyStructureId);
                rawSAPMaintenance.Maintenance = _rMaintenance.Get(p => p.MaintenanceId == rawSAPMaintenance.MaintenanceId);
                rawSAPMaintenance.Company = _rCompany.Get(p => p.CompanyId == rawSAPMaintenance.CompanyId);                    
                rawSAPMaintenance.Software = _rSoftware.Get(p => p.SoftwareId == rawSAPMaintenance.SoftwareId);
                rawSAPMaintenance.Component = _rComponent.Get(p => p.ComponentId == rawSAPMaintenance.ComponentId);                
            }

            var rawSAPMaintenances = (from p in _rRawSAPMaintenance.GetAll().ToList()
                                      where p.Module != null && p.Module.Trim().ToUpper() == module
                                      select new RawSAPMaintenanceView
                                      {
                                          RawSAPMaintenanceId = p.RawSAPMaintenanceId,
                                          CompanyStructureId = (p.CompanyStructure != null ?
                                                (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname) : ""),
                                          Module = p.Module,
                                          MaintenanceId = (p.Maintenance != null ? p.Maintenance.Name : ""),
                                          RawCompany = p.RawCompany,
                                          CompanyId = (p.Company != null ? p.Company.Name : ""),
                                          ComponentId = (p.Component != null ? p.Component.Code : ""),
                                          RawLogin = p.RawLogin,
                                          RawFullname = p.RawFullname,
                                          SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                          CodeMonthFrom = p.CodeMonthFrom,
                                          CodeMonthTo = p.CodeMonthTo
                                      });

            return View(new GridModel(rawSAPMaintenances.ToList()));
        }

        [GridAction]
        public ActionResult _Delete(int id, string module)
        {
            var rawSAPMaintenance = _rRawSAPMaintenance.Get(p => p.RawSAPMaintenanceId == id);
            _rRawSAPMaintenance.Delete(rawSAPMaintenance);

            var rawSAPMaintenances = (from p in _rRawSAPMaintenance.GetAll().ToList()
                                      where p.Module != null && p.Module.Trim().ToUpper() == module
                                      select new RawSAPMaintenanceView
                                      {
                                          RawSAPMaintenanceId = p.RawSAPMaintenanceId,
                                          CompanyStructureId = (p.CompanyStructure != null ?
                                                (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname) : ""),
                                          Module = p.Module,
                                          MaintenanceId = (p.Maintenance != null ? p.Maintenance.Name : ""),
                                          RawCompany = p.RawCompany,
                                          CompanyId = (p.Company != null ? p.Company.Name : ""),
                                          ComponentId = (p.Component != null ? p.Component.Code : ""),
                                          RawLogin = p.RawLogin,
                                          RawFullname = p.RawFullname,
                                          SoftwareId = (p.Software != null ? p.Software.Name : ""),
                                          CodeMonthFrom = p.CodeMonthFrom,
                                          CodeMonthTo = p.CodeMonthTo
                                      });

            return View(new GridModel(rawSAPMaintenances.ToList()));
        }

        #endregion

        #region _Import

        public void _Import(HttpPostedFileBase attachments, int period)
        {
            if (attachments != null && period != 0)
            {
                _rRawSAPMaintenance.BulkDelete("RawSAPMaintenances");

                var fileName = attachments.FileName;
                string connectionString = "";

                FileInfo fileInfo = new FileInfo(fileName);

                if (fileInfo.Extension == ".xlsx")
                {
                    connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties=Excel 12.0", fileName);
                }
                else
                {
                    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0", fileName);
                }

                //var adapter = new OleDbDataAdapter("SELECT \"Логин\", \"Имя пользователя\", \"Услуга\" FROM [SAP_Список$]", connectionString);
                var adapterUSHR = new OleDbDataAdapter("SELECT * FROM [SAP_Список$]", connectionString);
                var adapterMDM = new OleDbDataAdapter("SELECT * FROM [MDM_История подключений$]", connectionString);
                var dataSet = new DataSet();

                try
                {
                    adapterUSHR.Fill(dataSet, "USHR");
                    adapterMDM.Fill(dataSet, "MDM");

                    DataTable dataTableUSHR = dataSet.Tables["USHR"];
                    DataTable dataTableMDM = dataSet.Tables["MDM"];

                    //DateTime currentPeriod = Convert.ToDateTime(period);                          
                    string module = "";
                    //string company = "";

                    foreach (DataRow row in dataTableUSHR.Rows)
                    {
                        module = row.ItemArray[21].ToString().Length > 10 ? row.ItemArray[21].ToString().Substring(8, 2) : "";

                        if (module == "US" || module == "HR")
                        {
                            _rRawSAPMaintenance.Create(new RawSAPMaintenance()
                            {
                                CodeMonthFrom = period,
                                CodeMonthTo = period,
                                RawCompany = row.ItemArray[20].ToString(),
                                RawLogin = row.ItemArray[1].ToString(),
                                RawFullname = row.ItemArray[2].ToString(),
                                Module = module
                            });
                        }
                    }

                    foreach (DataRow row in dataTableMDM.Rows)
                    {
                        _rRawSAPMaintenance.Create(new RawSAPMaintenance()
                        {
                            CodeMonthFrom = period,
                            CodeMonthTo = period,
                            RawCompany = row.ItemArray[5].ToString(),
                            RawLogin = row.ItemArray[1].ToString(),
                            RawFullname = row.ItemArray[2].ToString(),
                            Module = "MDM"
                        });
                    }
                }
                catch {
                    //return Content("При обработке данных произошел сбой, смотрите журнал ошибок.");
                }

                //return Content("Данные обработаны.");
            }

            //return Content("При обработке данных произошел сбой, возможно заполнены не все параметры.");
        }

        public ContentResult _Parse()
        {            
            return Content(_sImportDataServer.ParseSAPMaintenance());
        }

        public ContentResult _Load()
        {
            return Content(_sImportDataServer.LoadSAPMaintenance());
        }

        #endregion

    }
}
