﻿using Core.Filters;
using Core.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Others;

namespace WebUI.Controllers
{    
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class InformationController : Controller
    {
        private readonly ActiveDirectoryProvider _rActiveDirectoryProvide;
        private readonly ICalendarLogic _lCalendar;

        public InformationController(ActiveDirectoryProvider rActiveDirectoryProvide, ICalendarLogic lCalendar)
        {
            _lCalendar = lCalendar;
            _rActiveDirectoryProvide = rActiveDirectoryProvide;
        }

        public ContentResult DisplayName()
        {
            var displayName = _rActiveDirectoryProvide.GetUserDisplayName(User.Identity.Name);
            return Content(displayName, "text/html");
        }

        public ContentResult CalculusPeriod()
        {
            var codeMonth = Convert.ToInt32(Session["CurrentCodeMonth"]);
            var currentDate = _lCalendar.GetDateByCodeMonth(codeMonth);

            return Content("Учетный период: " + currentDate.ToString("MMMM") +
                " '" + currentDate.ToString("yy"), "text/html");
        }
    }
}
