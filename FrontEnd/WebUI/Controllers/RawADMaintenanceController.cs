﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.Others;
using Business.ServerSide;
using Core.Filters;
using Core.Models;
using Core.Views;
using Telerik.Web.Mvc;

namespace WebUI.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class RawADMaintenanceController : Controller
    {
        private readonly IBaseRepository<RawADMaintenance> _rRawADMaintenance;
        private readonly IBaseRepository<SystemParameter> _rSystemParameter;
        private readonly IBaseRepository<Maintenance> _rMaintenance;
        private readonly IBaseRepository<Employee> _rEmployee;
        private readonly IBaseRepository<Company> _rCompany;
        private readonly IBaseRepository<CompanyStructure> _rCompanyStructure;
        private readonly IBaseRepository<Component> _rComponent;
        private readonly IImportDataServer _sImportDataServer;
        private readonly ICalendarLogic _lCalendar;
        
        public RawADMaintenanceController(IBaseRepository<RawADMaintenance> rRawADMaintenance,
            IBaseRepository<SystemParameter> rSystemParameter, IBaseRepository<Maintenance> rMaintenance,
            IBaseRepository<Employee> rEmployee, IBaseRepository<Company> rCompany, 
            IBaseRepository<CompanyStructure> rCompanyStructure, IBaseRepository<Component> rComponent,
            IImportDataServer sImportDataServer, ICalendarLogic lCalendar)
        {
            _rRawADMaintenance = rRawADMaintenance;
            _rSystemParameter = rSystemParameter;
            _rMaintenance = rMaintenance;
            _rEmployee = rEmployee;
            _rCompany = rCompany;
            _rCompanyStructure = rCompanyStructure;
            _rComponent = rComponent;
            _sImportDataServer = sImportDataServer;
            _lCalendar = lCalendar;
        }

        public ActionResult Index(string layout)
        {
            var companyStructures = (from p in _rCompanyStructure.GetAll().ToList()
                                     select new CompanyStructureShortView {
                                         CompanyStructureId = p.CompanyStructureId,
                                         Name = (p.Department != null ? p.Department.Name : p.Employee.Fullname)
                                     });

            ViewBag.Employees = new SelectList(_rEmployee.GetAll(), "EmployeeId", "Fullname");
            ViewBag.Maintenances = new SelectList(_rMaintenance.GetAll(), "MaintenanceId", "Name");
            ViewBag.Companies = new SelectList(_rCompany.GetAll(), "CompanyId", "Name");
            ViewBag.CompanyStructures = new SelectList(companyStructures, "CompanyStructureId", "Name");
            ViewBag.Components = new SelectList(_rComponent.GetAll(), "ComponentId", "Code");
            ViewBag.Periods = new SelectList(_lCalendar.GetPeriods(), "CodeMonth", "Name");

            if (layout == "Admin")
            {
                return View("Index", "~/Areas/Admin/Views/Shared/_Layout.cshtml");
            }
            else
            {
                return View("Index", "~/Areas/Customer/Views/Shared/_Layout.cshtml");
            }            
        }

        #region CRUD

        [GridAction]
        public ActionResult _Index()
        {
            var rawADMaintenances = (from p in _rRawADMaintenance.GetAll().ToList()
                                     select new RawADMaintenanceView { 
                                        RawADMaintenanceId = p.RawADMaintenanceId,
                                        MaintenanceId = (p.Maintenance != null ? p.Maintenance.Name : ""),
                                        RawAuthid = p.RawAuthid,
                                        EmployeeId = (p.EmployeeId != null ? p.Employee.Fullname : ""),
                                        CompanyId = (p.Company != null ? p.Company.Name : ""),
                                        CompanyStructureId = (p.CompanyStructure != null ? 
                                            (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname) : ""),
                                        ComponentId = (p.Component != null ? p.Component.Code : ""),
                                        CodeMonthFrom = p.CodeMonthFrom,
                                        CodeMonthTo = p.CodeMonthTo
                                     });

            return View(new GridModel(rawADMaintenances));
        }

        [GridAction]
        public ActionResult _CreateUpdate(RawADMaintenance rawADMaintenance)
        {
            if (ModelState.IsValid)
            {
                if (rawADMaintenance.RawADMaintenanceId.Equals(0))
                {
                    _rRawADMaintenance.Create(rawADMaintenance);
                }
                else
                {
                    _rRawADMaintenance.Update(rawADMaintenance);
                }

                rawADMaintenance.Maintenance = _rMaintenance.Get(p => p.MaintenanceId == rawADMaintenance.MaintenanceId);
                rawADMaintenance.Employee = _rEmployee.Get(p => p.EmployeeId == rawADMaintenance.EmployeeId);
            }

            var rawADMaintenances = (from p in _rRawADMaintenance.GetAll().ToList()
                                     select new RawADMaintenanceView
                                     {
                                         RawADMaintenanceId = p.RawADMaintenanceId,
                                         MaintenanceId = (p.Maintenance != null ? p.Maintenance.Name : ""),
                                         RawAuthid = p.RawAuthid,
                                         EmployeeId = (p.EmployeeId != null ? p.Employee.Fullname : ""),
                                         CompanyId = (p.Company != null ? p.Company.Name : ""),
                                         CompanyStructureId = (p.CompanyStructure != null ?
                                             (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname) : ""),
                                         ComponentId = (p.Component != null ? p.Component.Code : ""),
                                         CodeMonthFrom = p.CodeMonthFrom,
                                         CodeMonthTo = p.CodeMonthTo
                                     });

            return View(new GridModel(rawADMaintenances));
        }

        [GridAction]
        public ActionResult _Delete(int id)
        {
            var rawADMaintenance = _rRawADMaintenance.Get(p => p.RawADMaintenanceId == id);
            _rRawADMaintenance.Delete(rawADMaintenance);

            var rawADMaintenances = (from p in _rRawADMaintenance.GetAll().ToList()
                                     select new RawADMaintenanceView
                                     {
                                        RawADMaintenanceId = p.RawADMaintenanceId,
                                        MaintenanceId = (p.Maintenance != null ? p.Maintenance.Name : ""),
                                        RawAuthid = p.RawAuthid,
                                        EmployeeId = (p.EmployeeId != null ? p.Employee.Fullname : ""),
                                        CompanyId = (p.Company != null ? p.Company.Name : ""),
                                        CompanyStructureId = (p.CompanyStructure != null ?
                                            (p.CompanyStructure.Department != null ? p.CompanyStructure.Department.Name : p.CompanyStructure.Employee.Fullname) : ""),
                                        ComponentId = (p.Component != null ? p.Component.Code : ""),
                                        CodeMonthFrom = p.CodeMonthFrom,
                                        CodeMonthTo = p.CodeMonthTo
                                     });

            return View(new GridModel(rawADMaintenances));
        }

        #endregion

        #region Import

        public ContentResult _Import()
        {
            try
            {
                _rRawADMaintenance.BulkDelete("RawADMaintenances");

                var serverController = _rSystemParameter.Get(p => p.Name == "DomainServerController");
                var rootContainer = _rSystemParameter.Get(p => p.Name == "DomainRootContainer");

                PrincipalContext principalContext =
                    new PrincipalContext(ContextType.Domain, serverController.Value, rootContainer.Value);
                GroupPrincipal groupPrincipal;

                string[] containers;
                var maintenances = _rMaintenance.GetList(p => p.ContainerAD != null && p.ContainerAD.Trim() != "").ToList();

                foreach (Maintenance maintenance in maintenances)
                {
                    containers = maintenance.ContainerAD.Split(';');

                    foreach (string container in containers)
                    {
                        groupPrincipal = GroupPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, container.Trim());
                        if (groupPrincipal != null)
                        {
                            foreach (Principal principalUser in groupPrincipal.GetMembers(true))
                            {
                                if (!_rRawADMaintenance.GetList(p => p.RawAuthid == principalUser.SamAccountName 
                                    && p.MaintenanceId == maintenance.MaintenanceId).Any())
                                {
                                    _rRawADMaintenance.Create(new RawADMaintenance
                                    {
                                        RawAuthid = principalUser.SamAccountName,
                                        MaintenanceId = maintenance.MaintenanceId
                                    });
                                }
                            }
                        }
                    }
                }

                return Content("Данные обработаны");
            }
            catch
            {
                return Content("Данные обработаны");
            }
        }

        [HttpPost]
        public ContentResult _Parse()
        {            
            return Content(_sImportDataServer.ParseADMaintenance());
        }

        [HttpPost]
        public ContentResult _Load()
        {            
            return Content(_sImportDataServer.LoadADMaintenance());
        }

        #endregion

    }
}
