﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.ServerSide;
using Core.Filters;

namespace WebUI.Controllers
{
    [CustomAuthorize(Roles = "Admin, Customer")]
    public class RawDepartmentController : Controller
    {
        private readonly IImportDataServer _sImportDataServer;

        public RawDepartmentController(IImportDataServer sImportDataServer)
        {
            _sImportDataServer = sImportDataServer;
        }

        #region Import

        public ContentResult _LoadDepartment()
        {
            _sImportDataServer.LoadDepartment();
            return Content("Данные обработаны");
        }

        #endregion

    }
}
