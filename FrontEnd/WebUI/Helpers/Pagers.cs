﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Core.Views;

namespace WebUI.Helpers
{
    public static class Pagers
    {
        public static MvcHtmlString StandartPager(this AjaxHelper helper, string action, string controller,
            PagingInfo pagingInfo, string updateTargetId)
        {
            StringBuilder sb = new StringBuilder();
            int counter = Convert.ToInt32(Math.Ceiling((double)pagingInfo.TotalItems / pagingInfo.ItemsPerSheet));

            var options = new AjaxOptions()
            {
                UpdateTargetId = updateTargetId,
                InsertionMode = InsertionMode.Replace,
                HttpMethod = "POST"
            };

            for (int i = 1; i <= counter; i++)
            {
                var link = helper.ActionLink(i.ToString(), action, controller, new { currentPage = i }, options, new { @class = "standart-pager" });
                sb.Append(link.ToString());
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString StandartPager(this AjaxHelper helper, string action, string controller, int dataId,
            PagingInfo pagingInfo, string updateTargetId)
        {
            StringBuilder sb = new StringBuilder();
            int counter = Convert.ToInt32(Math.Ceiling((double)pagingInfo.TotalItems / pagingInfo.ItemsPerSheet));

            var options = new AjaxOptions()
            {
                UpdateTargetId = updateTargetId,
                InsertionMode = InsertionMode.Replace,
                HttpMethod = "POST"
            };

            for (int i = 1; i <= counter; i++)
            {
                var link = helper.ActionLink(i.ToString(), action, controller, new { currentPage = i, dataId = dataId }, options, new { @class = "standart-pager" });
                sb.Append(link.ToString());
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        //public static MvcHtmlString ParametrizedPager(this AjaxHelper helper, string action, string controller, int dataId,
        //    PagingInfo pagingInfo, string updateTargetId)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    int counter = Convert.ToInt32(Math.Ceiling((double)pagingInfo.TotalItems / pagingInfo.ItemsPerSheet));

        //    var options = new AjaxOptions()
        //    {
        //        UpdateTargetId = updateTargetId,
        //        InsertionMode = InsertionMode.Replace,
        //        HttpMethod = "POST"
        //    };

        //    for (int i = 1; i <= counter; i++)
        //    {
        //        var link = helper.ActionLink(i.ToString(), action, controller, new { currentPage = i, dataId = dataId }, options, new { @class = "standart-pager" });
        //        sb.Append(link.ToString());
        //    }

        //    return MvcHtmlString.Create(sb.ToString());
        //}

    }



}