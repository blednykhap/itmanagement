﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Business.Others
{
    public interface ICalendarLogic
    {
        DateTime GetDateByCodeMonth(int codeMonth);
        int GetCodeMonthByDate(DateTime date);
        IEnumerable<Period> GetPeriods();        
    }

    public class CalendarLogic : ICalendarLogic
    {
        public DateTime GetDateByCodeMonth(int codeMonth)
        {
            int year = 0;
            int month = 0;

            if ((codeMonth % 12) != 0)
            {
                year = codeMonth / 12;
                month = codeMonth % 12;
            }
            else
            {
                year = (codeMonth / 12) - 1;
                month = 12;
            }
            return new DateTime(year, month, 1);
        }

        public int GetCodeMonthByDate(DateTime date)
        {
            return date.Year * 12 + date.Month;
        }

        public IEnumerable<Period> GetPeriods()
        {
            DateTime today = DateTime.Now;
            var beginPeriod = today.AddMonths(-12);
            var endPeriod = Convert.ToDateTime("31.12.2099");

            List<Period> periods = new List<Period>();
            for (DateTime day = beginPeriod; day <= endPeriod; day = day.AddMonths(1))
            {
                periods.Add(new Period()
                {
                    CodeMonth = day.Year * 12 + day.Month,
                    Name = (day.ToString("MMMM") + " " + day.Year.ToString())
                });
            }

            return periods;
        }
    }
}
