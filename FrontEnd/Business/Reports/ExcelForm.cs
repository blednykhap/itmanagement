﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using System.Web;
using Business.Reports;
using Core.Views;
using Core.Reports;
using System.Collections;

namespace Business.Reports
{
    public interface IExcelForm
    {
        byte[] UsedLicenseExcel(IEnumerable usedLicense);
        byte[] InstalledSoftwareExcel(IEnumerable installedSoftware);
        byte[] MaintenanceCross(string[,] array, int x, int y);
        byte[] R24HoursMaintenanceExcel(IEnumerable r24HoursMaintenance);
        byte[] InvoiceExcel(IEnumerable invoice);
        byte[] ChessExcel(IEnumerable chess);
    }

    public class ExcelForm : IExcelForm
    {
        private HSSFWorkbook workbook;        
        private ICellStyle cellStyleNormal;
        private ICellStyle cellStyleRotated;

        #region Constructor

        public ExcelForm()
        {
            #region Templates

            string pathName = HttpContext.Current.Server.MapPath("~/Templates/ExcelForm/");
            string fileName = "";

            var action = HttpContext.Current.Request.RequestContext.RouteData.GetRequiredString("action").ToString();
            switch (action)
            {
                case "UsedLicense":
                    fileName = "UsedLicense.xls";
                    break;
                case "_UsedLicense":
                    fileName = "UsedLicense.xls";
                    break;
                case "InstalledSoftware":
                    fileName = "InstalledSoftware.xls";
                    break;
                case "_InstalledSoftware":
                    fileName = "InstalledSoftware.xls";
                    break;
                case "MaintenanceCross":
                    fileName = "MaintenanceCross.xls";
                    break;
                case "_MaintenanceCross":
                    fileName = "MaintenanceCross.xls";
                    break;
                case "R24HoursMaintenance":
                    fileName = "R24HoursMaintenance.xls";
                    break;
                case "_R24HoursMaintenance":
                    fileName = "R24HoursMaintenance.xls";
                    break;
                default: fileName = "UsedLicense.xls";
                    break;
            }

            string filePath = Path.Combine(pathName, fileName);

            #endregion

            FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            workbook = new HSSFWorkbook(file);

            #region Fonts & Styles

            var fontNormal = workbook.CreateFont();
            fontNormal.Boldweight = (short)FontBoldWeight.NORMAL;

            cellStyleNormal = workbook.CreateCellStyle();
            cellStyleNormal.SetFont(fontNormal);
            cellStyleNormal.BorderTop = BorderStyle.THIN;
            cellStyleNormal.BorderRight = BorderStyle.THIN;
            cellStyleNormal.BorderLeft = BorderStyle.THIN;
            cellStyleNormal.BorderBottom = BorderStyle.THIN;
            cellStyleNormal.WrapText = true;            

            cellStyleRotated = workbook.CreateCellStyle();
            cellStyleRotated.SetFont(fontNormal);
            cellStyleRotated.BorderTop = BorderStyle.THIN;
            cellStyleRotated.BorderRight = BorderStyle.THIN;
            cellStyleRotated.BorderLeft = BorderStyle.THIN;
            cellStyleRotated.BorderBottom = BorderStyle.THIN;
            cellStyleRotated.WrapText = true;
            cellStyleRotated.Rotation = 90;

            #endregion
        }

        #endregion

        #region byte[] UsedLicenseExcel(IEnumerable usedLicense)

        public byte[] UsedLicenseExcel(IEnumerable usedLicense)
        {
            ISheet sheet = workbook.GetSheet("Лист");
            int iRow = 2;

            foreach (UsedLicense item in usedLicense)
            {
                var currentRow = sheet.CreateRow(iRow);

                currentRow.CreateCell(0).SetCellValue(item.Contract);
                currentRow.GetCell(0).CellStyle = cellStyleNormal;
                currentRow.CreateCell(1).SetCellValue(item.Contractor);
                currentRow.GetCell(1).CellStyle = cellStyleNormal;
                currentRow.CreateCell(2).SetCellValue(item.Software);
                currentRow.GetCell(2).CellStyle = cellStyleNormal;
                currentRow.CreateCell(3).SetCellValue(item.DateTo);
                currentRow.GetCell(3).CellStyle = cellStyleNormal;
                currentRow.CreateCell(4).SetCellValue(item.Amount);
                currentRow.GetCell(4).CellStyle = cellStyleNormal;
                currentRow.CreateCell(5).SetCellValue(item.UsedAmount);
                currentRow.GetCell(5).CellStyle = cellStyleNormal;
                currentRow.CreateCell(6).SetCellValue(item.Balance);
                currentRow.GetCell(6).CellStyle = cellStyleNormal;

                iRow++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);

            return ms.ToArray();
        }

        #endregion

        #region byte[] InstalledSoftwareExcel(IEnumerable installedSoftware)
        
        public byte[] InstalledSoftwareExcel(IEnumerable installedSoftware)
        {
            ISheet sheet = workbook.GetSheet("Лист");
            int iRow = 2;

            foreach (InstalledSoftwareView item in installedSoftware)
            {
                var currentRow = sheet.CreateRow(iRow);

                currentRow.CreateCell(0).SetCellValue(item.EmployeeFullname);
                currentRow.GetCell(0).CellStyle = cellStyleNormal;
                currentRow.CreateCell(1).SetCellValue(item.NetbiosName);
                currentRow.GetCell(1).CellStyle = cellStyleNormal;
                currentRow.CreateCell(2).SetCellValue(item.Software);
                currentRow.GetCell(2).CellStyle = cellStyleNormal;

                iRow++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);

            return ms.ToArray();
        }

        #endregion

        #region byte[] MaintenanceCross()

        public byte[] MaintenanceCross(string[,] array, int x, int y)
        {
            ISheet sheet = workbook.GetSheet("Лист");

            var currentRow = sheet.CreateRow(0+1);

            for (int j = 0; j < y; j++) // col
            {                
                currentRow.CreateCell(j).SetCellValue(array[0, j]);                
                currentRow.GetCell(j).CellStyle = cellStyleRotated;                
            }

            for (int i = 1; i < x; i++)  // row
            {
                currentRow = sheet.CreateRow(i+1);

                for (int j = 0; j < y; j++) // col
                {                    
                    currentRow.CreateCell(j).SetCellValue(array[i, j]);
                    currentRow.GetCell(j).CellStyle = cellStyleNormal;                    
                }                
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);

            return ms.ToArray();
        }

        #endregion

        #region byte[] R24HoursMaintenanceExcel(IEnumerable r24HoursMaintenance)

        public byte[] R24HoursMaintenanceExcel(IEnumerable r24HoursMaintenance)
        {
            ISheet sheet = workbook.GetSheet("Лист");
            int iRow = 2;

            foreach (R24HoursMaintenance item in r24HoursMaintenance)
            {
                var currentRow = sheet.CreateRow(iRow);

                currentRow.CreateCell(0).SetCellValue(item.Maintenance);
                currentRow.GetCell(0).CellStyle = cellStyleNormal;
                currentRow.CreateCell(1).SetCellValue(item.Authid);
                currentRow.GetCell(1).CellStyle = cellStyleNormal;
                currentRow.CreateCell(2).SetCellValue(item.Employee);
                currentRow.GetCell(2).CellStyle = cellStyleNormal;

                iRow++;
            }

            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);

            return ms.ToArray();
        }

        #endregion

        #region byte[] InvoiceExcel(IEnumerable invoice)

        public byte[] InvoiceExcel(IEnumerable invoice)
        {
            return null;
        }
        
        #endregion

        #region byte[] ChessExcel(IEnumerable chess)

        public byte[] ChessExcel(IEnumerable chess)
        {
            return null;
        }

        #endregion
    }
}
