﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Views;
using Core.Models;
using Core.Reports;
using System.Data.SqlClient;

namespace Business.Reports
{
    public interface IReportRepository : IDisposable
    {        
        IEnumerable<InstalledSoftwareView> InstalledSoftwareReport(int softwareId = 0);
        IEnumerable<UsedLicense> UsedLicenseReport();
        IEnumerable<R24HoursMaintenance> R24HoursMaintenanceReport(int period);
        IEnumerable<Invoice> InvoiceReport(int userId, int codeMonth, int companyId, int subdivisionId, string ifSec);
        IEnumerable<Chess> ChessReport(int codeMonth, int companyId, int subdivisionId);
    }

    public class ReportRepository : IReportRepository
    {
        private ITManagementDBContext _context;

        public ReportRepository(ITManagementDBContext context)
        {
            _context = context;
        }

        #region IEnumerable<InstalledSoftwareView> InstalledSoftwareReport(int softwareId = 0)

        public IEnumerable<InstalledSoftwareView> InstalledSoftwareReport(int softwareId = 0)
        {

            var param = new SqlParameter("softwareId", softwareId);

            var data = _context.Database
                .SqlQuery<InstalledSoftwareView>("exec dbo.ReportInstalledSoftware @softwareId", param);

            return data.ToList();
        }

        #endregion

        #region IEnumerable<UsedLicense> UsedLicenseReport()

        public IEnumerable<UsedLicense> UsedLicenseReport()
        {
            var data = _context.Database
                .SqlQuery<UsedLicense>("exec dbo.ReportUsedLicense");

            return data.ToList();
        }

        #endregion

        #region IEnumerable<R24HoursMaintenance> R24HoursMaintenanceReport()

        public IEnumerable<R24HoursMaintenance> R24HoursMaintenanceReport(int period)
        {
            var param = new SqlParameter("period", period);

            var data = _context.Database
                .SqlQuery<R24HoursMaintenance>("exec dbo.ReportR24HoursMaintenance @period", param);

            return data.ToList();
        }

        #endregion

        public void Dispose()
        {
            _context.Dispose();
        }

        #region IEnumerable<Invoice> InvoiceReport()

        public IEnumerable<Invoice> InvoiceReport(int userId, int codeMonth, int companyId, int subdivisionId, string ifSec)
        {
            /* var param = new SqlParameter("nuser", userId);
             var param2 = new SqlParameter("ndocdate", codeMonth);
             var param3 = new SqlParameter("ncompanyid", companyId);
             var param4 = new SqlParameter("sec", ifSec);*/

            var cmd = _context.Database.Connection.CreateCommand();
            cmd.CommandTimeout = 300;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "dbo.ReportInvoice";

            cmd.Parameters.Add(new SqlParameter("nuser", userId));
            cmd.Parameters.Add(new SqlParameter("ndocdate", codeMonth));
            cmd.Parameters.Add(new SqlParameter("ncompanyid", companyId));
            cmd.Parameters.Add(new SqlParameter("nsubdivisionid", subdivisionId));
            cmd.Parameters.Add(new SqlParameter("sec", ifSec));

            /*cmd.Parameters.AddWithValue("@nuser", userId);
            cmd.Parameters.AddWithValue("@ndocdate", codeMonth);
            cmd.Parameters.AddWithValue("@ncompanyid", companyId);
            cmd.Parameters.AddWithValue("@sec", ifSec);*/

            _context.Database.Connection.Open();


            var reader = cmd.ExecuteReader();
            List<Invoice> data = new List<Invoice>();

            while (reader.Read())
            {
                Invoice itm = new Invoice();
                itm.ServiceLine = reader["ServiceLine"].ToString();
                itm.ServiceLineName = reader["ServiceLineName"].ToString();
                itm.ServiceCode = reader["ServiceCode"].ToString();
                itm.Component = reader["Component"].ToString();
                itm.ServiceName = reader["ServiceName"].ToString();
                itm.ServiceNameFull = reader["ServiceNameFull"].ToString();
                itm.Measure = reader["Measure"].ToString();
                itm.SupportLevel = reader["SupportLevel"].ToString();
                itm.Price = Convert.ToDecimal(reader["Price"].ToString());
                itm.Amount = Convert.ToInt32(reader["Amount"].ToString());
                itm.Cost = Convert.ToDecimal(reader["Cost"].ToString());
                itm.Department = reader["Department"].ToString();
                itm.Mvz = reader["Mvz"].ToString();
                itm.SubdivName = reader["SubdivName"].ToString();

                data.Add(itm);
            }

            _context.Database.Connection.Close();

            /* var data = _context.Database
                 .SqlQuery<Invoice>("exec dbo.ReportInvoice @nuser, @ndocdate, @ncompanyid, @sec", param, param2, param3,param4);
             */
            return data;
        }

        #endregion

        #region IEnumerable<Chess> ChessReport()

        public IEnumerable<Chess> ChessReport(int codeMonth, int companyId, int subdivisionId)
        {
            var param1 = new SqlParameter("ndocdate", codeMonth);
            var param2 = new SqlParameter("ncompanyid", companyId);
            var param3 = new SqlParameter("nsubdivisionid", subdivisionId);


            var data = _context.Database
                .SqlQuery<Chess>("exec dbo.ReportChess @ndocdate, @ncompanyid,@nsubdivisionid ", param1, param2, param3);

            return data.ToList();
        }

        #endregion
    }
}
