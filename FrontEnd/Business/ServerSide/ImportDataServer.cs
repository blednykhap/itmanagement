﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using System.Data.SqlClient;
using System.Web;

namespace Business.ServerSide
{
    public interface IImportDataServer : IDisposable
    {
        string ImportEmployee();
        string ParseEmployee();
        string LoadEmployee();

        string LoadDepartment(); 

        string ParseADMaintenance();
        string LoadADMaintenance();

        string ParseMaintenanceByUser();
        string LoadMaintenanceByUser();

        string ParseSAPMaintenance();
        string LoadSAPMaintenance();

        string ImportSystem();
        string ParseSystem();
        string LoadSystem();

        string ImportInstalledSoftware();
        string ParseInstalledSoftware();
        string LoadInstalledSoftware();

        bool ParseCostCenter();
        bool LoadCostCenter();
    }

    public class ImportDataServer : IImportDataServer
    {
        private readonly ITManagementDBContext _context;
        private readonly IBaseRepository<LogError> _rLogError;        

        public ImportDataServer(ITManagementDBContext context, IBaseRepository<LogError> rLogError)
        {
            _context = context;
            _rLogError = rLogError;           
        }

        #region ExecuteScript (Core)

        private string ExecuteScript(string sql)
        {            
            var parameter = new SqlParameter();
            parameter.ParameterName = "result";
            parameter.Direction = System.Data.ParameterDirection.Output;
            parameter.DbType = System.Data.DbType.Boolean;

            try
            {                
                _context.Database.ExecuteSqlCommand(sql, parameter);
                if (Convert.ToBoolean(parameter.Value))
                {
                    return "Данные обработаны.";
                }
                else
                {
                    return "При обработке данных произошел сбой.";
                }                
            }
            catch (Exception e)
            {                
                _rLogError.Create(new LogError { 
                    Authid = HttpContext.Current.Request.LogonUserIdentity.Name,
                    ComputerIP = HttpContext.Current.Request.UserHostAddress,
                    EventDate = DateTime.Now,
                    Message = e.Message.ToString(),
                    InnerException = (e.InnerException != null ? 
                        (e.InnerException.InnerException != null ? e.InnerException.InnerException.Message : e.InnerException.Message ) : ""),
                    Source = e.Source,
                    StackTrace = e.StackTrace                    
                });
                return "При обработке данных произошел сбой, смотрите журнал ошибок.";                
            } 
        }

        #endregion

        #region Employee

        public string ImportEmployee()
        {
            var sql = "exec ImportEmployee @Result OUTPUT";
            return ExecuteScript(sql);             
        }

        public string ParseEmployee()
        {
            var sql = "exec ParseEmployee @Result OUTPUT";
            return ExecuteScript(sql);            
        }

        public string LoadEmployee()
        {
            var sql = "exec LoadEmployee @Result OUTPUT";
            return ExecuteScript(sql);            
        }

        #endregion

        #region Department

        public string LoadDepartment()
        {
            var sql = "exec LoadDepartment @Result OUTPUT";
            return ExecuteScript(sql);            
        }

        #endregion

        #region ADMaintenance

        public string ParseADMaintenance()
        {
            var sql = "exec ParseADMaintenance @Result OUTPUT";
            return ExecuteScript(sql);            
        }

        public string LoadADMaintenance()
        {
            var sql = "exec LoadADMaintenance @Result OUTPUT";
            return ExecuteScript(sql);            
        }

        #endregion

        #region MaintenanceByUser

        public string ParseMaintenanceByUser()
        {            
            var sql = "exec ParseMaintenanceByUser @Result OUTPUT";
            return ExecuteScript(sql);
        }

        public string LoadMaintenanceByUser()
        {
            var sql = "exec LoadMaintenanceByUser @Result OUTPUT";
            return ExecuteScript(sql);
        }

        #endregion

        #region SAPMaintenance

        public string ParseSAPMaintenance()
        {            
            var sql = "exec ParseSAPMaintenance @Result OUTPUT";
            return ExecuteScript(sql);            
        }

        public string LoadSAPMaintenance()
        {
            var sql = "exec LoadSAPMaintenance @Result OUTPUT";
            return ExecuteScript(sql);
        }

        #endregion

        #region CostCenter

        public bool ParseCostCenter()
        {
            var sql = "exec ParseCostCenter";

            _context.Database.ExecuteSqlCommand(sql);
            return true;
        }

        public bool LoadCostCenter()
        {
            var sql = "exec LoadCostCenter";

            _context.Database.ExecuteSqlCommand(sql);
            return true;
        }

        #endregion

        #region Computer System

        public string ImportSystem()
        {
            var sql = "exec ImportSystem @Result OUTPUT";
            return ExecuteScript(sql);
        }

        public string ParseSystem()
        {
            var sql = "exec ParseSystem @Result OUTPUT";
            return ExecuteScript(sql);
        }

        public string LoadSystem()
        {
            var sql = "exec LoadSystem @Result OUTPUT";
            return ExecuteScript(sql);
        }

        #endregion

        #region Installed Software

        public string ImportInstalledSoftware()
        {
            var sql = "exec ImportInstalledSoftware @Result OUTPUT";
            return ExecuteScript(sql);
        }

        public string ParseInstalledSoftware()
        {
            var sql = "exec ParseInstalledSoftware @Result OUTPUT";
            return ExecuteScript(sql);
        }

        public string LoadInstalledSoftware()
        {
            var sql = "exec LoadInstalledSoftware @Result OUTPUT";
            return ExecuteScript(sql);
        }

        #endregion

        public void Dispose()
        {
            _context.Dispose();            
        }
    }
}
